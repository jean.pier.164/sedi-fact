﻿Imports System.Data.SqlClient
Public Class ReporteBoletas

    Inherits conexion
    Dim cmd As New SqlCommand
    Public Function mostrar_reporte_boleta() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("reporte_boletas")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@fechai", frmcCantidadProductosporUsuario.txtfechai.Value)
            cmd.Parameters.AddWithValue("@fechafi", frmcCantidadProductosporUsuario.txtfechafi.Value)



            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt



        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try


    End Function

End Class
