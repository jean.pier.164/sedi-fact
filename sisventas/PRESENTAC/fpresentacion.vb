﻿Imports System.Data.SqlClient
Public Class fpresentacion
    Inherits conexion
    Dim cmd As New SqlCommand

    Public Function mostrar() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("mostrar_presentacion")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function


    Public Function insertar(ByVal dts As vpresentacion) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("insertar_presentacion")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@undm", dts.gundm)

            cmd.Parameters.AddWithValue("@sundm", dts.gsundm)
            cmd.Parameters.AddWithValue("@descripcion", dts.gdescripcion)



            If cmd.ExecuteNonQuery Then
                Return True

            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try

    End Function
    Public Function editar(ByVal dts As vpresentacion) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("editar_presentacion")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@idpresentacion", dts.gidpresentacion)
            cmd.Parameters.AddWithValue("@undm", dts.gundm)

            cmd.Parameters.AddWithValue("@sundm", dts.gsundm)
            cmd.Parameters.AddWithValue("@descripcion", dts.gdescripcion)

            If cmd.ExecuteNonQuery Then
                Return True

            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try

    End Function

    Public Function eliminar(ByVal dts As vpresentacion) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("eliminar_presentacion")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.Add("@idpresentacion", SqlDbType.NVarChar, 50).Value = dts.gidpresentacion

            If cmd.ExecuteNonQuery Then
                Return True
            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False

        End Try
    End Function

End Class
