﻿Public Class vproveedor
    Dim idproveedor As Integer
    Dim razon_social, sector_comercial, tipo_documento, num_documento, direccion, telefono, email, url As String



    Public Property gidproveedor
        Get
            Return idproveedor
        End Get
        Set(ByVal value)
            idproveedor = value
        End Set
    End Property

    Public Property grazon_social
        Get
            Return razon_social
        End Get
        Set(ByVal value)
            razon_social = value
        End Set
    End Property

    Public Property gsector_comercial
        Get
            Return sector_comercial
        End Get
        Set(ByVal value)
            sector_comercial = value
        End Set
    End Property


    Public Property gtipo_documento
        Get
            Return tipo_documento
        End Get
        Set(ByVal value)
            tipo_documento = value
        End Set
    End Property
    Public Property gnum_documento
        Get
            Return num_documento
        End Get
        Set(ByVal value)
            num_documento = value
        End Set
    End Property
    Public Property gdireccion
        Get
            Return direccion
        End Get
        Set(ByVal value)
            direccion = value
        End Set
    End Property
    Public Property gtelefono
        Get
            Return telefono
        End Get
        Set(ByVal value)
            telefono = value
        End Set
    End Property


    Public Property gemail
        Get
            Return email
        End Get
        Set(ByVal value)
            email = value
        End Set
    End Property
    Public Property gurl
        Get
            Return url
        End Get
        Set(ByVal value)
            url = value
        End Set
    End Property

    Sub New()

    End Sub
    Public Sub New(ByVal idproveedor As Integer, ByVal razon_social As String, ByVal sector_comercial As String, ByVal tipo_documento As String, ByVal num_documento As String, ByVal direccion As String, ByVal telefono As String, ByVal email As String, ByVal url As String)
        gidproveedor = idproveedor
        grazon_social = razon_social
        gsector_comercial = sector_comercial
        gtipo_documento = tipo_documento

        gnum_documento = num_documento
        gdireccion = direccion
        gtelefono = telefono
        gemail = email
        gurl = url

    End Sub
End Class
