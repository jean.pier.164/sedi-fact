﻿Public Class vdetalle_ingreso
    Dim iddetalle_ingreso, idingreso, idproducto As Integer
    Dim precio_compra, precio_venta As Double
    Dim stock_inicial, stock_actual As Integer
    Dim fecha_produccion, fecha_vencimiento As Date

    Public Property giddetalle_ingreso
        Get
            Return iddetalle_ingreso
        End Get
        Set(ByVal value)
            iddetalle_ingreso = value
        End Set
    End Property

    Public Property gidingreso
        Get
            Return idingreso
        End Get
        Set(ByVal value)
            idingreso = value
        End Set
    End Property

    Public Property gidproducto
        Get
            Return idproducto
        End Get
        Set(ByVal value)
            idproducto = value
        End Set
    End Property



    Public Property gprecio_compra
        Get
            Return precio_compra
        End Get
        Set(ByVal value)
            precio_compra = value
        End Set
    End Property

    Public Property gprecio_venta
        Get
            Return precio_venta
        End Get
        Set(ByVal value)
            precio_venta = value
        End Set
    End Property


    Public Property gstock_inicial
        Get
            Return stock_inicial
        End Get
        Set(ByVal value)
            stock_inicial = value
        End Set
    End Property


    Public Property gstock_actual
        Get
            Return stock_actual
        End Get
        Set(ByVal value)
            stock_actual = value
        End Set
    End Property

    Public Property gfecha_produccion
        Get
            Return fecha_produccion
        End Get
        Set(ByVal value)
            fecha_produccion = value
        End Set
    End Property

    Public Property gfecha_vencimiento
        Get
            Return fecha_vencimiento
        End Get
        Set(ByVal value)
            fecha_vencimiento = value
        End Set
    End Property
    Public Sub New()

    End Sub
    Public Sub New(ByVal iddetalle_ingreso As Integer, ByVal idingreso As Integer, ByVal idproducto As Integer, ByVal precio_compra As Double, ByVal precio_venta As Double, ByVal stock_inicial As Integer, ByVal stock_actual As Integer, ByVal fecha_produccion As Date, ByVal fecha_vencimiento As Date)
        giddetalle_ingreso = iddetalle_ingreso
        gidingreso = idingreso
        gidproducto = idproducto
        gprecio_compra = precio_compra
        gprecio_venta = precio_venta
        gstock_inicial = stock_inicial
        gstock_actual = stock_actual
        gfecha_produccion = fecha_produccion
        gfecha_vencimiento = fecha_vencimiento

    End Sub

End Class
