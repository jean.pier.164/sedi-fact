﻿Public Class vclienteTipoDocumento
    Dim idTipo As Integer
    Dim codigoSunat, descripcion As String



    'seeter y getter

    Public Property gidcliente
        Get
            Return idTipo

        End Get
        Set(ByVal value)


            idTipo = value

        End Set
    End Property

    Public Property gcodigoSunat

        Get
            Return codigoSunat


        End Get
        Set(ByVal value)
            codigoSunat = value
        End Set
    End Property


    Public Property gdescripcion
        Get
            Return descripcion
        End Get
        Set(ByVal value)
            descripcion = value
        End Set
    End Property

    'constructores

    Public Sub New()

    End Sub

    Public Sub New(ByVal idTipo As Integer, ByVal nombres As String, ByVal codigoSunat As String, ByVal descripcion As String)
        gidcliente = idTipo
        gcodigoSunat = codigoSunat
        gdescripcion = descripcion

    End Sub
End Class
