﻿Public Class vresumenDiario

    Dim id As Integer
    Dim idTipoDocSerie As Integer
    Dim correlativo As Integer
    Dim codigoEstadoEnvioSunat, mensajeEnvioSunat, nombreArchivoZip, nombreArchivo, nombreDocumento As String
    Dim fecha As Date


    'seeter y getter

    Public Property gidcliente
        Get
            Return id

        End Get
        Set(ByVal value)


            id = value

        End Set
    End Property

    Public Property gidTipoDocSerie
        Get
            Return idTipoDocSerie

        End Get
        Set(ByVal value)


            idTipoDocSerie = value

        End Set
    End Property

    Public Property gcorrelativo
        Get
            Return correlativo

        End Get
        Set(ByVal value)


            correlativo = value

        End Set
    End Property

    Public Property gcodigoEstadoEnvioSunat

        Get
            Return codigoEstadoEnvioSunat


        End Get
        Set(ByVal value)
            codigoEstadoEnvioSunat = value
        End Set
    End Property


    Public Property gmensajeEnvioSunat
        Get
            Return mensajeEnvioSunat
        End Get
        Set(ByVal value)
            mensajeEnvioSunat = value
        End Set
    End Property


    Public Property gnombreArchivoZip
        Get
            Return nombreArchivoZip
        End Get
        Set(ByVal value)
            nombreArchivoZip = value
        End Set
    End Property


    Public Property gnombreArchivo
        Get
            Return nombreArchivo
        End Get
        Set(ByVal value)
            nombreArchivo = value
        End Set
    End Property


    Public Property gnombreDocumento
        Get
            Return nombreDocumento
        End Get
        Set(ByVal value)
            nombreDocumento = value
        End Set
    End Property

    Public Property gfecha
        Get
            Return fecha
        End Get
        Set(ByVal value)
            fecha = value
        End Set
    End Property

    'constructores

    Public Sub New()

    End Sub

    Public Sub New(ByVal idcliente As Integer, ByVal idTipoDocSerie As Integer, ByVal correlativo As Integer, ByVal codigoEstadoEnvioSunat As String,
                   ByVal mensajeEnvioSunat As String, ByVal nombreArchivoZip As String, ByVal nombreArchivo As String, ByVal nombreDocumento As String,
                   ByVal fecha As Date)
        gidcliente = idcliente
        gidTipoDocSerie = idTipoDocSerie
        gcorrelativo = correlativo
        gcodigoEstadoEnvioSunat = codigoEstadoEnvioSunat
        gmensajeEnvioSunat = mensajeEnvioSunat
        gnombreArchivoZip = nombreArchivoZip
        gnombreArchivo = nombreArchivo
        gnombreDocumento = nombreDocumento
        gfecha = fecha

    End Sub
End Class
