﻿Imports System.Configuration
Imports System.Data.SqlClient

Public Class FrmComprobantes
    Dim conexion As New SqlConnection(ConfigurationManager.ConnectionStrings("BRAVOSPORT.My.MySettings.marketConnectionString").ConnectionString)
    Dim cadena As String
    Dim datos As New DataSet
    Dim base As New SqlDataAdapter("select * from usuario", conexion)
    Dim variable As SqlDataReader
    Dim consulta3 As New SqlCommand
    Private dt As New DataTable
    Private Sub FrmComprobantes_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub mostrar_comprobantes()
        Try
            Dim func As New Fcomprobantes
            dt = func.mostrar_comprobantes()



            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt

                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing

                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try



    End Sub

    Private Sub btnconsultar_Click(sender As Object, e As EventArgs) Handles btnconsultar.Click

        If cbocom.Text = "BOLETAS" Then

            cbocom.Text = "03"

        End If
        If cbocom.Text = "FACTURAS" Then

            cbocom.Text = "01"

        End If

        mostrar_comprobantes()


        Dim mycount As Integer

        mycount = datalistado.Rows.Count()
        txtcount.Text = mycount



        Try
            Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("TOTAL").Value IsNot Nothing) AndAlso
                    (row.Cells("TOTAL").Value IsNot DBNull.Value)) Select row.Cells("TOTAL").Value()

            Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDouble(row))
            txtssventa.Text = String.Format(resultado)


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try


    End Sub

    Private Sub btnimprimir_Click(sender As Object, e As EventArgs) Handles btnimprimir.Click
        ReporteComprobantes.txtfechai.Text = Me.txtfechai.Text
        ReporteComprobantes.txtfechafi.Text = Me.txtfechafi.Text
        ReporteComprobantes.txtcom.Text = cbocom.Text





        ReporteComprobantes.ShowDialog()
    End Sub
End Class