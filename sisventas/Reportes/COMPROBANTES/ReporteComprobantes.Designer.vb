﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReporteComprobantes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.NUTRICHIKENFV02DataSet = New BRAVOSPORT.NUTRICHIKENFV02DataSet()
        Me.mostrar_comprobantesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.mostrar_comprobantesTableAdapter = New BRAVOSPORT.NUTRICHIKENFV02DataSetTableAdapters.mostrar_comprobantesTableAdapter()
        Me.txtfechafi = New System.Windows.Forms.TextBox()
        Me.txtfechai = New System.Windows.Forms.TextBox()
        Me.txtcom = New System.Windows.Forms.TextBox()
        CType(Me.NUTRICHIKENFV02DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mostrar_comprobantesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "dscomprobantes"
        ReportDataSource1.Value = Me.mostrar_comprobantesBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "BRAVOSPORT.Comprobantes.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(800, 450)
        Me.ReportViewer1.TabIndex = 1
        '
        'NUTRICHIKENFV02DataSet
        '
        Me.NUTRICHIKENFV02DataSet.DataSetName = "NUTRICHIKENFV02DataSet"
        Me.NUTRICHIKENFV02DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'mostrar_comprobantesBindingSource
        '
        Me.mostrar_comprobantesBindingSource.DataMember = "mostrar_comprobantes"
        Me.mostrar_comprobantesBindingSource.DataSource = Me.NUTRICHIKENFV02DataSet
        '
        'mostrar_comprobantesTableAdapter
        '
        Me.mostrar_comprobantesTableAdapter.ClearBeforeFill = True
        '
        'txtfechafi
        '
        Me.txtfechafi.Location = New System.Drawing.Point(45, 71)
        Me.txtfechafi.Name = "txtfechafi"
        Me.txtfechafi.Size = New System.Drawing.Size(74, 20)
        Me.txtfechafi.TabIndex = 23
        Me.txtfechafi.Visible = False
        '
        'txtfechai
        '
        Me.txtfechai.Location = New System.Drawing.Point(45, 45)
        Me.txtfechai.Name = "txtfechai"
        Me.txtfechai.Size = New System.Drawing.Size(74, 20)
        Me.txtfechai.TabIndex = 22
        Me.txtfechai.Visible = False
        '
        'txtcom
        '
        Me.txtcom.Location = New System.Drawing.Point(45, 97)
        Me.txtcom.Name = "txtcom"
        Me.txtcom.Size = New System.Drawing.Size(74, 20)
        Me.txtcom.TabIndex = 24
        Me.txtcom.Visible = False
        '
        'ReporteComprobantes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.txtcom)
        Me.Controls.Add(Me.txtfechafi)
        Me.Controls.Add(Me.txtfechai)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "ReporteComprobantes"
        Me.Text = "ReporteComprobantes"
        CType(Me.NUTRICHIKENFV02DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mostrar_comprobantesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents mostrar_comprobantesBindingSource As BindingSource
    Friend WithEvents NUTRICHIKENFV02DataSet As NUTRICHIKENFV02DataSet
    Friend WithEvents mostrar_comprobantesTableAdapter As NUTRICHIKENFV02DataSetTableAdapters.mostrar_comprobantesTableAdapter
    Friend WithEvents txtfechafi As TextBox
    Friend WithEvents txtfechai As TextBox
    Friend WithEvents txtcom As TextBox
End Class
