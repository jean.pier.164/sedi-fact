﻿Imports System.Data.SqlClient
Public Class Fcomprobantes
    Inherits conexion
    Dim cmd As New SqlCommand

    Public Function mostrar_comprobantes() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("mostrar_comprobantes")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@tipo_comprobante ", FrmComprobantes.cbocom.Text)

            cmd.Parameters.AddWithValue("@fechai", FrmComprobantes.txtfechai.Value)
            cmd.Parameters.AddWithValue("@fechafi", FrmComprobantes.txtfechafi.Value)


            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt



        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try


    End Function

End Class
