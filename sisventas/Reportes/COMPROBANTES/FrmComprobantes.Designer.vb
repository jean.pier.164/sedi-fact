﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmComprobantes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtcount = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtstotalcompra = New System.Windows.Forms.TextBox()
        Me.cbologin = New System.Windows.Forms.ComboBox()
        Me.txtfechafi = New System.Windows.Forms.DateTimePicker()
        Me.txtssventa = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnconsultar = New System.Windows.Forms.Button()
        Me.txtfechai = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.inexistente = New System.Windows.Forms.Label()
        Me.datalistado = New System.Windows.Forms.DataGridView()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbocom = New System.Windows.Forms.ComboBox()
        Me.btnimprimir = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox2.SuspendLayout()
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label6.Location = New System.Drawing.Point(855, 44)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(112, 16)
        Me.Label6.TabIndex = 41
        Me.Label6.Text = "N° de Registros :"
        '
        'txtcount
        '
        Me.txtcount.Location = New System.Drawing.Point(973, 41)
        Me.txtcount.Name = "txtcount"
        Me.txtcount.Size = New System.Drawing.Size(63, 22)
        Me.txtcount.TabIndex = 40
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label1.Location = New System.Drawing.Point(13, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 16)
        Me.Label1.TabIndex = 33
        Me.Label1.Text = "Login :"
        '
        'txtstotalcompra
        '
        Me.txtstotalcompra.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtstotalcompra.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.txtstotalcompra.Location = New System.Drawing.Point(1032, 678)
        Me.txtstotalcompra.Name = "txtstotalcompra"
        Me.txtstotalcompra.Size = New System.Drawing.Size(193, 30)
        Me.txtstotalcompra.TabIndex = 39
        '
        'cbologin
        '
        Me.cbologin.FormattingEnabled = True
        Me.cbologin.Items.AddRange(New Object() {"TODOS"})
        Me.cbologin.Location = New System.Drawing.Point(8, 39)
        Me.cbologin.Name = "cbologin"
        Me.cbologin.Size = New System.Drawing.Size(121, 24)
        Me.cbologin.TabIndex = 2
        Me.cbologin.Text = "TODOS"
        '
        'txtfechafi
        '
        Me.txtfechafi.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtfechafi.Location = New System.Drawing.Point(476, 39)
        Me.txtfechafi.Name = "txtfechafi"
        Me.txtfechafi.Size = New System.Drawing.Size(110, 22)
        Me.txtfechafi.TabIndex = 6
        '
        'txtssventa
        '
        Me.txtssventa.Font = New System.Drawing.Font("Arial Black", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtssventa.ForeColor = System.Drawing.Color.Blue
        Me.txtssventa.Location = New System.Drawing.Point(1214, 36)
        Me.txtssventa.Name = "txtssventa"
        Me.txtssventa.Size = New System.Drawing.Size(105, 30)
        Me.txtssventa.TabIndex = 50
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label7.Location = New System.Drawing.Point(510, 20)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(91, 16)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Fecha Final :"
        '
        'btnconsultar
        '
        Me.btnconsultar.Location = New System.Drawing.Point(645, 36)
        Me.btnconsultar.Name = "btnconsultar"
        Me.btnconsultar.Size = New System.Drawing.Size(75, 27)
        Me.btnconsultar.TabIndex = 8
        Me.btnconsultar.Text = "Consultar"
        Me.btnconsultar.UseVisualStyleBackColor = True
        '
        'txtfechai
        '
        Me.txtfechai.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtfechai.Location = New System.Drawing.Point(303, 41)
        Me.txtfechai.Name = "txtfechai"
        Me.txtfechai.Size = New System.Drawing.Size(121, 22)
        Me.txtfechai.TabIndex = 4
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.txtssventa)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.cbocom)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtcount)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtstotalcompra)
        Me.GroupBox2.Controls.Add(Me.cbologin)
        Me.GroupBox2.Controls.Add(Me.btnimprimir)
        Me.GroupBox2.Controls.Add(Me.txtfechafi)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.btnconsultar)
        Me.GroupBox2.Controls.Add(Me.inexistente)
        Me.GroupBox2.Controls.Add(Me.datalistado)
        Me.GroupBox2.Controls.Add(Me.txtfechai)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.GroupBox2.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1338, 625)
        Me.GroupBox2.TabIndex = 47
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Consultas"
        '
        'inexistente
        '
        Me.inexistente.AutoSize = True
        Me.inexistente.Location = New System.Drawing.Point(190, 171)
        Me.inexistente.Name = "inexistente"
        Me.inexistente.Size = New System.Drawing.Size(117, 16)
        Me.inexistente.TabIndex = 3
        Me.inexistente.Text = "Datos Inexistente"
        '
        'datalistado
        '
        Me.datalistado.AllowUserToAddRows = False
        Me.datalistado.AllowUserToDeleteRows = False
        Me.datalistado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.datalistado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.datalistado.Location = New System.Drawing.Point(3, 72)
        Me.datalistado.Name = "datalistado"
        Me.datalistado.ReadOnly = True
        Me.datalistado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.datalistado.Size = New System.Drawing.Size(1316, 518)
        Me.datalistado.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label8.Location = New System.Drawing.Point(300, 20)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(94, 16)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Fecha Inicio :"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label9.Location = New System.Drawing.Point(151, 17)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(108, 16)
        Me.Label9.TabIndex = 43
        Me.Label9.Text = "Comprobantes :"
        '
        'cbocom
        '
        Me.cbocom.FormattingEnabled = True
        Me.cbocom.Items.AddRange(New Object() {"TODOS", "BOLETAS", "FACTURAS"})
        Me.cbocom.Location = New System.Drawing.Point(138, 39)
        Me.cbocom.Name = "cbocom"
        Me.cbocom.Size = New System.Drawing.Size(121, 24)
        Me.cbocom.TabIndex = 42
        Me.cbocom.Text = "BOLETAS"
        '
        'btnimprimir
        '
        Me.btnimprimir.BackgroundImage = Global.BRAVOSPORT.My.Resources.Resources.Printer
        Me.btnimprimir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnimprimir.Location = New System.Drawing.Point(757, 36)
        Me.btnimprimir.Name = "btnimprimir"
        Me.btnimprimir.Size = New System.Drawing.Size(75, 30)
        Me.btnimprimir.TabIndex = 10
        Me.btnimprimir.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label2.Location = New System.Drawing.Point(1096, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 16)
        Me.Label2.TabIndex = 51
        Me.Label2.Text = "Suma Total"
        '
        'FrmComprobantes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1361, 632)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "FrmComprobantes"
        Me.Text = "FrmComprobantes"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label6 As Label
    Friend WithEvents txtcount As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents txtstotalcompra As TextBox
    Friend WithEvents cbologin As ComboBox
    Friend WithEvents btnimprimir As Button
    Friend WithEvents txtfechafi As DateTimePicker
    Friend WithEvents txtssventa As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents btnconsultar As Button
    Friend WithEvents txtfechai As DateTimePicker
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label9 As Label
    Friend WithEvents cbocom As ComboBox
    Friend WithEvents inexistente As Label
    Friend WithEvents datalistado As DataGridView
    Friend WithEvents Label8 As Label
    Friend WithEvents Label2 As Label
End Class
