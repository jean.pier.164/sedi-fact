﻿Imports System.Data.SqlClient
Public Class Ffamilia
    Inherits conexion

    Dim cmd As New SqlCommand


    Public Function mostrar() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("mostrar_familia")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function




    Public Function insertar(ByVal dts As vfamilia) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("insertar_familia")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@nombre_familia", dts.gnombre_familia)



            If cmd.ExecuteNonQuery Then
                Return True

            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try

    End Function


    Public Function editar(ByVal dts As vfamilia) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("editar_familia")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@idfamilia", dts.gidfamilia)
            cmd.Parameters.AddWithValue("@nombre_familia", dts.gnombre_familia)

            If cmd.ExecuteNonQuery Then
                Return True

            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try

    End Function

    Public Function eliminar(ByVal dts As vfamilia) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("eliminar_familia")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.Add("@idfamilia", SqlDbType.NVarChar, 50).Value = dts.gidfamilia

            If cmd.ExecuteNonQuery Then
                Return True
            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False

        End Try
    End Function
End Class
