﻿Imports System.Configuration
Imports System.Data.SqlClient
Public Class conexion
    Protected cnn As New SqlConnection

    Public idusuario As Integer
    Shared cadenaConexion As String = ConfigurationManager.ConnectionStrings("BRAVOSPORT.My.MySettings.marketConnectionString").ConnectionString

    Protected Function conectado()
        Try
            'cnn = New SqlConnection("data source=(local);initial catalog=market;integrated security=true")
            cnn = New SqlConnection(cadenaConexion)
            cnn.Open()
            Return True

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        End Try

    End Function


    Protected Function desconectado()
        Try
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
                Return True
            Else
                Return False


            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return False


        End Try
    End Function
End Class
