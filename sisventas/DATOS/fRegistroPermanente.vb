﻿Imports System.Data.SqlClient
Public Class fRegistroPermanente
    Inherits conexion
    Dim cmd As New SqlCommand

    Public Function mostrar_RegistroPermanente() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("registro_permanente")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@nombre_producto", frmcPermanenteValorizado.cbonombre_producto.Text)



            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt



        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try


    End Function

End Class
