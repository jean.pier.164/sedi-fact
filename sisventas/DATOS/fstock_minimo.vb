﻿

Imports System.Data.SqlClient

Public Class fstock_minimo
    Inherits conexion

    Dim cmd As New SqlCommand

    Public Function mostrar_stockminimo() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("stock_minimo")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function

End Class
