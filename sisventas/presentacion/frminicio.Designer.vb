﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frminicio
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frminicio))
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.lbluser = New System.Windows.Forms.Label()
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.lblLicenciaMsje = New System.Windows.Forms.Label()
        Me.IngresosXFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.INToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RegistroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarVentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ResumenDiarioBoletasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoResumenesDiariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComunicacionDeBajaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoComprobantesSunatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaComprobantesPendienteEnvíoSunatToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotaCreditoMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NotaDebitoMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDeVentasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComprasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresosFacturasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CATALOGOToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArticulosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoPreciosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CategoriasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PresentacionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FamiliaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BULTOSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SUBCATEGORIAToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VentasToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.StockMinimoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EgresosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.A4ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MMToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaDePreciosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.COMPROBANTESToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CierrasCajaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AperturaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RetirarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CierresCajaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArchivosFEStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.StatusStrip.SuspendLayout()
        Me.MenuStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'lbluser
        '
        Me.lbluser.AutoSize = True
        Me.lbluser.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbluser.Location = New System.Drawing.Point(32, 51)
        Me.lbluser.Name = "lbluser"
        Me.lbluser.Size = New System.Drawing.Size(55, 16)
        Me.lbluser.TabIndex = 9
        Me.lbluser.Text = "Label1"
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Font = New System.Drawing.Font("Segoe UI", 10.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(53, 19)
        Me.ToolStripStatusLabel.Text = "Estado"
        '
        'StatusStrip
        '
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 439)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(1370, 24)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'lblLicenciaMsje
        '
        Me.lblLicenciaMsje.AutoSize = True
        Me.lblLicenciaMsje.Location = New System.Drawing.Point(927, 450)
        Me.lblLicenciaMsje.Name = "lblLicenciaMsje"
        Me.lblLicenciaMsje.Size = New System.Drawing.Size(0, 13)
        Me.lblLicenciaMsje.TabIndex = 11
        '
        'IngresosXFacturasToolStripMenuItem
        '
        Me.IngresosXFacturasToolStripMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources._1043188_200
        Me.IngresosXFacturasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.IngresosXFacturasToolStripMenuItem.Name = "IngresosXFacturasToolStripMenuItem"
        Me.IngresosXFacturasToolStripMenuItem.Size = New System.Drawing.Size(249, 56)
        Me.IngresosXFacturasToolStripMenuItem.Text = "Ingresos x Facturas"
        '
        'INToolStripMenuItem
        '
        Me.INToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.INToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Desktop
        Me.INToolStripMenuItem.Image = CType(resources.GetObject("INToolStripMenuItem.Image"), System.Drawing.Image)
        Me.INToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.INToolStripMenuItem.Name = "INToolStripMenuItem"
        Me.INToolStripMenuItem.Size = New System.Drawing.Size(129, 54)
        Me.INToolStripMenuItem.Text = "Clientes"
        '
        'VentasToolStripMenuItem
        '
        Me.VentasToolStripMenuItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.VentasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RegistroToolStripMenuItem, Me.ModificarVentasToolStripMenuItem, Me.ResumenDiarioBoletasToolStripMenuItem, Me.ListadoResumenesDiariosToolStripMenuItem, Me.ComunicacionDeBajaToolStripMenuItem, Me.ListadoComprobantesSunatToolStripMenuItem, Me.ListaComprobantesPendienteEnvíoSunatToolStripMenuItem, Me.NotaCreditoMenuItem, Me.NotaDebitoMenuItem, Me.ListadoDeVentasToolStripMenuItem})
        Me.VentasToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.VentasToolStripMenuItem.ForeColor = System.Drawing.Color.DodgerBlue
        Me.VentasToolStripMenuItem.Image = CType(resources.GetObject("VentasToolStripMenuItem.Image"), System.Drawing.Image)
        Me.VentasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.VentasToolStripMenuItem.Name = "VentasToolStripMenuItem"
        Me.VentasToolStripMenuItem.Size = New System.Drawing.Size(117, 54)
        Me.VentasToolStripMenuItem.Text = "Ventas"
        '
        'RegistroToolStripMenuItem
        '
        Me.RegistroToolStripMenuItem.Image = CType(resources.GetObject("RegistroToolStripMenuItem.Image"), System.Drawing.Image)
        Me.RegistroToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.RegistroToolStripMenuItem.Name = "RegistroToolStripMenuItem"
        Me.RegistroToolStripMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.RegistroToolStripMenuItem.Text = "Realizar Ventas"
        '
        'ModificarVentasToolStripMenuItem
        '
        Me.ModificarVentasToolStripMenuItem.Image = CType(resources.GetObject("ModificarVentasToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ModificarVentasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ModificarVentasToolStripMenuItem.Name = "ModificarVentasToolStripMenuItem"
        Me.ModificarVentasToolStripMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.ModificarVentasToolStripMenuItem.Text = "Modificar Ventas"
        '
        'ResumenDiarioBoletasToolStripMenuItem
        '
        Me.ResumenDiarioBoletasToolStripMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.resumen_resize
        Me.ResumenDiarioBoletasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ResumenDiarioBoletasToolStripMenuItem.Name = "ResumenDiarioBoletasToolStripMenuItem"
        Me.ResumenDiarioBoletasToolStripMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.ResumenDiarioBoletasToolStripMenuItem.Text = "Resumen Diario Boletas"
        '
        'ListadoResumenesDiariosToolStripMenuItem
        '
        Me.ListadoResumenesDiariosToolStripMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.resumen_2
        Me.ListadoResumenesDiariosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ListadoResumenesDiariosToolStripMenuItem.Name = "ListadoResumenesDiariosToolStripMenuItem"
        Me.ListadoResumenesDiariosToolStripMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.ListadoResumenesDiariosToolStripMenuItem.Text = "Listado Resumenes Diarios"
        Me.ListadoResumenesDiariosToolStripMenuItem.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        '
        'ComunicacionDeBajaToolStripMenuItem
        '
        Me.ComunicacionDeBajaToolStripMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.baja_
        Me.ComunicacionDeBajaToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ComunicacionDeBajaToolStripMenuItem.Name = "ComunicacionDeBajaToolStripMenuItem"
        Me.ComunicacionDeBajaToolStripMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.ComunicacionDeBajaToolStripMenuItem.Text = "Comunicacion de Baja"
        '
        'ListadoComprobantesSunatToolStripMenuItem
        '
        Me.ListadoComprobantesSunatToolStripMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.Bill__1_
        Me.ListadoComprobantesSunatToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ListadoComprobantesSunatToolStripMenuItem.Name = "ListadoComprobantesSunatToolStripMenuItem"
        Me.ListadoComprobantesSunatToolStripMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.ListadoComprobantesSunatToolStripMenuItem.Text = "Listado Comprobantes Sunat"
        '
        'ListaComprobantesPendienteEnvíoSunatToolStripMenuItem
        '
        Me.ListaComprobantesPendienteEnvíoSunatToolStripMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.Updating_Invoices
        Me.ListaComprobantesPendienteEnvíoSunatToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ListaComprobantesPendienteEnvíoSunatToolStripMenuItem.Name = "ListaComprobantesPendienteEnvíoSunatToolStripMenuItem"
        Me.ListaComprobantesPendienteEnvíoSunatToolStripMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.ListaComprobantesPendienteEnvíoSunatToolStripMenuItem.Text = "Lista Comprobantes Pendiente Envío Sunat"
        '
        'NotaCreditoMenuItem
        '
        Me.NotaCreditoMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.img_documento
        Me.NotaCreditoMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.NotaCreditoMenuItem.Name = "NotaCreditoMenuItem"
        Me.NotaCreditoMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.NotaCreditoMenuItem.Text = "Nota de Crédito"
        '
        'NotaDebitoMenuItem
        '
        Me.NotaDebitoMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.f_icon_c4
        Me.NotaDebitoMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.NotaDebitoMenuItem.Name = "NotaDebitoMenuItem"
        Me.NotaDebitoMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.NotaDebitoMenuItem.Text = "Nota de Débito"
        '
        'ListadoDeVentasToolStripMenuItem
        '
        Me.ListadoDeVentasToolStripMenuItem.Name = "ListadoDeVentasToolStripMenuItem"
        Me.ListadoDeVentasToolStripMenuItem.Size = New System.Drawing.Size(419, 60)
        Me.ListadoDeVentasToolStripMenuItem.Text = "Listado de Ventas"
        '
        'ClientesToolStripMenuItem
        '
        Me.ClientesToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold)
        Me.ClientesToolStripMenuItem.ForeColor = System.Drawing.Color.SeaGreen
        Me.ClientesToolStripMenuItem.Image = CType(resources.GetObject("ClientesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ClientesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ClientesToolStripMenuItem.Name = "ClientesToolStripMenuItem"
        Me.ClientesToolStripMenuItem.Size = New System.Drawing.Size(146, 54)
        Me.ClientesToolStripMenuItem.Text = "Proveedor"
        '
        'ComprasToolStripMenuItem
        '
        Me.ComprasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresosFacturasToolStripMenuItem})
        Me.ComprasToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold)
        Me.ComprasToolStripMenuItem.ForeColor = System.Drawing.Color.DarkOrange
        Me.ComprasToolStripMenuItem.Image = CType(resources.GetObject("ComprasToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ComprasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ComprasToolStripMenuItem.Name = "ComprasToolStripMenuItem"
        Me.ComprasToolStripMenuItem.Size = New System.Drawing.Size(133, 54)
        Me.ComprasToolStripMenuItem.Text = "Compras"
        '
        'IngresosFacturasToolStripMenuItem
        '
        Me.IngresosFacturasToolStripMenuItem.Name = "IngresosFacturasToolStripMenuItem"
        Me.IngresosFacturasToolStripMenuItem.Size = New System.Drawing.Size(203, 22)
        Me.IngresosFacturasToolStripMenuItem.Text = "Ingresos Facturas"
        '
        'CATALOGOToolStripMenuItem
        '
        Me.CATALOGOToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArticulosToolStripMenuItem, Me.CategoriasToolStripMenuItem, Me.PresentacionToolStripMenuItem, Me.FamiliaToolStripMenuItem, Me.BULTOSToolStripMenuItem, Me.SUBCATEGORIAToolStripMenuItem})
        Me.CATALOGOToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold)
        Me.CATALOGOToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Desktop
        Me.CATALOGOToolStripMenuItem.Image = CType(resources.GetObject("CATALOGOToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CATALOGOToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.CATALOGOToolStripMenuItem.Name = "CATALOGOToolStripMenuItem"
        Me.CATALOGOToolStripMenuItem.Size = New System.Drawing.Size(133, 54)
        Me.CATALOGOToolStripMenuItem.Text = "Catalogo"
        '
        'ArticulosToolStripMenuItem
        '
        Me.ArticulosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListadoPreciosToolStripMenuItem})
        Me.ArticulosToolStripMenuItem.Image = CType(resources.GetObject("ArticulosToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ArticulosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ArticulosToolStripMenuItem.Name = "ArticulosToolStripMenuItem"
        Me.ArticulosToolStripMenuItem.Size = New System.Drawing.Size(215, 56)
        Me.ArticulosToolStripMenuItem.Text = "Productos"
        '
        'ListadoPreciosToolStripMenuItem
        '
        Me.ListadoPreciosToolStripMenuItem.Image = Global.BRAVOSPORT.My.Resources.Resources.resumen_2
        Me.ListadoPreciosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ListadoPreciosToolStripMenuItem.Name = "ListadoPreciosToolStripMenuItem"
        Me.ListadoPreciosToolStripMenuItem.Size = New System.Drawing.Size(220, 56)
        Me.ListadoPreciosToolStripMenuItem.Text = "Listado Precios"
        '
        'CategoriasToolStripMenuItem
        '
        Me.CategoriasToolStripMenuItem.Image = CType(resources.GetObject("CategoriasToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CategoriasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.CategoriasToolStripMenuItem.Name = "CategoriasToolStripMenuItem"
        Me.CategoriasToolStripMenuItem.Size = New System.Drawing.Size(215, 56)
        Me.CategoriasToolStripMenuItem.Text = "Categorias"
        '
        'PresentacionToolStripMenuItem
        '
        Me.PresentacionToolStripMenuItem.Image = CType(resources.GetObject("PresentacionToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PresentacionToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.PresentacionToolStripMenuItem.Name = "PresentacionToolStripMenuItem"
        Me.PresentacionToolStripMenuItem.Size = New System.Drawing.Size(215, 56)
        Me.PresentacionToolStripMenuItem.Text = "Presentacion"
        '
        'FamiliaToolStripMenuItem
        '
        Me.FamiliaToolStripMenuItem.Name = "FamiliaToolStripMenuItem"
        Me.FamiliaToolStripMenuItem.Size = New System.Drawing.Size(215, 56)
        Me.FamiliaToolStripMenuItem.Text = "Familia"
        '
        'BULTOSToolStripMenuItem
        '
        Me.BULTOSToolStripMenuItem.Name = "BULTOSToolStripMenuItem"
        Me.BULTOSToolStripMenuItem.Size = New System.Drawing.Size(215, 56)
        Me.BULTOSToolStripMenuItem.Text = "Bultos"
        '
        'SUBCATEGORIAToolStripMenuItem
        '
        Me.SUBCATEGORIAToolStripMenuItem.Name = "SUBCATEGORIAToolStripMenuItem"
        Me.SUBCATEGORIAToolStripMenuItem.Size = New System.Drawing.Size(215, 56)
        Me.SUBCATEGORIAToolStripMenuItem.Text = "Sub_Categoria"
        '
        'ConsultasToolStripMenuItem
        '
        Me.ConsultasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VentasToolStripMenuItem1, Me.ProductosToolStripMenuItem1, Me.StockMinimoToolStripMenuItem, Me.EgresosToolStripMenuItem})
        Me.ConsultasToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ConsultasToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Desktop
        Me.ConsultasToolStripMenuItem.Image = CType(resources.GetObject("ConsultasToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ConsultasToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ConsultasToolStripMenuItem.Name = "ConsultasToolStripMenuItem"
        Me.ConsultasToolStripMenuItem.Size = New System.Drawing.Size(141, 54)
        Me.ConsultasToolStripMenuItem.Text = "Consultas"
        '
        'VentasToolStripMenuItem1
        '
        Me.VentasToolStripMenuItem1.Name = "VentasToolStripMenuItem1"
        Me.VentasToolStripMenuItem1.Size = New System.Drawing.Size(171, 22)
        Me.VentasToolStripMenuItem1.Text = "Ventas"
        '
        'ProductosToolStripMenuItem1
        '
        Me.ProductosToolStripMenuItem1.Name = "ProductosToolStripMenuItem1"
        Me.ProductosToolStripMenuItem1.Size = New System.Drawing.Size(171, 22)
        Me.ProductosToolStripMenuItem1.Text = "Productos"
        '
        'StockMinimoToolStripMenuItem
        '
        Me.StockMinimoToolStripMenuItem.Name = "StockMinimoToolStripMenuItem"
        Me.StockMinimoToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.StockMinimoToolStripMenuItem.Text = "Stock Minimo"
        '
        'EgresosToolStripMenuItem
        '
        Me.EgresosToolStripMenuItem.Name = "EgresosToolStripMenuItem"
        Me.EgresosToolStripMenuItem.Size = New System.Drawing.Size(171, 22)
        Me.EgresosToolStripMenuItem.Text = "Egresos"
        '
        'ReportesToolStripMenuItem
        '
        Me.ReportesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProductosToolStripMenuItem2, Me.ListaDePreciosToolStripMenuItem, Me.COMPROBANTESToolStripMenuItem})
        Me.ReportesToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReportesToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Desktop
        Me.ReportesToolStripMenuItem.Image = CType(resources.GetObject("ReportesToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ReportesToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ReportesToolStripMenuItem.Name = "ReportesToolStripMenuItem"
        Me.ReportesToolStripMenuItem.Size = New System.Drawing.Size(136, 54)
        Me.ReportesToolStripMenuItem.Text = "Reportes"
        '
        'ProductosToolStripMenuItem2
        '
        Me.ProductosToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.A4ToolStripMenuItem, Me.MMToolStripMenuItem})
        Me.ProductosToolStripMenuItem2.Name = "ProductosToolStripMenuItem2"
        Me.ProductosToolStripMenuItem2.Size = New System.Drawing.Size(206, 22)
        Me.ProductosToolStripMenuItem2.Text = "Productos"
        '
        'A4ToolStripMenuItem
        '
        Me.A4ToolStripMenuItem.Name = "A4ToolStripMenuItem"
        Me.A4ToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.A4ToolStripMenuItem.Text = "A4"
        '
        'MMToolStripMenuItem
        '
        Me.MMToolStripMenuItem.Name = "MMToolStripMenuItem"
        Me.MMToolStripMenuItem.Size = New System.Drawing.Size(118, 22)
        Me.MMToolStripMenuItem.Text = "80MM"
        '
        'ListaDePreciosToolStripMenuItem
        '
        Me.ListaDePreciosToolStripMenuItem.Name = "ListaDePreciosToolStripMenuItem"
        Me.ListaDePreciosToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.ListaDePreciosToolStripMenuItem.Text = "Lista de Precios"
        '
        'COMPROBANTESToolStripMenuItem
        '
        Me.COMPROBANTESToolStripMenuItem.Name = "COMPROBANTESToolStripMenuItem"
        Me.COMPROBANTESToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.COMPROBANTESToolStripMenuItem.Text = "COMPROBANTES"
        '
        'CierrasCajaToolStripMenuItem
        '
        Me.CierrasCajaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AperturaToolStripMenuItem, Me.RetirarToolStripMenuItem, Me.CierresCajaToolStripMenuItem})
        Me.CierrasCajaToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CierrasCajaToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Desktop
        Me.CierrasCajaToolStripMenuItem.Image = CType(resources.GetObject("CierrasCajaToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CierrasCajaToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.CierrasCajaToolStripMenuItem.Name = "CierrasCajaToolStripMenuItem"
        Me.CierrasCajaToolStripMenuItem.Size = New System.Drawing.Size(157, 54)
        Me.CierrasCajaToolStripMenuItem.Text = "Cierras Caja"
        '
        'AperturaToolStripMenuItem
        '
        Me.AperturaToolStripMenuItem.Name = "AperturaToolStripMenuItem"
        Me.AperturaToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.AperturaToolStripMenuItem.Text = "Apertura"
        '
        'RetirarToolStripMenuItem
        '
        Me.RetirarToolStripMenuItem.Name = "RetirarToolStripMenuItem"
        Me.RetirarToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.RetirarToolStripMenuItem.Text = "Retirar Dinero"
        '
        'CierresCajaToolStripMenuItem
        '
        Me.CierresCajaToolStripMenuItem.Name = "CierresCajaToolStripMenuItem"
        Me.CierresCajaToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.CierresCajaToolStripMenuItem.Text = "Cierres Caja"
        '
        'UsuariosToolStripMenuItem
        '
        Me.UsuariosToolStripMenuItem.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.UsuariosToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Desktop
        Me.UsuariosToolStripMenuItem.Image = CType(resources.GetObject("UsuariosToolStripMenuItem.Image"), System.Drawing.Image)
        Me.UsuariosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.UsuariosToolStripMenuItem.Name = "UsuariosToolStripMenuItem"
        Me.UsuariosToolStripMenuItem.Size = New System.Drawing.Size(133, 54)
        Me.UsuariosToolStripMenuItem.Text = "Usuarios"
        '
        'ArchivosFEStripMenuItem1
        '
        Me.ArchivosFEStripMenuItem1.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold)
        Me.ArchivosFEStripMenuItem1.ForeColor = System.Drawing.SystemColors.Desktop
        Me.ArchivosFEStripMenuItem1.Image = Global.BRAVOSPORT.My.Resources.Resources._62319
        Me.ArchivosFEStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ArchivosFEStripMenuItem1.Name = "ArchivosFEStripMenuItem1"
        Me.ArchivosFEStripMenuItem1.Size = New System.Drawing.Size(154, 54)
        Me.ArchivosFEStripMenuItem1.Text = "Archivos Fe"
        '
        'MenuStrip
        '
        Me.MenuStrip.AutoSize = False
        Me.MenuStrip.BackColor = System.Drawing.Color.White
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.INToolStripMenuItem, Me.VentasToolStripMenuItem, Me.ClientesToolStripMenuItem, Me.ComprasToolStripMenuItem, Me.CATALOGOToolStripMenuItem, Me.ConsultasToolStripMenuItem, Me.ReportesToolStripMenuItem, Me.CierrasCajaToolStripMenuItem, Me.UsuariosToolStripMenuItem, Me.ArchivosFEStripMenuItem1})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.MdiWindowListItem = Me.VentasToolStripMenuItem
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Size = New System.Drawing.Size(1370, 58)
        Me.MenuStrip.TabIndex = 5
        Me.MenuStrip.Text = "MenuStrip"
        '
        'frminicio
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.HighlightText
        Me.ClientSize = New System.Drawing.Size(1370, 463)
        Me.Controls.Add(Me.lblLicenciaMsje)
        Me.Controls.Add(Me.lbluser)
        Me.Controls.Add(Me.MenuStrip)
        Me.Controls.Add(Me.StatusStrip)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.Location = New System.Drawing.Point(500, 50)
        Me.MainMenuStrip = Me.MenuStrip
        Me.Name = "frminicio"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = ".: Sistema de Ventas :."
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents lbluser As System.Windows.Forms.Label
    Friend WithEvents ToolStripStatusLabel As ToolStripStatusLabel
    Friend WithEvents StatusStrip As StatusStrip
    Friend WithEvents lblLicenciaMsje As Label
    Friend WithEvents IngresosXFacturasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents INToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RegistroToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ModificarVentasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ResumenDiarioBoletasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListadoResumenesDiariosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ComunicacionDeBajaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListadoComprobantesSunatToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListaComprobantesPendienteEnvíoSunatToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NotaCreditoMenuItem As ToolStripMenuItem
    Friend WithEvents NotaDebitoMenuItem As ToolStripMenuItem
    Friend WithEvents ListadoDeVentasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ClientesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ComprasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents IngresosFacturasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CATALOGOToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ArticulosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListadoPreciosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CategoriasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PresentacionToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsultasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents VentasToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ProductosToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents StockMinimoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EgresosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReportesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProductosToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents A4ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MMToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListaDePreciosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents COMPROBANTESToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CierrasCajaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AperturaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RetirarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CierresCajaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UsuariosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ArchivosFEStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents MenuStrip As MenuStrip
    Friend WithEvents FamiliaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SUBCATEGORIAToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BULTOSToolStripMenuItem As ToolStripMenuItem
End Class
