﻿Imports System.Data.SqlClient
Imports System.Configuration

Public Class FrmCierreCaja

    Shared conexion As New SqlConnection(ConfigurationManager.ConnectionStrings("BRAVOSPORT.My.MySettings.marketConnectionString").ConnectionString)
    Private dt As New DataTable
    Private dt1 As New DataTable
    Private dt2 As New DataTable
    Dim variable As SqlDataReader
    Dim consulta3 As New SqlCommand
    Private Sub btnbuscar_Click(sender As Object, e As EventArgs) Handles btnbuscar.Click


        Try
            'instanciamos la clase f... de la carpeta datos y llamamos a la función mostrar
            Dim func As New FcierreCaja
            dt = func.mostraringreso_usuario(cbousuario.Text, txtfecha_inicio.Value, txtfecha_fin.Value)
            'ocultamos la columna donde aparece el check de eliminación
            ' datalistado.Columns.Item("Eliminar").Visible = False

            'si hay registros entonces llenamos la lista con los registros, mostramos las columas del datagridview datalistado
            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt
                datalistado.ColumnHeadersVisible = True
                lblInexistente.Visible = False



                Dim suma As Decimal
                For Each row As DataGridViewRow In datalistado.Rows

                    ' Valor de la celda Monto.
                    Dim valor As Object = row.Cells(6).Value

                    suma += CDec(valor)


                Next
                'lbltconsumo.Text = "Total Consumo: S/. " & suma
                txtconsumo.Text = suma
            Else
                'si no hay registros entonces no mostramos nada y mostramos el mensaje de datos inexistentes y ocultamos las columas
                datalistado.DataSource = Nothing
                datalistado.ColumnHeadersVisible = False

                lblInexistente.Visible = True
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Try
            'instanciamos la clase f... de la carpeta datos y llamamos a la función mostrar
            Dim func2 As New FcierreCaja
            dt2 = func2.mostrarapertura_usuario(cbousuario.Text, txtfecha_inicio.Value, txtfecha_fin.Value)
            'ocultamos la columna donde aparece el check de eliminación


            'si hay registros entonces llenamos la lista con los registros, mostramos las columas del datagridview datalistado
            If dt2.Rows.Count <> 0 Then
                dataapertura.DataSource = dt2
                dataapertura.ColumnHeadersVisible = True


                Dim suma As Decimal
                For Each row1 As DataGridViewRow In dataapertura.Rows

                    ' Valor de la celda Monto.
                    Dim valor As Object = row1.Cells(2).Value

                    suma += CDec(valor)


                Next
                'lbltcaja.Text = "Caja: S/. " & suma
                txtapertura.Text = suma

            Else
                'si no hay registros entonces no mostramos nada y mostramos el mensaje de datos inexistentes y ocultamos las columas
                dataapertura.DataSource = Nothing
                dataapertura.ColumnHeadersVisible = False


            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try






        Try
            'instanciamos la clase f... de la carpeta datos y llamamos a la función mostrar
            Dim func As New FcierreCaja
            dt = func.mostraregresos_usuario(cbousuario.Text, txtfecha_inicio.Value, txtfecha_fin.Value)
            'ocultamos la columna donde aparece el check de eliminación


            'si hay registros entonces llenamos la lista con los registros, mostramos las columas del datagridview datalistado
            If dt.Rows.Count <> 0 Then
                dataegresos.DataSource = dt
                dataegresos.ColumnHeadersVisible = True


                Dim suma As Decimal
                For Each row As DataGridViewRow In dataegresos.Rows

                    ' Valor de la celda Monto.
                    Dim valor As Object = row.Cells(3).Value

                    suma += CDec(valor)


                Next
                'lblsalidacaja.Text = "Salida Caja: S/. " & suma
                txtsalidacaja.Text = suma
            Else
                'si no hay registros entonces no mostramos nada y mostramos el mensaje de datos inexistentes y ocultamos las columas
                dataegresos.DataSource = Nothing
                dataegresos.ColumnHeadersVisible = False


            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Dim sumatotal As Decimal
        Dim a As Decimal
        Dim b As Decimal
        Dim c As Decimal
        Dim d As Decimal
        a = txtconsumo.Text
        b = txtapertura.Text

        d = txtsalidacaja.Text

        sumatotal = (a + b) - d
        txtcierrecaja.Text = sumatotal

        Dim mycount As Integer

        mycount = datalistado.Rows.Count()
        txtcount.Text = mycount
    End Sub

    Private Sub btnImprimir_Click(sender As Object, e As EventArgs) Handles btnImprimir.Click


        CIERRECAJA.txtfecha_ini.Text = txtfecha_inicio.Value
        CIERRECAJA.txtfecha_fi.Text = txtfecha_fin.Value
        CIERRECAJA.txtlogin.Text = cbousuario.Text
        CIERRECAJA.ShowDialog()


    End Sub

    Private Sub btnSalir_Click(sender As Object, e As EventArgs) Handles btnSalir.Click

    End Sub

    Private Sub FrmCierreCaja_Load(sender As Object, e As EventArgs) Handles MyBase.Load


        Dim mycount As Integer

        mycount = datalistado.Rows.Count()
        txtcount.Text = mycount


        cbousuario.Text = "TODOS"
        consulta3.CommandType = CommandType.Text
        consulta3.CommandText = ("select login from usuario")
        consulta3.Connection = (conexion)
        conexion.Open()
        variable = consulta3.ExecuteReader

        While variable.Read = True
            cbousuario.Items.Add(variable.Item(0))
        End While

        conexion.Close()
    End Sub

    Private Sub cbousuario_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbousuario.SelectedIndexChanged

    End Sub
End Class