﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmCierreCaja
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dataapertura = New System.Windows.Forms.DataGridView()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtcierrecaja = New System.Windows.Forms.TextBox()
        Me.txtsalidacaja = New System.Windows.Forms.TextBox()
        Me.txtconsumo = New System.Windows.Forms.TextBox()
        Me.txtapertura = New System.Windows.Forms.TextBox()
        Me.dataegresos = New System.Windows.Forms.DataGridView()
        Me.lblInexistente = New System.Windows.Forms.Label()
        Me.datalistado = New System.Windows.Forms.DataGridView()
        Me.txtfecha_inicio = New System.Windows.Forms.DateTimePicker()
        Me.txtfecha_fin = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbousuario = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnbuscar = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtcount = New System.Windows.Forms.TextBox()
        Me.datadetalleventa = New System.Windows.Forms.DataGridView()
        CType(Me.dataapertura, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.dataegresos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.datadetalleventa, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dataapertura
        '
        Me.dataapertura.AllowUserToAddRows = False
        Me.dataapertura.AllowUserToDeleteRows = False
        Me.dataapertura.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dataapertura.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dataapertura.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataapertura.Location = New System.Drawing.Point(882, 12)
        Me.dataapertura.Name = "dataapertura"
        Me.dataapertura.ReadOnly = True
        Me.dataapertura.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dataapertura.Size = New System.Drawing.Size(333, 98)
        Me.dataapertura.TabIndex = 71
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Location = New System.Drawing.Point(16, 116)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1476, 527)
        Me.TabControl1.TabIndex = 72
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.txtcierrecaja)
        Me.TabPage1.Controls.Add(Me.txtsalidacaja)
        Me.TabPage1.Controls.Add(Me.txtconsumo)
        Me.TabPage1.Controls.Add(Me.txtapertura)
        Me.TabPage1.Controls.Add(Me.dataegresos)
        Me.TabPage1.Controls.Add(Me.lblInexistente)
        Me.TabPage1.Controls.Add(Me.datalistado)
        Me.TabPage1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1468, 501)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Pagos"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(228, 452)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(80, 16)
        Me.Label11.TabIndex = 25
        Me.Label11.Text = "Total Caja"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.Color.Red
        Me.Label10.Location = New System.Drawing.Point(6, 7)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(103, 16)
        Me.Label10.TabIndex = 24
        Me.Label10.Text = "Salida Dinero"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(506, 3)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(126, 16)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Ventas/Servicios"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(205, 416)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(103, 16)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = "Salida Dinero"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(166, 379)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(142, 16)
        Me.Label3.TabIndex = 19
        Me.Label3.Text = "Consumo/Servicios"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(205, 346)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(103, 16)
        Me.Label2.TabIndex = 18
        Me.Label2.Text = "Apertura Caja"
        '
        'txtcierrecaja
        '
        Me.txtcierrecaja.Location = New System.Drawing.Point(325, 449)
        Me.txtcierrecaja.Name = "txtcierrecaja"
        Me.txtcierrecaja.Size = New System.Drawing.Size(100, 22)
        Me.txtcierrecaja.TabIndex = 17
        Me.txtcierrecaja.Text = "0"
        '
        'txtsalidacaja
        '
        Me.txtsalidacaja.ForeColor = System.Drawing.Color.Red
        Me.txtsalidacaja.Location = New System.Drawing.Point(325, 410)
        Me.txtsalidacaja.Name = "txtsalidacaja"
        Me.txtsalidacaja.Size = New System.Drawing.Size(100, 22)
        Me.txtsalidacaja.TabIndex = 16
        Me.txtsalidacaja.Text = "0"
        '
        'txtconsumo
        '
        Me.txtconsumo.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.txtconsumo.Location = New System.Drawing.Point(325, 379)
        Me.txtconsumo.Name = "txtconsumo"
        Me.txtconsumo.Size = New System.Drawing.Size(100, 22)
        Me.txtconsumo.TabIndex = 14
        Me.txtconsumo.Text = "0"
        '
        'txtapertura
        '
        Me.txtapertura.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.txtapertura.Location = New System.Drawing.Point(325, 346)
        Me.txtapertura.Name = "txtapertura"
        Me.txtapertura.Size = New System.Drawing.Size(100, 22)
        Me.txtapertura.TabIndex = 13
        Me.txtapertura.Text = "0"
        '
        'dataegresos
        '
        Me.dataegresos.AllowUserToAddRows = False
        Me.dataegresos.AllowUserToDeleteRows = False
        Me.dataegresos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dataegresos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dataegresos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataegresos.Location = New System.Drawing.Point(6, 26)
        Me.dataegresos.Name = "dataegresos"
        Me.dataegresos.ReadOnly = True
        Me.dataegresos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dataegresos.Size = New System.Drawing.Size(497, 314)
        Me.dataegresos.TabIndex = 9
        '
        'lblInexistente
        '
        Me.lblInexistente.AutoSize = True
        Me.lblInexistente.Location = New System.Drawing.Point(772, 132)
        Me.lblInexistente.Name = "lblInexistente"
        Me.lblInexistente.Size = New System.Drawing.Size(135, 16)
        Me.lblInexistente.TabIndex = 6
        Me.lblInexistente.Text = "Datos Inexistentes"
        '
        'datalistado
        '
        Me.datalistado.AllowUserToAddRows = False
        Me.datalistado.AllowUserToDeleteRows = False
        Me.datalistado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.datalistado.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.datalistado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.datalistado.Location = New System.Drawing.Point(509, 28)
        Me.datalistado.Name = "datalistado"
        Me.datalistado.ReadOnly = True
        Me.datalistado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.datalistado.Size = New System.Drawing.Size(950, 453)
        Me.datalistado.TabIndex = 3
        '
        'txtfecha_inicio
        '
        Me.txtfecha_inicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtfecha_inicio.Location = New System.Drawing.Point(215, 30)
        Me.txtfecha_inicio.Name = "txtfecha_inicio"
        Me.txtfecha_inicio.Size = New System.Drawing.Size(124, 22)
        Me.txtfecha_inicio.TabIndex = 55
        '
        'txtfecha_fin
        '
        Me.txtfecha_fin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.txtfecha_fin.Location = New System.Drawing.Point(384, 29)
        Me.txtfecha_fin.Name = "txtfecha_fin"
        Me.txtfecha_fin.Size = New System.Drawing.Size(100, 22)
        Me.txtfecha_fin.TabIndex = 57
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(216, 4)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(96, 16)
        Me.Label6.TabIndex = 54
        Me.Label6.Text = "Fecha Inicio:"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.White
        Me.GroupBox2.Controls.Add(Me.cbousuario)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.btnSalir)
        Me.GroupBox2.Controls.Add(Me.btnbuscar)
        Me.GroupBox2.Controls.Add(Me.btnImprimir)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.txtfecha_inicio)
        Me.GroupBox2.Controls.Add(Me.txtfecha_fin)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(16, 16)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(838, 94)
        Me.GroupBox2.TabIndex = 73
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Cierres de Caja"
        '
        'cbousuario
        '
        Me.cbousuario.FormattingEnabled = True
        Me.cbousuario.Location = New System.Drawing.Point(33, 32)
        Me.cbousuario.Name = "cbousuario"
        Me.cbousuario.Size = New System.Drawing.Size(130, 24)
        Me.cbousuario.TabIndex = 62
        Me.cbousuario.Text = "TODOS"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(39, 13)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(70, 16)
        Me.Label12.TabIndex = 61
        Me.Label12.Text = "Usuario :"
        '
        'btnSalir
        '
        Me.btnSalir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSalir.Location = New System.Drawing.Point(712, 17)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(88, 34)
        Me.btnSalir.TabIndex = 9
        Me.btnSalir.Text = "Salir"
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnbuscar
        '
        Me.btnbuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnbuscar.Location = New System.Drawing.Point(501, 18)
        Me.btnbuscar.Name = "btnbuscar"
        Me.btnbuscar.Size = New System.Drawing.Size(99, 34)
        Me.btnbuscar.TabIndex = 58
        Me.btnbuscar.Text = "Buscar"
        Me.btnbuscar.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImprimir.Location = New System.Drawing.Point(606, 17)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(100, 34)
        Me.btnImprimir.TabIndex = 8
        Me.btnImprimir.Text = "Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(381, 4)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(80, 16)
        Me.Label1.TabIndex = 56
        Me.Label1.Text = "Fecha Fin:"
        '
        'txtcount
        '
        Me.txtcount.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.txtcount.Location = New System.Drawing.Point(1305, 110)
        Me.txtcount.Name = "txtcount"
        Me.txtcount.Size = New System.Drawing.Size(126, 20)
        Me.txtcount.TabIndex = 26
        '
        'datadetalleventa
        '
        Me.datadetalleventa.AllowUserToAddRows = False
        Me.datadetalleventa.AllowUserToDeleteRows = False
        Me.datadetalleventa.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.datadetalleventa.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.datadetalleventa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.datadetalleventa.Location = New System.Drawing.Point(1243, 12)
        Me.datadetalleventa.Name = "datadetalleventa"
        Me.datadetalleventa.ReadOnly = True
        Me.datadetalleventa.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.datadetalleventa.Size = New System.Drawing.Size(205, 70)
        Me.datadetalleventa.TabIndex = 74
        '
        'FrmCierreCaja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1517, 707)
        Me.Controls.Add(Me.datadetalleventa)
        Me.Controls.Add(Me.txtcount)
        Me.Controls.Add(Me.dataapertura)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "FrmCierreCaja"
        Me.Text = "FrmCierreCaja"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dataapertura, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.dataegresos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.datadetalleventa, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dataapertura As DataGridView
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents txtcierrecaja As TextBox
    Friend WithEvents txtsalidacaja As TextBox
    Friend WithEvents txtconsumo As TextBox
    Friend WithEvents txtapertura As TextBox
    Friend WithEvents dataegresos As DataGridView
    Friend WithEvents lblInexistente As Label
    Friend WithEvents datalistado As DataGridView
    Friend WithEvents txtfecha_inicio As DateTimePicker
    Friend WithEvents txtfecha_fin As DateTimePicker
    Friend WithEvents Label6 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents cbousuario As ComboBox
    Friend WithEvents Label12 As Label
    Friend WithEvents btnSalir As Button
    Friend WithEvents btnbuscar As Button
    Friend WithEvents btnImprimir As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtcount As TextBox
    Friend WithEvents datadetalleventa As DataGridView
End Class
