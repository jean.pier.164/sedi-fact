﻿Public Class FrmInformeCierreCaja
    Private Sub FrmInformeCierreCaja_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: esta línea de código carga datos en la tabla 'DSCIERRECAJA.mostrar_apertura_usuario' Puede moverla o quitarla según sea necesario.
        Me.mostrar_apertura_usuarioTableAdapter.Fill(Me.DSCIERRECAJA.mostrar_apertura_usuario, login:=txtlogin.Text, fecha_ini:=txtfecha_ini.Text, fecha_fin:=txtfecha_fi.Text)
        'TODO: esta línea de código carga datos en la tabla 'DSCIERRECAJA.mostrar_egresos_usuario' Puede moverla o quitarla según sea necesario.
        Me.mostrar_egresos_usuarioTableAdapter.Fill(Me.DSCIERRECAJA.mostrar_egresos_usuario, login:=txtlogin.Text, fecha_ini:=txtfecha_ini.Text, fecha_fin:=txtfecha_fi.Text)
        'TODO: esta línea de código carga datos en la tabla 'DSCIERRECAJA.mostrar_ingreso_usuario' Puede moverla o quitarla según sea necesario.
        Me.mostrar_ingreso_usuarioTableAdapter.Fill(Me.DSCIERRECAJA.mostrar_ingreso_usuario, login:=txtlogin.Text, fecha_ini:=txtfecha_ini.Text, fecha_fin:=txtfecha_fi.Text)

        Me.ReportViewer1.RefreshReport()
    End Sub
End Class