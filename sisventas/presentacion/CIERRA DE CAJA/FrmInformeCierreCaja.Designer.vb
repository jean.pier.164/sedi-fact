﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmInformeCierreCaja
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource2 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Dim ReportDataSource3 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.mostrar_apertura_usuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DSCIERRECAJA = New BRAVOSPORT.DSCIERRECAJA()
        Me.mostrar_egresos_usuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.mostrar_ingreso_usuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.txtlogin = New System.Windows.Forms.TextBox()
        Me.txtfecha_fi = New System.Windows.Forms.TextBox()
        Me.txtfecha_ini = New System.Windows.Forms.TextBox()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.mostrar_apertura_usuarioTableAdapter = New BRAVOSPORT.DSCIERRECAJATableAdapters.mostrar_apertura_usuarioTableAdapter()
        Me.mostrar_egresos_usuarioTableAdapter = New BRAVOSPORT.DSCIERRECAJATableAdapters.mostrar_egresos_usuarioTableAdapter()
        Me.mostrar_ingreso_usuarioTableAdapter = New BRAVOSPORT.DSCIERRECAJATableAdapters.mostrar_ingreso_usuarioTableAdapter()
        Me.txtcount = New System.Windows.Forms.TextBox()
        CType(Me.mostrar_apertura_usuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSCIERRECAJA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mostrar_egresos_usuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mostrar_ingreso_usuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mostrar_apertura_usuarioBindingSource
        '
        Me.mostrar_apertura_usuarioBindingSource.DataMember = "mostrar_apertura_usuario"
        Me.mostrar_apertura_usuarioBindingSource.DataSource = Me.DSCIERRECAJA
        '
        'DSCIERRECAJA
        '
        Me.DSCIERRECAJA.DataSetName = "DSCIERRECAJA"
        Me.DSCIERRECAJA.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'mostrar_egresos_usuarioBindingSource
        '
        Me.mostrar_egresos_usuarioBindingSource.DataMember = "mostrar_egresos_usuario"
        Me.mostrar_egresos_usuarioBindingSource.DataSource = Me.DSCIERRECAJA
        '
        'mostrar_ingreso_usuarioBindingSource
        '
        Me.mostrar_ingreso_usuarioBindingSource.DataMember = "mostrar_ingreso_usuario"
        Me.mostrar_ingreso_usuarioBindingSource.DataSource = Me.DSCIERRECAJA
        '
        'txtlogin
        '
        Me.txtlogin.Location = New System.Drawing.Point(24, 89)
        Me.txtlogin.Name = "txtlogin"
        Me.txtlogin.Size = New System.Drawing.Size(74, 20)
        Me.txtlogin.TabIndex = 14
        Me.txtlogin.Visible = False
        '
        'txtfecha_fi
        '
        Me.txtfecha_fi.Location = New System.Drawing.Point(24, 63)
        Me.txtfecha_fi.Name = "txtfecha_fi"
        Me.txtfecha_fi.Size = New System.Drawing.Size(74, 20)
        Me.txtfecha_fi.TabIndex = 13
        Me.txtfecha_fi.Visible = False
        '
        'txtfecha_ini
        '
        Me.txtfecha_ini.Location = New System.Drawing.Point(24, 37)
        Me.txtfecha_ini.Name = "txtfecha_ini"
        Me.txtfecha_ini.Size = New System.Drawing.Size(74, 20)
        Me.txtfecha_ini.TabIndex = 12
        Me.txtfecha_ini.Visible = False
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "dsCierreCaja"
        ReportDataSource1.Value = Me.mostrar_apertura_usuarioBindingSource
        ReportDataSource2.Name = "DsEgresos"
        ReportDataSource2.Value = Me.mostrar_egresos_usuarioBindingSource
        ReportDataSource3.Name = "DsIngresos"
        ReportDataSource3.Value = Me.mostrar_ingreso_usuarioBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource2)
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource3)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "BRAVOSPORT.CierreCaja.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(597, 583)
        Me.ReportViewer1.TabIndex = 11
        '
        'mostrar_apertura_usuarioTableAdapter
        '
        Me.mostrar_apertura_usuarioTableAdapter.ClearBeforeFill = True
        '
        'mostrar_egresos_usuarioTableAdapter
        '
        Me.mostrar_egresos_usuarioTableAdapter.ClearBeforeFill = True
        '
        'mostrar_ingreso_usuarioTableAdapter
        '
        Me.mostrar_ingreso_usuarioTableAdapter.ClearBeforeFill = True
        '
        'txtcount
        '
        Me.txtcount.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcount.Location = New System.Drawing.Point(468, 26)
        Me.txtcount.Name = "txtcount"
        Me.txtcount.Size = New System.Drawing.Size(117, 22)
        Me.txtcount.TabIndex = 15
        '
        'FrmInformeCierreCaja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(597, 583)
        Me.Controls.Add(Me.txtcount)
        Me.Controls.Add(Me.txtlogin)
        Me.Controls.Add(Me.txtfecha_fi)
        Me.Controls.Add(Me.txtfecha_ini)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "FrmInformeCierreCaja"
        Me.Text = "FrmInformeCierreCaja"
        CType(Me.mostrar_apertura_usuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSCIERRECAJA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mostrar_egresos_usuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mostrar_ingreso_usuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtlogin As TextBox
    Friend WithEvents txtfecha_fi As TextBox
    Friend WithEvents txtfecha_ini As TextBox
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents mostrar_apertura_usuarioBindingSource As BindingSource
    Friend WithEvents DSCIERRECAJA As DSCIERRECAJA
    Friend WithEvents mostrar_egresos_usuarioBindingSource As BindingSource
    Friend WithEvents mostrar_ingreso_usuarioBindingSource As BindingSource
    Friend WithEvents mostrar_apertura_usuarioTableAdapter As DSCIERRECAJATableAdapters.mostrar_apertura_usuarioTableAdapter
    Friend WithEvents mostrar_egresos_usuarioTableAdapter As DSCIERRECAJATableAdapters.mostrar_egresos_usuarioTableAdapter
    Friend WithEvents mostrar_ingreso_usuarioTableAdapter As DSCIERRECAJATableAdapters.mostrar_ingreso_usuarioTableAdapter
    Friend WithEvents txtcount As TextBox
End Class
