﻿Imports System.Data.SqlClient
Public Class FcierreCaja

    'Incluímos la conexion a la base de datos
    Inherits conexion
    'Declaramos  una variable para poder enviar comandos a sql server
    Dim cmd As New SqlCommand
    Dim cmd1 As New SqlCommand
    Dim cmd2 As New SqlCommand

    Public Function mostrarapertura_usuario(ByVal login As String, ByVal fecha_inicio As Date, ByVal fecha_fin As Date) As DataTable
        Try
            'Nos conectamos a la base de datos 
            conectado()

            'Llamamos al objeto llamado mostrar _consumo de la base de datos
            cmd = New SqlCommand("mostrar_apertura_usuario")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            'le enviamos la reserva de la cual queremos mostrar sus consumos
            cmd.Parameters.AddWithValue("@login", login)
            cmd.Parameters.AddWithValue("@fecha_ini", fecha_inicio)
            cmd.Parameters.AddWithValue("@fecha_fin", fecha_fin)

            'si ejecutamos la consulta
            If cmd.ExecuteNonQuery Then

                'declaramos una variable que almacena todos los datos en una tabla en memoria ram
                Dim dt As New DataTable

                '(sqladapter que representa un conjunto de comando de datos)
                Dim da As New SqlDataAdapter(cmd)

                'pasamos los datos de la variable dt a la variable da
                da.Fill(dt)
                Return dt
            Else

                'En caso no se ejecute la consulta devolvemos nada


                Return Nothing
            End If


        Catch ex As Exception

            'cerramos el capturador y mostramos el posible error
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()


        End Try

    End Function

    Public Function mostraringreso_usuario(ByVal login As String, ByVal fecha_inicio As Date, ByVal fecha_fin As Date) As DataTable
        Try
            'Nos conectamos a la base de datos 
            conectado()

            'Llamamos al objeto llamado mostrar _consumo de la base de datos
            cmd = New SqlCommand("mostrar_ingreso_usuario")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            'le enviamos la reserva de la cual queremos mostrar sus consumos
            cmd.Parameters.AddWithValue("@login", login)
            cmd.Parameters.AddWithValue("@fecha_ini", fecha_inicio)
            cmd.Parameters.AddWithValue("@fecha_fin", fecha_fin)

            'si ejecutamos la consulta
            If cmd.ExecuteNonQuery Then

                'declaramos una variable que almacena todos los datos en una tabla en memoria ram
                Dim dt As New DataTable

                '(sqladapter que representa un conjunto de comando de datos)
                Dim da As New SqlDataAdapter(cmd)

                'pasamos los datos de la variable dt a la variable da
                da.Fill(dt)
                Return dt
            Else

                'En caso no se ejecute la consulta devolvemos nada


                Return Nothing
            End If


        Catch ex As Exception

            'cerramos el capturador y mostramos el posible error
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()


        End Try

    End Function



    Public Function mostraregresos_usuario(ByVal login As String, ByVal fecha_inicio As Date, ByVal fecha_fin As Date) As DataTable
        Try
            'Nos conectamos a la base de datos 
            conectado()

            'Llamamos al objeto llamado mostrar _consumo de la base de datos
            cmd = New SqlCommand("mostrar_egresos_usuario")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            'le enviamos la reserva de la cual queremos mostrar sus consumos
            cmd.Parameters.AddWithValue("@login", login)
            cmd.Parameters.AddWithValue("@fecha_ini", fecha_inicio)
            cmd.Parameters.AddWithValue("@fecha_fin", fecha_fin)

            'si ejecutamos la consulta
            If cmd.ExecuteNonQuery Then

                'declaramos una variable que almacena todos los datos en una tabla en memoria ram
                Dim dt As New DataTable

                '(sqladapter que representa un conjunto de comando de datos)
                Dim da As New SqlDataAdapter(cmd)

                'pasamos los datos de la variable dt a la variable da
                da.Fill(dt)
                Return dt
            Else

                'En caso no se ejecute la consulta devolvemos nada


                Return Nothing
            End If


        Catch ex As Exception

            'cerramos el capturador y mostramos el posible error
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()


        End Try

    End Function

End Class
