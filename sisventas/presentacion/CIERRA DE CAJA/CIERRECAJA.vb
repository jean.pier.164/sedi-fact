﻿Public Class CIERRECAJA
    Private Sub CIERRECAJA_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Try

            'TODO: esta línea de código carga datos en la tabla 'CIERRECAJADS.cantproductosvendidosporusuariom' Puede moverla o quitarla según sea necesario.
            Me.cantproductosvendidosporusuariomTableAdapter.Fill(Me.CIERRECAJADS.cantproductosvendidosporusuariom, log:=txtlogin.Text, fechafi:=txtfecha_fi.Text, fechai:=txtfecha_ini.Text)
            'TODO: esta línea de código carga datos en la tabla 'CIERRECAJADS.mostrar_apertura_usuario' Puede moverla o quitarla según sea necesario.
            Me.mostrar_apertura_usuarioTableAdapter.Fill(Me.CIERRECAJADS.mostrar_apertura_usuario, login:=txtlogin.Text, fecha_ini:=txtfecha_fi.Text, fecha_fin:=txtfecha_ini.Text)
            'TODO: esta línea de código carga datos en la tabla 'CIERRECAJADS.mostrar_egresos_usuario' Puede moverla o quitarla según sea necesario.
            Me.mostrar_egresos_usuarioTableAdapter.Fill(Me.CIERRECAJADS.mostrar_egresos_usuario, login:=txtlogin.Text, fecha_ini:=txtfecha_fi.Text, fecha_fin:=txtfecha_ini.Text)
            'TODO: esta línea de código carga datos en la tabla 'CIERRECAJADS.mostrar_ingreso_usuario' Puede moverla o quitarla según sea necesario.
            Me.mostrar_ingreso_usuarioTableAdapter.Fill(Me.CIERRECAJADS.mostrar_ingreso_usuario, login:=txtlogin.Text, fecha_ini:=txtfecha_fi.Text, fecha_fin:=txtfecha_ini.Text)
            Me.ReportViewer1.RefreshReport()
        Catch ex As Exception
            Me.ReportViewer1.RefreshReport()
        End Try

    End Sub

End Class