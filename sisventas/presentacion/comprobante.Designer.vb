﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class comprobante
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.generar_comprobantemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.marketdscomprobante = New BRAVOSPORT.marketdscomprobante()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.txtidventa = New System.Windows.Forms.TextBox()
        Me.generar_comprobantemTableAdapter = New BRAVOSPORT.marketdscomprobanteTableAdapters.generar_comprobantemTableAdapter()
        CType(Me.generar_comprobantemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.marketdscomprobante, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'generar_comprobantemBindingSource
        '
        Me.generar_comprobantemBindingSource.DataMember = "generar_comprobantem"
        Me.generar_comprobantemBindingSource.DataSource = Me.marketdscomprobante
        '
        'marketdscomprobante
        '
        Me.marketdscomprobante.DataSetName = "marketdscomprobante"
        Me.marketdscomprobante.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "dsbante"
        ReportDataSource1.Value = Me.generar_comprobantemBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "BRAVOSPORT.comprobante.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(268, 197)
        Me.ReportViewer1.TabIndex = 0
        '
        'txtidventa
        '
        Me.txtidventa.Location = New System.Drawing.Point(153, 55)
        Me.txtidventa.Name = "txtidventa"
        Me.txtidventa.Size = New System.Drawing.Size(100, 20)
        Me.txtidventa.TabIndex = 1
        Me.txtidventa.Visible = False
        '
        'generar_comprobantemTableAdapter
        '
        Me.generar_comprobantemTableAdapter.ClearBeforeFill = True
        '
        'comprobante
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(268, 197)
        Me.Controls.Add(Me.txtidventa)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "comprobante"
        Me.Text = "comprobante"
        CType(Me.generar_comprobantemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.marketdscomprobante, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents generar_comprobantemBindingSource As BindingSource
    Friend WithEvents marketdscomprobante As marketdscomprobante
    Friend WithEvents txtidventa As TextBox
    Friend WithEvents generar_comprobantemTableAdapter As marketdscomprobanteTableAdapters.generar_comprobantemTableAdapter
End Class
