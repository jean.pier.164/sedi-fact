﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmReciboA5
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.mostrar_comprobantea5BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.marketdsA5 = New BRAVOSPORT.marketdsA5()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.mostrar_comprobantea5TableAdapter = New BRAVOSPORT.marketdsA5TableAdapters.mostrar_comprobantea5TableAdapter()
        Me.txtidventa = New System.Windows.Forms.TextBox()
        Me._SORDONEZ_FV02DataSet1 = New BRAVOSPORT._SORDONEZ_FV02DataSet()
        CType(Me.mostrar_comprobantea5BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.marketdsA5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._SORDONEZ_FV02DataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mostrar_comprobantea5BindingSource
        '
        Me.mostrar_comprobantea5BindingSource.DataMember = "mostrar_comprobantea5"
        Me.mostrar_comprobantea5BindingSource.DataSource = Me.marketdsA5
        '
        'marketdsA5
        '
        Me.marketdsA5.DataSetName = "marketdsA5"
        Me.marketdsA5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ReportViewer1.DocumentMapWidth = 8
        Me.ReportViewer1.Font = New System.Drawing.Font("Arial Black", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        ReportDataSource1.Name = "dsa5"
        ReportDataSource1.Value = Me.mostrar_comprobantea5BindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "BRAVOSPORT.ComprobanteA5.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(505, 520)
        Me.ReportViewer1.TabIndex = 5
        '
        'mostrar_comprobantea5TableAdapter
        '
        Me.mostrar_comprobantea5TableAdapter.ClearBeforeFill = True
        '
        'txtidventa
        '
        Me.txtidventa.Location = New System.Drawing.Point(235, 250)
        Me.txtidventa.Name = "txtidventa"
        Me.txtidventa.Size = New System.Drawing.Size(100, 20)
        Me.txtidventa.TabIndex = 6
        Me.txtidventa.Visible = False
        '
        '_SORDONEZ_FV02DataSet1
        '
        Me._SORDONEZ_FV02DataSet1.DataSetName = "_SORDONEZ_FV02DataSet"
        Me._SORDONEZ_FV02DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FrmReciboA5
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(505, 520)
        Me.Controls.Add(Me.txtidventa)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "FrmReciboA5"
        Me.Text = "FrmReciboA5"
        CType(Me.mostrar_comprobantea5BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.marketdsA5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._SORDONEZ_FV02DataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents mostrar_comprobantea5BindingSource As BindingSource
    Friend WithEvents marketdsA5 As marketdsA5
    Friend WithEvents mostrar_comprobantea5TableAdapter As marketdsA5TableAdapters.mostrar_comprobantea5TableAdapter
    Friend WithEvents txtidventa As TextBox
    Friend WithEvents _SORDONEZ_FV02DataSet1 As _SORDONEZ_FV02DataSet
End Class
