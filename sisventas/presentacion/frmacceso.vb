﻿Public Class frmacceso
    Private dt As New DataTable
    Private Sub btnguardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnguardar.Click
        If Me.ValidateChildren = True And txtnombre.Text <> "" Then
            Try
                Dim dts As New vusuario
                Dim func As New fusuario

                dts.gnombre = txtnombre.Text
                dts.gapellidos = txtapellidos.Text
                dts.gdni = txtdni.Text
                dts.gdireccion = txtdireccion.Text
                dts.gtelefono = txttelefono.Text
                dts.glogin = txtlogin.Text
                dts.gpassword = txtpassword.Text
                dts.gacceso = txtacceso.Text



                If func.insertar(dts) Then
                    MessageBox.Show("Usuario Registrado Correctamente", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar()
                    limpiar()
                Else
                    MessageBox.Show("Usuario no fue registrado intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar()
                    limpiar()

                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub frmacceso_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mostrar()
    End Sub
    Public Sub limpiar()
        btnguardar.Visible = True
        btneditar.Visible = False
        txtidusuario.Text = ""
        txtnombre.Text = ""

        txtapellidos.Text = ""
        txtdni.Text = ""
        txtdireccion.Text = ""
        txttelefono.Text = ""
        txtlogin.Text = ""
        cboacceso.Text = ""


    End Sub
    Private Sub mostrar()
        Try
            Dim func As New fusuario
            dt = func.mostrar

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt
                txtbuscar.Enabled = True

                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing
                txtbuscar.Enabled = False

                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
        btnnuevo.Visible = True
        btneditar.Visible = False

        buscar()

    End Sub

    Private Sub buscar()
        Try

            Dim ds As New DataSet

            ds.Tables.Add(dt.Copy)

            Dim dv As New DataView(ds.Tables(0))


            dv.RowFilter = cbocampo.Text & " like'" & txtbuscar.Text & "%'"

            If dv.Count <> 0 Then
                inexistente.Visible = False
                datalistado.DataSource = dv
                ocultar_columnas()

            Else
                inexistente.Visible = True
                datalistado.DataSource = Nothing


            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub
    Private Sub ocultar_columnas()
        datalistado.Columns(1).Visible = False

    End Sub

    Private Sub btneliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneliminar.Click
        Dim result As DialogResult
        result = MessageBox.Show("Realmente desea eliminar los usuarios seleccionados?", "Eliminando registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)


        If result = DialogResult.OK Then
            Try
                For Each row As DataGridViewRow In datalistado.Rows
                    Dim marcado As Boolean = Convert.ToBoolean(row.Cells("Eliminar").Value)

                    If marcado Then
                        Dim onekey As Integer = Convert.ToInt32(row.Cells("idusuario").Value)
                        Dim vdb As New vusuario
                        Dim func As New fusuario

                        vdb.gidusuario = onekey
                        If func.eliminar(vdb) Then

                        Else
                            MessageBox.Show("Usuario no fue eliminado?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If

                    End If
                Next

                Call mostrar()
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("Cancelando eliminacion de Registros?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Call mostrar()
        End If
        Call limpiar()
    End Sub

    Private Sub datalistado_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellClick
        txtidusuario.Text = datalistado.SelectedCells.Item(1).Value
        txtnombre.Text = datalistado.SelectedCells.Item(2).Value
        txtapellidos.Text = datalistado.SelectedCells.Item(3).Value
        txtdni.Text = datalistado.SelectedCells.Item(4).Value
        txtdireccion.Text = datalistado.SelectedCells.Item(5).Value
        txttelefono.Text = datalistado.SelectedCells.Item(6).Value
        txtlogin.Text = datalistado.SelectedCells.Item(7).Value
        txtpassword.Text = datalistado.SelectedCells.Item(8).Value
        cboacceso.Text = datalistado.SelectedCells.Item(9).Value

        btneditar.Visible = True
        btnguardar.Visible = False
    End Sub

    Private Sub datalistado_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles datalistado.CellContentClick

    End Sub

    Private Sub btneditar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btneditar.Click
        Dim result As DialogResult

        result = MessageBox.Show("Realmente desea editar los datos del Usuario ?", "Modificando Registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)

        If result = DialogResult.OK Then

        End If

        If Me.ValidateChildren = True And txtnombre.Text <> "" And txtidusuario.Text <> "" Then
            Try
                Dim dts As New vusuario
                Dim func As New fusuario

                dts.gidusuario = txtidusuario.Text
                dts.gnombre = txtnombre.Text
                dts.gapellidos = txtapellidos.Text
                dts.gdni = txtdni.Text
                dts.gdireccion = txtdireccion.Text
                dts.gtelefono = txttelefono.Text
                dts.glogin = txtlogin.Text
                dts.gpassword = txtpassword.Text
                dts.gacceso = txtacceso.Text


                If func.editar(dts) Then
                    MessageBox.Show("Usuario Modificado Correctamente", "Modificando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar()
                    limpiar()
                Else
                    MessageBox.Show("Categoria no fue Modificada intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar()
                    limpiar()

                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub cboacceso_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboacceso.SelectedIndexChanged
        If cboacceso.Text = "Administrador" Then
            txtacceso.Text = "1"
        End If
        If cboacceso.Text = "Cajero" Then
            txtacceso.Text = "2"
        End If
    End Sub
End Class