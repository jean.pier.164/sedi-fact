﻿Public Class DApertura
    Private idapertura As Integer
    Private monto As Double
    Private fecha As DateTime
    Private idusuario As Integer

    Public Property gidapertura

        Get
            Return idapertura

        End Get
        Set(value)
            idapertura = value
        End Set
    End Property

    Public Property gmonto

        Get
            Return monto
        End Get
        Set(value)
            monto = value
        End Set
    End Property

    Public Property gfecha
        Get
            Return fecha
        End Get
        Set(value)
            fecha = value

        End Set
    End Property

    Public Property gidusuario
        Get
            Return idusuario
        End Get
        Set(value)
            idusuario = value

        End Set
    End Property
    Public Sub New()

    End Sub

    Public Sub New(ByVal idapertura As Integer, ByVal monto As Double, ByVal fecha As DateTime, ByVal idusuario As Integer)

        'Recibir los valores y enviarle a la propiedad respectiva 

        Me.gidapertura = idapertura
        Me.gmonto = monto
        Me.gfecha = fecha
        Me.gidusuario = idusuario



    End Sub
End Class
