﻿Public Class FrmApertura
    Private dt As New DataTable
    Private Sub btnguardar_Click(sender As Object, e As EventArgs) Handles btnguardar.Click
        Try
            Dim dts As New DApertura 'instanciamos a la clase vcategoria que tiene los objetos, los meétodos setter y getter y los constructores
            Dim func As New FApertura 'instanciamos a la clase funciones de la tabla categoría

            'enviamos los datos que ingresamos en las cajas de texto a sus respectivos objetos


            dts.gmonto = txtmonto.Text
            dts.gfecha = dtpfechaapertura.Value
            dts.gidusuario = txtidtrabajador.Text

            'si los datos son ingresados y se recibe un true de la función insertar de la clase fcategoria
            If func.insertar(dts) Then
                MessageBox.Show("Guardado Correctamente", "Guardando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                mostrar()

                'en caso los datos no sean ingresados y recibimos un false
            Else
                MessageBox.Show("Error al intentar Guardar", "Guardando registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                mostrar()

            End If
        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub btnactualizar_Click(sender As Object, e As EventArgs) Handles btnactualizar.Click
        'declaramos una variable de tipo dialogresult que me capture lo que yo elija en el msgbox
        Dim result As DialogResult
        'preguntamos si está seguro de editar o no los datos
        result = MessageBox.Show("Realmente desea editar los datos", "Modificando registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
        'si esta seguro de modificar
        'verficamos que todos los campos no esten vacios


        Try
            Dim dts As New DApertura 'instanciamos a la clase vcategoria que tiene los objetos, los meétodos setter y getter y los constructores
            Dim func As New FApertura 'instanciamos a la clase funciones de la tabla categoría

            'enviamos los datos 

            dts.gidapertura = txtidapertura.Text
            dts.gmonto = txtmonto.Text
            dts.gfecha = dtpfechaapertura.Value
            dts.gidusuario = txtidtrabajador.Text


            'llamamos a la funcion editar de la clase fusuario y si logramos modificamos los datos mostramos un mensaje
            If func.editar(dts) Then
                MessageBox.Show("Modificado Correctamente", "Modificando", MessageBoxButtons.OK, MessageBoxIcon.Information)
                mostrar()



            Else
                'en caso no podamos editar los registros mostramos el mensaje de error
                MessageBox.Show("Error al intentar Modificar", "Modificanco", MessageBoxButtons.OK, MessageBoxIcon.Error)
                mostrar()
            End If


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub FrmApertura_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        mostrarusuario()
        mostrar()
    End Sub

    Private Sub mostrarusuario()

        Dim func As New FApertura
        dt = func.mostrarusu

    End Sub
    Private Sub mostrar()
        Try
            Dim func As New FApertura
            dt = func.mostrar

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt

                datalistado.ColumnHeadersVisible = True
                datalistado.Columns(0).Visible = False
                datalistado.Columns(3).Visible = False

            Else

                datalistado.DataSource = Nothing
                datalistado.ColumnHeadersVisible = False

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        'ocultamos y mostramos los botones correspondientes


    End Sub

    Private Sub datalistado_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellClick
        txtidapertura.Text = datalistado.SelectedCells.Item(0).Value
        txtmonto.Text = datalistado.SelectedCells.Item(1).Value
        dtpfechaapertura.Text = datalistado.SelectedCells.Item(2).Value
        txtidtrabajador.Text = datalistado.SelectedCells.Item(3).Value
    End Sub

    Private Sub datalistado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellContentClick

    End Sub
End Class