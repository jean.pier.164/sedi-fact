﻿Imports System.Data.SqlClient
Public Class FApertura

    Inherits conexion

    Dim cmd As New SqlCommand
    Public Function mostrar() As DataTable
        Try
            'Nos conectamos a la base de datos 
            conectado()

            'Llamamos al objeto llamado mostrar_habitación de la base de dato
            cmd = New SqlCommand("mostrar_apertura_total")

            cmd.CommandType = CommandType.StoredProcedure

            cmd.Connection = cnn

            If cmd.ExecuteNonQuery Then

                Dim dt As New DataTable

                Dim da As New SqlDataAdapter(cmd)

                da.Fill(dt)
                Return dt
            Else


                Return Nothing
            End If


        Catch ex As Exception

            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()


        End Try

    End Function
    Public Function insertar(ByVal dts As DApertura) As Boolean
        Try

            conectado()

            cmd = New SqlCommand("insertar_apertura")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@monto", dts.gmonto)
            cmd.Parameters.AddWithValue("@fecha", dts.gfecha)
            cmd.Parameters.AddWithValue("@idusuario", dts.gidusuario)

            If cmd.ExecuteNonQuery Then

                Return True
            Else

                Return False


            End If

        Catch ex As Exception
            MsgBox(ex.Message)

            Return False
        Finally
            desconectado()


        End Try


    End Function




    Public Function editar(ByVal dts As DApertura) As Boolean
        Try

            conectado()

            cmd = New SqlCommand("editar_apertura")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@idapertura", dts.gidapertura)
            cmd.Parameters.AddWithValue("@monto", dts.gmonto)
            cmd.Parameters.AddWithValue("@fecha", dts.gfecha)
            cmd.Parameters.AddWithValue("@idusuario", dts.gidusuario)



            If cmd.ExecuteNonQuery Then

                Return True
            Else

                Return False


            End If

        Catch ex As Exception
            MsgBox(ex.Message)

            Return False
        Finally
            desconectado()


        End Try


    End Function



    Public Function mostrarusu() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("userlog")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@login", frmlogin.txtlogin.Text)

            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                ' no conozco el nombre d elos campos ni de los controles

                ' frmdetalle_venta.txtusuario.Text = dt.Rows(0).Item("login").ToString

                FrmApertura.txtidtrabajador.Text = dt.Rows(0).Item("idusuario").ToString



            End If
            Return dt



        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function


End Class
