﻿Public Class DEgreso
    Private idegresos As Integer
    Private monto As Double
    Private motivo As String
    Private fecha As DateTime
    Private idusuario As Integer


    Public Property gidegresos

        Get
            Return idegresos

        End Get
        Set(value)
            idegresos = value
        End Set
    End Property

    Public Property gmonto

        Get
            Return monto
        End Get
        Set(value)
            monto = value
        End Set
    End Property
    Public Property gmotivo
        Get
            Return motivo
        End Get
        Set(value)
            motivo = value

        End Set
    End Property
    Public Property gfecha
        Get
            Return fecha
        End Get
        Set(value)
            fecha = value

        End Set
    End Property
    Public Property gidusuario
        Get
            Return idusuario
        End Get
        Set(value)
            idusuario = value

        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal idegresos As Integer, ByVal monto As Double, ByVal motivo As Double, ByVal fecha As DateTime, ByVal idusuario As Integer)

        'Recibir los valores y enviarle a la propiedad respectiva 

        Me.gidegresos = idegresos
        Me.gmonto = monto
        Me.gmotivo = motivo
        Me.gfecha = fecha
        Me.gidusuario = idusuario


    End Sub
End Class
