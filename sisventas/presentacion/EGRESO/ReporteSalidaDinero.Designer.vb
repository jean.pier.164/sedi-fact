﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReporteSalidaDinero
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.txtlogin = New System.Windows.Forms.TextBox()
        Me.txtfechafi = New System.Windows.Forms.TextBox()
        Me.txtfechai = New System.Windows.Forms.TextBox()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.ConsEgresosPorUsuario = New BRAVOSPORT.ConsEgresosPorUsuario()
        Me.mostrar_egresos_usuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.mostrar_egresos_usuarioTableAdapter = New BRAVOSPORT.ConsEgresosPorUsuarioTableAdapters.mostrar_egresos_usuarioTableAdapter()
        CType(Me.ConsEgresosPorUsuario, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mostrar_egresos_usuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtlogin
        '
        Me.txtlogin.Location = New System.Drawing.Point(24, 89)
        Me.txtlogin.Name = "txtlogin"
        Me.txtlogin.Size = New System.Drawing.Size(74, 20)
        Me.txtlogin.TabIndex = 18
        Me.txtlogin.Visible = False
        '
        'txtfechafi
        '
        Me.txtfechafi.Location = New System.Drawing.Point(24, 63)
        Me.txtfechafi.Name = "txtfechafi"
        Me.txtfechafi.Size = New System.Drawing.Size(74, 20)
        Me.txtfechafi.TabIndex = 17
        Me.txtfechafi.Visible = False
        '
        'txtfechai
        '
        Me.txtfechai.Location = New System.Drawing.Point(24, 37)
        Me.txtfechai.Name = "txtfechai"
        Me.txtfechai.Size = New System.Drawing.Size(74, 20)
        Me.txtfechai.TabIndex = 16
        Me.txtfechai.Visible = False
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "ReportEgreso"
        ReportDataSource1.Value = Me.mostrar_egresos_usuarioBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "BRAVOSPORT.ReportEgreso.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(345, 450)
        Me.ReportViewer1.TabIndex = 15
        '
        'ConsEgresosPorUsuario
        '
        Me.ConsEgresosPorUsuario.DataSetName = "ConsEgresosPorUsuario"
        Me.ConsEgresosPorUsuario.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'mostrar_egresos_usuarioBindingSource
        '
        Me.mostrar_egresos_usuarioBindingSource.DataMember = "mostrar_egresos_usuario"
        Me.mostrar_egresos_usuarioBindingSource.DataSource = Me.ConsEgresosPorUsuario
        '
        'mostrar_egresos_usuarioTableAdapter
        '
        Me.mostrar_egresos_usuarioTableAdapter.ClearBeforeFill = True
        '
        'ReporteSalidaDinero
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(345, 450)
        Me.Controls.Add(Me.txtlogin)
        Me.Controls.Add(Me.txtfechafi)
        Me.Controls.Add(Me.txtfechai)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "ReporteSalidaDinero"
        Me.Text = "ReporteSalidaDinero"
        CType(Me.ConsEgresosPorUsuario, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mostrar_egresos_usuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtlogin As TextBox
    Friend WithEvents txtfechafi As TextBox
    Friend WithEvents txtfechai As TextBox
    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents mostrar_egresos_usuarioBindingSource As BindingSource
    Friend WithEvents ConsEgresosPorUsuario As ConsEgresosPorUsuario
    Friend WithEvents mostrar_egresos_usuarioTableAdapter As ConsEgresosPorUsuarioTableAdapters.mostrar_egresos_usuarioTableAdapter
End Class
