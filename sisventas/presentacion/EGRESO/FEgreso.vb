﻿Imports System.Data.SqlClient
Public Class FEgreso
    Inherits conexion
    Dim cmd As New SqlCommand
    Public Function mostrar() As DataTable
        Try
            'Nos conectamos a la base de datos 
            conectado()

            'Llamamos al objeto llamado mostrar_habitación de la base de dato
            cmd = New SqlCommand("mostrar_egresos_total")

            cmd.CommandType = CommandType.StoredProcedure

            cmd.Connection = cnn

            If cmd.ExecuteNonQuery Then

                Dim dt As New DataTable

                Dim da As New SqlDataAdapter(cmd)

                da.Fill(dt)
                Return dt
            Else


                Return Nothing
            End If


        Catch ex As Exception

            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try

    End Function
    Public Function insertar(ByVal dts As DEgreso) As Boolean
        Try

            conectado()

            cmd = New SqlCommand("insertar_egresos")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@monto", dts.gmonto)
            cmd.Parameters.AddWithValue("@motivo", dts.gmotivo)
            cmd.Parameters.AddWithValue("@fecha", dts.gfecha)
            cmd.Parameters.AddWithValue("@idusuario", dts.gidusuario)


            If cmd.ExecuteNonQuery Then

                Return True
            Else

                Return False


            End If

        Catch ex As Exception
            MsgBox(ex.Message)

            Return False
        Finally
            desconectado()


        End Try


    End Function

    Public Function editar(ByVal dts As DEgreso) As Boolean
        Try

            conectado()

            cmd = New SqlCommand("editar_egresos")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            cmd.Parameters.AddWithValue("@idegresos", dts.gidegresos)
            cmd.Parameters.AddWithValue("@monto", dts.gmonto)
            cmd.Parameters.AddWithValue("@motivo", dts.gmotivo)
            cmd.Parameters.AddWithValue("@fecha", dts.gfecha)
            cmd.Parameters.AddWithValue("@idusuario", dts.gidusuario)



            If cmd.ExecuteNonQuery Then

                Return True
            Else

                Return False


            End If

        Catch ex As Exception
            MsgBox(ex.Message)

            Return False
        Finally
            desconectado()


        End Try


    End Function


    Public Function mostraregresos(ByVal fecha_inicio As Date, ByVal fecha_fin As Date) As DataTable
        Try
            'Nos conectamos a la base de datos 
            conectado()

            'Llamamos al objeto llamado mostrar _consumo de la base de datos
            cmd = New SqlCommand("mostrar_egresos")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            'le enviamos la reserva de la cual queremos mostrar sus consumos
            cmd.Parameters.AddWithValue("@fecha_ini", fecha_inicio)
            cmd.Parameters.AddWithValue("@fecha_fin", fecha_fin)

            'si ejecutamos la consulta
            If cmd.ExecuteNonQuery Then

                'declaramos una variable que almacena todos los datos en una tabla en memoria ram
                Dim dt As New DataTable

                '(sqladapter que representa un conjunto de comando de datos)
                Dim da As New SqlDataAdapter(cmd)

                'pasamos los datos de la variable dt a la variable da
                da.Fill(dt)
                Return dt
            Else

                'En caso no se ejecute la consulta devolvemos nada


                Return Nothing
            End If


        Catch ex As Exception

            'cerramos el capturador y mostramos el posible error
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()


        End Try
    End Function


    Public Function mostrar_egresosporfechas() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("mostrar_egresos_xfechas")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@fecha_ini", FrmEgreso.txtfechai.Value)
            cmd.Parameters.AddWithValue("@fecha_fin", FrmEgreso.txtfechafi.Value)


            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt



        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try


    End Function


    Public Function mostrar_egresosporusuario() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("mostrar_egresos_usuario")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@login", FrmEgreso.cbologin.Text)
            cmd.Parameters.AddWithValue("@fecha_ini", FrmEgreso.txtfechai.Value)
            cmd.Parameters.AddWithValue("@fecha_fin", FrmEgreso.txtfechafi.Value)


            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt



        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try


    End Function

    Public Function mostraregresos_recepcionista(ByVal recepcionista As String, ByVal fecha_inicio As Date, ByVal fecha_fin As Date) As DataTable
        Try
            'Nos conectamos a la base de datos 
            conectado()

            'Llamamos al objeto llamado mostrar _consumo de la base de datos
            cmd = New SqlCommand("mostrar_egresos_recepcionista")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            'le enviamos la reserva de la cual queremos mostrar sus consumos

            cmd.Parameters.AddWithValue("@recepcionista", recepcionista)
            cmd.Parameters.AddWithValue("@fecha_ini", fecha_inicio)
            cmd.Parameters.AddWithValue("@fecha_fin", fecha_fin)

            'si ejecutamos la consulta
            If cmd.ExecuteNonQuery Then

                'declaramos una variable que almacena todos los datos en una tabla en memoria ram
                Dim dt As New DataTable

                '(sqladapter que representa un conjunto de comando de datos)
                Dim da As New SqlDataAdapter(cmd)

                'pasamos los datos de la variable dt a la variable da
                da.Fill(dt)
                Return dt
            Else

                'En caso no se ejecute la consulta devolvemos nada


                Return Nothing
            End If


        Catch ex As Exception

            'cerramos el capturador y mostramos el posible error
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()


        End Try
    End Function



    Public Function mostrar_egresosplog() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("egresos")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn
            '    cmd.Parameters.AddWithValue("@login", frmcierredecaja.cbologin.Text)
            '    cmd.Parameters.AddWithValue("@fechae", frmcierredecaja.txtfechai.Value)



            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(dt)
            Return dt



        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

        End Try


    End Function





    Public Function mostraregresos_usuario(ByVal login As String, ByVal fecha_inicio As Date, ByVal fecha_fin As Date) As DataTable
        Try
            'Nos conectamos a la base de datos 
            conectado()

            'Llamamos al objeto llamado mostrar _consumo de la base de datos
            cmd = New SqlCommand("mostrar_egresos_usuario")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            'le enviamos la reserva de la cual queremos mostrar sus consumos
            cmd.Parameters.AddWithValue("@login", login)
            cmd.Parameters.AddWithValue("@fecha_ini", fecha_inicio)
            cmd.Parameters.AddWithValue("@fecha_fin", fecha_fin)

            'si ejecutamos la consulta
            If cmd.ExecuteNonQuery Then

                'declaramos una variable que almacena todos los datos en una tabla en memoria ram
                Dim dt As New DataTable

                '(sqladapter que representa un conjunto de comando de datos)
                Dim da As New SqlDataAdapter(cmd)

                'pasamos los datos de la variable dt a la variable da
                da.Fill(dt)
                Return dt
            Else

                'En caso no se ejecute la consulta devolvemos nada


                Return Nothing
            End If


        Catch ex As Exception

            'cerramos el capturador y mostramos el posible error
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()


        End Try

    End Function

End Class
