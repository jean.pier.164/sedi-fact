﻿Imports System.Configuration
Imports System.Data.SqlClient
Public Class FrmEgreso
    Dim conexion As New SqlConnection(ConfigurationManager.ConnectionStrings("BRAVOSPORT.My.MySettings.marketConnectionString").ConnectionString)
    Dim cadena As String
    Dim datos As New DataSet
    Dim base As New SqlDataAdapter("select * from usuario", conexion)
    Dim variable As SqlDataReader
    Dim consulta3 As New SqlCommand
    Private dt As New DataTable
    Private Sub mostrar_ingresosporusuario()
        Try
            Dim func As New FEgreso
            dt = func.mostrar_egresosporusuario

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try



    End Sub


    Private Sub mostrar_egresosporfechas()
        Try
            Dim func As New FEgreso
            dt = func.mostrar_egresosporfechas




            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try



    End Sub
    Private Sub btnconsultar_Click(sender As Object, e As EventArgs) Handles btnconsultar.Click
        If cbologin.Text = "TODOS" Then
            MessageBox.Show("TODOS")
            mostrar_egresosporfechas()

            Dim mycount As Integer

            mycount = datalistado.Rows.Count()
            txtcount.Text = mycount


        Else
            mostrar_ingresosporusuario()


            Dim mycount As Integer

            mycount = datalistado.Rows.Count()
            txtcount.Text = mycount







        End If

        Try
            Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("monto").Value IsNot Nothing) AndAlso
                    (row.Cells("monto").Value IsNot DBNull.Value)) Select row.Cells("monto").Value()

            Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDecimal(row))

            txtmonto.Text = resultado


        Catch ex As Exception
            MessageBox.Show(ex.Message)

        End Try

    End Sub

    Private Sub cbologin_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbologin.SelectedIndexChanged

    End Sub

    Private Sub FrmEgreso_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        mostrar_egresosporfechas()


        'mostrar()

        Dim mycount As Integer

        mycount = datalistado.Rows.Count()
        txtcount.Text = mycount

        cbologin.Text = "TODOS"
        consulta3.CommandType = CommandType.Text
        consulta3.CommandText = ("select login from usuario")
        consulta3.Connection = (conexion)
        conexion.Open()
        variable = consulta3.ExecuteReader

        While variable.Read = True
            cbologin.Items.Add(variable.Item(0))
        End While

        conexion.Close()
    End Sub

    Private Sub btnimprimir_Click(sender As Object, e As EventArgs) Handles btnimprimir.Click
        ReporteSalidaDinero.txtfechai.Text = txtfechai.Value
        ReporteSalidaDinero.txtfechafi.Text = txtfechafi.Value
        ReporteSalidaDinero.txtlogin.Text = cbologin.Text




        ReporteSalidaDinero.ShowDialog()
    End Sub
End Class