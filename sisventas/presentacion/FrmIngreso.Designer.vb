﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmIngreso
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtIgv = New System.Windows.Forms.TextBox()
        Me.label11 = New System.Windows.Forms.Label()
        Me.btnQuitar = New System.Windows.Forms.Button()
        Me.btnAgregar = New System.Windows.Forms.Button()
        Me.dtFecha_Vencimiento = New System.Windows.Forms.DateTimePicker()
        Me.label15 = New System.Windows.Forms.Label()
        Me.dtFecha_Produccion = New System.Windows.Forms.DateTimePicker()
        Me.label14 = New System.Windows.Forms.Label()
        Me.txtPrecio_Venta = New System.Windows.Forms.TextBox()
        Me.label13 = New System.Windows.Forms.Label()
        Me.txtPrecio_Compra = New System.Windows.Forms.TextBox()
        Me.txtIdarticulo = New System.Windows.Forms.TextBox()
        Me.txtCorrelativo = New System.Windows.Forms.TextBox()
        Me.label8 = New System.Windows.Forms.Label()
        Me.cbTipo_Comprobante = New System.Windows.Forms.ComboBox()
        Me.dtFecha = New System.Windows.Forms.DateTimePicker()
        Me.btnBuscarProveedor = New System.Windows.Forms.Button()
        Me.txtIdproveedor = New System.Windows.Forms.TextBox()
        Me.txtProveedor = New System.Windows.Forms.TextBox()
        Me.label7 = New System.Windows.Forms.Label()
        Me.txtSerie = New System.Windows.Forms.TextBox()
        Me.label6 = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.txtIdingreso = New System.Windows.Forms.TextBox()
        Me.label3 = New System.Windows.Forms.Label()
        Me.ttMensaje = New System.Windows.Forms.ToolTip(Me.components)
        Me.label1 = New System.Windows.Forms.Label()
        Me.label10 = New System.Windows.Forms.Label()
        Me.pictureBox1 = New System.Windows.Forms.PictureBox()
        Me.label12 = New System.Windows.Forms.Label()
        Me.tabControl1 = New System.Windows.Forms.TabControl()
        Me.tabPage1 = New System.Windows.Forms.TabPage()
        Me.label9 = New System.Windows.Forms.Label()
        Me.dtFecha2 = New System.Windows.Forms.DateTimePicker()
        Me.dtFecha1 = New System.Windows.Forms.DateTimePicker()
        Me.dataListado = New System.Windows.Forms.DataGridView()
        Me.Eliminar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.chkEliminar = New System.Windows.Forms.CheckBox()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.label2 = New System.Windows.Forms.Label()
        Me.tabPage2 = New System.Windows.Forms.TabPage()
        Me.groupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblTotal_Pagado = New System.Windows.Forms.Label()
        Me.label16 = New System.Windows.Forms.Label()
        Me.dataListadoDetalle = New System.Windows.Forms.DataGridView()
        Me.groupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtStock = New System.Windows.Forms.TextBox()
        Me.label5 = New System.Windows.Forms.Label()
        Me.btnBuscarArticulo = New System.Windows.Forms.Button()
        Me.label4 = New System.Windows.Forms.Label()
        Me.txtArticulo = New System.Windows.Forms.TextBox()
        Me.errorIcono = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabControl1.SuspendLayout()
        Me.tabPage1.SuspendLayout()
        CType(Me.dataListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabPage2.SuspendLayout()
        Me.groupBox1.SuspendLayout()
        CType(Me.dataListadoDetalle, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.groupBox2.SuspendLayout()
        CType(Me.errorIcono, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtIgv
        '
        Me.txtIgv.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtIgv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIgv.Location = New System.Drawing.Point(478, 70)
        Me.txtIgv.Margin = New System.Windows.Forms.Padding(2)
        Me.txtIgv.Name = "txtIgv"
        Me.txtIgv.Size = New System.Drawing.Size(82, 20)
        Me.txtIgv.TabIndex = 27
        '
        'label11
        '
        Me.label11.AutoSize = True
        Me.label11.Location = New System.Drawing.Point(414, 72)
        Me.label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label11.Name = "label11"
        Me.label11.Size = New System.Drawing.Size(28, 13)
        Me.label11.TabIndex = 26
        Me.label11.Text = "IGV:"
        '
        'btnQuitar
        '
        Me.btnQuitar.Location = New System.Drawing.Point(594, 44)
        Me.btnQuitar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnQuitar.Name = "btnQuitar"
        Me.btnQuitar.Size = New System.Drawing.Size(43, 27)
        Me.btnQuitar.TabIndex = 41
        Me.btnQuitar.UseVisualStyleBackColor = True
        '
        'btnAgregar
        '
        Me.btnAgregar.Location = New System.Drawing.Point(594, 13)
        Me.btnAgregar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnAgregar.Name = "btnAgregar"
        Me.btnAgregar.Size = New System.Drawing.Size(43, 27)
        Me.btnAgregar.TabIndex = 40
        Me.btnAgregar.UseVisualStyleBackColor = True
        '
        'dtFecha_Vencimiento
        '
        Me.dtFecha_Vencimiento.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFecha_Vencimiento.Location = New System.Drawing.Point(471, 40)
        Me.dtFecha_Vencimiento.Margin = New System.Windows.Forms.Padding(2)
        Me.dtFecha_Vencimiento.Name = "dtFecha_Vencimiento"
        Me.dtFecha_Vencimiento.Size = New System.Drawing.Size(112, 20)
        Me.dtFecha_Vencimiento.TabIndex = 39
        '
        'label15
        '
        Me.label15.AutoSize = True
        Me.label15.Location = New System.Drawing.Point(403, 42)
        Me.label15.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label15.Name = "label15"
        Me.label15.Size = New System.Drawing.Size(71, 13)
        Me.label15.TabIndex = 38
        Me.label15.Text = "Fecha Venc.:"
        '
        'dtFecha_Produccion
        '
        Me.dtFecha_Produccion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFecha_Produccion.Location = New System.Drawing.Point(471, 17)
        Me.dtFecha_Produccion.Margin = New System.Windows.Forms.Padding(2)
        Me.dtFecha_Produccion.Name = "dtFecha_Produccion"
        Me.dtFecha_Produccion.Size = New System.Drawing.Size(112, 20)
        Me.dtFecha_Produccion.TabIndex = 33
        '
        'label14
        '
        Me.label14.AutoSize = True
        Me.label14.Location = New System.Drawing.Point(403, 20)
        Me.label14.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label14.Name = "label14"
        Me.label14.Size = New System.Drawing.Size(68, 13)
        Me.label14.TabIndex = 32
        Me.label14.Text = "Fecha Prod.:"
        '
        'txtPrecio_Venta
        '
        Me.txtPrecio_Venta.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtPrecio_Venta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPrecio_Venta.Location = New System.Drawing.Point(293, 44)
        Me.txtPrecio_Venta.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPrecio_Venta.Name = "txtPrecio_Venta"
        Me.txtPrecio_Venta.Size = New System.Drawing.Size(94, 20)
        Me.txtPrecio_Venta.TabIndex = 37
        '
        'label13
        '
        Me.label13.AutoSize = True
        Me.label13.Location = New System.Drawing.Point(218, 44)
        Me.label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label13.Name = "label13"
        Me.label13.Size = New System.Drawing.Size(71, 13)
        Me.label13.TabIndex = 36
        Me.label13.Text = "Precio Venta:"
        '
        'txtPrecio_Compra
        '
        Me.txtPrecio_Compra.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtPrecio_Compra.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPrecio_Compra.Location = New System.Drawing.Point(293, 17)
        Me.txtPrecio_Compra.Margin = New System.Windows.Forms.Padding(2)
        Me.txtPrecio_Compra.Name = "txtPrecio_Compra"
        Me.txtPrecio_Compra.Size = New System.Drawing.Size(94, 20)
        Me.txtPrecio_Compra.TabIndex = 35
        '
        'txtIdarticulo
        '
        Me.txtIdarticulo.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtIdarticulo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIdarticulo.Location = New System.Drawing.Point(19, 78)
        Me.txtIdarticulo.Margin = New System.Windows.Forms.Padding(2)
        Me.txtIdarticulo.Name = "txtIdarticulo"
        Me.txtIdarticulo.Size = New System.Drawing.Size(42, 20)
        Me.txtIdarticulo.TabIndex = 31
        '
        'txtCorrelativo
        '
        Me.txtCorrelativo.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtCorrelativo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCorrelativo.Location = New System.Drawing.Point(317, 66)
        Me.txtCorrelativo.Margin = New System.Windows.Forms.Padding(2)
        Me.txtCorrelativo.Name = "txtCorrelativo"
        Me.txtCorrelativo.Size = New System.Drawing.Size(82, 20)
        Me.txtCorrelativo.TabIndex = 25
        '
        'label8
        '
        Me.label8.AutoSize = True
        Me.label8.Location = New System.Drawing.Point(201, 67)
        Me.label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label8.Name = "label8"
        Me.label8.Size = New System.Drawing.Size(47, 13)
        Me.label8.TabIndex = 24
        Me.label8.Text = "Número:"
        '
        'cbTipo_Comprobante
        '
        Me.cbTipo_Comprobante.FormattingEnabled = True
        Me.cbTipo_Comprobante.Items.AddRange(New Object() {"TICKET", "BOLETA", "FACTURA", "GUIA REMISION"})
        Me.cbTipo_Comprobante.Location = New System.Drawing.Point(88, 62)
        Me.cbTipo_Comprobante.Margin = New System.Windows.Forms.Padding(2)
        Me.cbTipo_Comprobante.Name = "cbTipo_Comprobante"
        Me.cbTipo_Comprobante.Size = New System.Drawing.Size(92, 21)
        Me.cbTipo_Comprobante.TabIndex = 23
        Me.cbTipo_Comprobante.Text = "TICKET"
        '
        'dtFecha
        '
        Me.dtFecha.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFecha.Location = New System.Drawing.Point(478, 32)
        Me.dtFecha.Margin = New System.Windows.Forms.Padding(2)
        Me.dtFecha.Name = "dtFecha"
        Me.dtFecha.Size = New System.Drawing.Size(112, 20)
        Me.dtFecha.TabIndex = 22
        '
        'btnBuscarProveedor
        '
        Me.btnBuscarProveedor.Location = New System.Drawing.Point(366, 28)
        Me.btnBuscarProveedor.Margin = New System.Windows.Forms.Padding(2)
        Me.btnBuscarProveedor.Name = "btnBuscarProveedor"
        Me.btnBuscarProveedor.Size = New System.Drawing.Size(32, 25)
        Me.btnBuscarProveedor.TabIndex = 18
        Me.btnBuscarProveedor.UseVisualStyleBackColor = True
        '
        'txtIdproveedor
        '
        Me.txtIdproveedor.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtIdproveedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIdproveedor.Location = New System.Drawing.Point(268, 10)
        Me.txtIdproveedor.Margin = New System.Windows.Forms.Padding(2)
        Me.txtIdproveedor.Name = "txtIdproveedor"
        Me.txtIdproveedor.Size = New System.Drawing.Size(42, 20)
        Me.txtIdproveedor.TabIndex = 17
        '
        'txtProveedor
        '
        Me.txtProveedor.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtProveedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtProveedor.Location = New System.Drawing.Point(268, 32)
        Me.txtProveedor.Margin = New System.Windows.Forms.Padding(2)
        Me.txtProveedor.Name = "txtProveedor"
        Me.txtProveedor.Size = New System.Drawing.Size(94, 20)
        Me.txtProveedor.TabIndex = 16
        '
        'label7
        '
        Me.label7.AutoSize = True
        Me.label7.Location = New System.Drawing.Point(201, 34)
        Me.label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label7.Name = "label7"
        Me.label7.Size = New System.Drawing.Size(59, 13)
        Me.label7.TabIndex = 15
        Me.label7.Text = "Proveedor:"
        '
        'txtSerie
        '
        Me.txtSerie.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtSerie.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSerie.Location = New System.Drawing.Point(268, 66)
        Me.txtSerie.Margin = New System.Windows.Forms.Padding(2)
        Me.txtSerie.Name = "txtSerie"
        Me.txtSerie.Size = New System.Drawing.Size(46, 20)
        Me.txtSerie.TabIndex = 11
        '
        'label6
        '
        Me.label6.AutoSize = True
        Me.label6.Location = New System.Drawing.Point(9, 62)
        Me.label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label6.Name = "label6"
        Me.label6.Size = New System.Drawing.Size(73, 13)
        Me.label6.TabIndex = 10
        Me.label6.Text = "Comprobante:"
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.Location = New System.Drawing.Point(568, 284)
        Me.btnCancelar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(80, 27)
        Me.btnCancelar.TabIndex = 9
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGuardar.Location = New System.Drawing.Point(478, 284)
        Me.btnGuardar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(80, 27)
        Me.btnGuardar.TabIndex = 7
        Me.btnGuardar.Text = "&Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'btnNuevo
        '
        Me.btnNuevo.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnNuevo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNuevo.Location = New System.Drawing.Point(393, 284)
        Me.btnNuevo.Margin = New System.Windows.Forms.Padding(2)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(80, 26)
        Me.btnNuevo.TabIndex = 6
        Me.btnNuevo.Text = "&Nuevo"
        Me.btnNuevo.UseVisualStyleBackColor = False
        '
        'txtIdingreso
        '
        Me.txtIdingreso.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtIdingreso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtIdingreso.Location = New System.Drawing.Point(88, 32)
        Me.txtIdingreso.Margin = New System.Windows.Forms.Padding(2)
        Me.txtIdingreso.Name = "txtIdingreso"
        Me.txtIdingreso.Size = New System.Drawing.Size(94, 20)
        Me.txtIdingreso.TabIndex = 3
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Location = New System.Drawing.Point(18, 37)
        Me.label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(43, 13)
        Me.label3.TabIndex = 0
        Me.label3.Text = "Código:"
        '
        'ttMensaje
        '
        Me.ttMensaje.IsBalloon = True
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.ForeColor = System.Drawing.Color.Maroon
        Me.label1.Location = New System.Drawing.Point(30, 17)
        Me.label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(221, 29)
        Me.label1.TabIndex = 12
        Me.label1.Text = "Ingresos Almacén"
        '
        'label10
        '
        Me.label10.AutoSize = True
        Me.label10.Location = New System.Drawing.Point(414, 34)
        Me.label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label10.Name = "label10"
        Me.label10.Size = New System.Drawing.Size(40, 13)
        Me.label10.TabIndex = 21
        Me.label10.Text = "Fecha:"
        '
        'pictureBox1
        '
        Me.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pictureBox1.Location = New System.Drawing.Point(275, 17)
        Me.pictureBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.pictureBox1.Name = "pictureBox1"
        Me.pictureBox1.Size = New System.Drawing.Size(68, 48)
        Me.pictureBox1.TabIndex = 14
        Me.pictureBox1.TabStop = False
        '
        'label12
        '
        Me.label12.AutoSize = True
        Me.label12.Location = New System.Drawing.Point(218, 17)
        Me.label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label12.Name = "label12"
        Me.label12.Size = New System.Drawing.Size(79, 13)
        Me.label12.TabIndex = 34
        Me.label12.Text = "Precio Compra:"
        '
        'tabControl1
        '
        Me.tabControl1.Controls.Add(Me.tabPage1)
        Me.tabControl1.Controls.Add(Me.tabPage2)
        Me.tabControl1.Location = New System.Drawing.Point(21, 60)
        Me.tabControl1.Margin = New System.Windows.Forms.Padding(2)
        Me.tabControl1.Name = "tabControl1"
        Me.tabControl1.SelectedIndex = 0
        Me.tabControl1.Size = New System.Drawing.Size(808, 373)
        Me.tabControl1.TabIndex = 13
        '
        'tabPage1
        '
        Me.tabPage1.Controls.Add(Me.label9)
        Me.tabPage1.Controls.Add(Me.dtFecha2)
        Me.tabPage1.Controls.Add(Me.dtFecha1)
        Me.tabPage1.Controls.Add(Me.dataListado)
        Me.tabPage1.Controls.Add(Me.lblTotal)
        Me.tabPage1.Controls.Add(Me.chkEliminar)
        Me.tabPage1.Controls.Add(Me.btnImprimir)
        Me.tabPage1.Controls.Add(Me.btnEliminar)
        Me.tabPage1.Controls.Add(Me.btnBuscar)
        Me.tabPage1.Controls.Add(Me.label2)
        Me.tabPage1.Location = New System.Drawing.Point(4, 22)
        Me.tabPage1.Margin = New System.Windows.Forms.Padding(2)
        Me.tabPage1.Name = "tabPage1"
        Me.tabPage1.Padding = New System.Windows.Forms.Padding(2)
        Me.tabPage1.Size = New System.Drawing.Size(800, 347)
        Me.tabPage1.TabIndex = 0
        Me.tabPage1.Text = "Listado"
        Me.tabPage1.UseVisualStyleBackColor = True
        '
        'label9
        '
        Me.label9.AutoSize = True
        Me.label9.Location = New System.Drawing.Point(136, 15)
        Me.label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label9.Name = "label9"
        Me.label9.Size = New System.Drawing.Size(57, 13)
        Me.label9.TabIndex = 10
        Me.label9.Text = "Fecha Fin:"
        '
        'dtFecha2
        '
        Me.dtFecha2.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFecha2.Location = New System.Drawing.Point(139, 39)
        Me.dtFecha2.Margin = New System.Windows.Forms.Padding(2)
        Me.dtFecha2.Name = "dtFecha2"
        Me.dtFecha2.Size = New System.Drawing.Size(80, 20)
        Me.dtFecha2.TabIndex = 9
        '
        'dtFecha1
        '
        Me.dtFecha1.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtFecha1.Location = New System.Drawing.Point(20, 38)
        Me.dtFecha1.Margin = New System.Windows.Forms.Padding(2)
        Me.dtFecha1.Name = "dtFecha1"
        Me.dtFecha1.Size = New System.Drawing.Size(80, 20)
        Me.dtFecha1.TabIndex = 8
        '
        'dataListado
        '
        Me.dataListado.AllowUserToAddRows = False
        Me.dataListado.AllowUserToDeleteRows = False
        Me.dataListado.AllowUserToOrderColumns = True
        Me.dataListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Eliminar})
        Me.dataListado.Location = New System.Drawing.Point(10, 87)
        Me.dataListado.Margin = New System.Windows.Forms.Padding(2)
        Me.dataListado.MultiSelect = False
        Me.dataListado.Name = "dataListado"
        Me.dataListado.ReadOnly = True
        Me.dataListado.RowTemplate.Height = 24
        Me.dataListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dataListado.Size = New System.Drawing.Size(670, 241)
        Me.dataListado.TabIndex = 7
        '
        'Eliminar
        '
        Me.Eliminar.HeaderText = "Eliminar"
        Me.Eliminar.Name = "Eliminar"
        Me.Eliminar.ReadOnly = True
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(375, 64)
        Me.lblTotal.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(35, 13)
        Me.lblTotal.TabIndex = 6
        Me.lblTotal.Text = "label3"
        '
        'chkEliminar
        '
        Me.chkEliminar.AutoSize = True
        Me.chkEliminar.Location = New System.Drawing.Point(10, 64)
        Me.chkEliminar.Margin = New System.Windows.Forms.Padding(2)
        Me.chkEliminar.Name = "chkEliminar"
        Me.chkEliminar.Size = New System.Drawing.Size(56, 17)
        Me.chkEliminar.TabIndex = 5
        Me.chkEliminar.Text = "Anular"
        Me.chkEliminar.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.BackColor = System.Drawing.Color.Silver
        Me.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImprimir.Location = New System.Drawing.Point(593, 28)
        Me.btnImprimir.Margin = New System.Windows.Forms.Padding(2)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(88, 28)
        Me.btnImprimir.TabIndex = 4
        Me.btnImprimir.Text = "&Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = False
        '
        'btnEliminar
        '
        Me.btnEliminar.BackColor = System.Drawing.Color.Silver
        Me.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEliminar.Location = New System.Drawing.Point(498, 28)
        Me.btnEliminar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(81, 28)
        Me.btnEliminar.TabIndex = 3
        Me.btnEliminar.Text = "&Anular"
        Me.btnEliminar.UseVisualStyleBackColor = False
        '
        'btnBuscar
        '
        Me.btnBuscar.BackColor = System.Drawing.Color.Silver
        Me.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBuscar.Location = New System.Drawing.Point(405, 28)
        Me.btnBuscar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(79, 28)
        Me.btnBuscar.TabIndex = 2
        Me.btnBuscar.Text = "&Buscar"
        Me.btnBuscar.UseVisualStyleBackColor = False
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Location = New System.Drawing.Point(17, 15)
        Me.label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(68, 13)
        Me.label2.TabIndex = 0
        Me.label2.Text = "Fecha Inicio:"
        '
        'tabPage2
        '
        Me.tabPage2.Controls.Add(Me.groupBox1)
        Me.tabPage2.Location = New System.Drawing.Point(4, 22)
        Me.tabPage2.Margin = New System.Windows.Forms.Padding(2)
        Me.tabPage2.Name = "tabPage2"
        Me.tabPage2.Padding = New System.Windows.Forms.Padding(2)
        Me.tabPage2.Size = New System.Drawing.Size(800, 347)
        Me.tabPage2.TabIndex = 1
        Me.tabPage2.Text = "Mantenimiento"
        Me.tabPage2.UseVisualStyleBackColor = True
        '
        'groupBox1
        '
        Me.groupBox1.Controls.Add(Me.lblTotal_Pagado)
        Me.groupBox1.Controls.Add(Me.label16)
        Me.groupBox1.Controls.Add(Me.dataListadoDetalle)
        Me.groupBox1.Controls.Add(Me.groupBox2)
        Me.groupBox1.Controls.Add(Me.txtIgv)
        Me.groupBox1.Controls.Add(Me.txtIdarticulo)
        Me.groupBox1.Controls.Add(Me.label11)
        Me.groupBox1.Controls.Add(Me.txtCorrelativo)
        Me.groupBox1.Controls.Add(Me.label8)
        Me.groupBox1.Controls.Add(Me.cbTipo_Comprobante)
        Me.groupBox1.Controls.Add(Me.dtFecha)
        Me.groupBox1.Controls.Add(Me.label10)
        Me.groupBox1.Controls.Add(Me.btnBuscarProveedor)
        Me.groupBox1.Controls.Add(Me.txtIdproveedor)
        Me.groupBox1.Controls.Add(Me.txtProveedor)
        Me.groupBox1.Controls.Add(Me.label7)
        Me.groupBox1.Controls.Add(Me.txtSerie)
        Me.groupBox1.Controls.Add(Me.label6)
        Me.groupBox1.Controls.Add(Me.btnCancelar)
        Me.groupBox1.Controls.Add(Me.btnGuardar)
        Me.groupBox1.Controls.Add(Me.btnNuevo)
        Me.groupBox1.Controls.Add(Me.txtIdingreso)
        Me.groupBox1.Controls.Add(Me.label3)
        Me.groupBox1.Location = New System.Drawing.Point(10, 17)
        Me.groupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.groupBox1.Name = "groupBox1"
        Me.groupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.groupBox1.Size = New System.Drawing.Size(768, 315)
        Me.groupBox1.TabIndex = 0
        Me.groupBox1.TabStop = False
        Me.groupBox1.Text = "Ingresos Almacén"
        '
        'lblTotal_Pagado
        '
        Me.lblTotal_Pagado.AutoSize = True
        Me.lblTotal_Pagado.Location = New System.Drawing.Point(106, 291)
        Me.lblTotal_Pagado.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTotal_Pagado.Name = "lblTotal_Pagado"
        Me.lblTotal_Pagado.Size = New System.Drawing.Size(22, 13)
        Me.lblTotal_Pagado.TabIndex = 43
        Me.lblTotal_Pagado.Text = "0.0"
        '
        'label16
        '
        Me.label16.AutoSize = True
        Me.label16.Location = New System.Drawing.Point(9, 290)
        Me.label16.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label16.Name = "label16"
        Me.label16.Size = New System.Drawing.Size(95, 13)
        Me.label16.TabIndex = 42
        Me.label16.Text = "Total Pagado: S/. "
        '
        'dataListadoDetalle
        '
        Me.dataListadoDetalle.AllowUserToAddRows = False
        Me.dataListadoDetalle.AllowUserToDeleteRows = False
        Me.dataListadoDetalle.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dataListadoDetalle.Location = New System.Drawing.Point(11, 175)
        Me.dataListadoDetalle.Margin = New System.Windows.Forms.Padding(2)
        Me.dataListadoDetalle.Name = "dataListadoDetalle"
        Me.dataListadoDetalle.RowTemplate.Height = 24
        Me.dataListadoDetalle.Size = New System.Drawing.Size(637, 104)
        Me.dataListadoDetalle.TabIndex = 32
        '
        'groupBox2
        '
        Me.groupBox2.Controls.Add(Me.btnQuitar)
        Me.groupBox2.Controls.Add(Me.btnAgregar)
        Me.groupBox2.Controls.Add(Me.dtFecha_Vencimiento)
        Me.groupBox2.Controls.Add(Me.label15)
        Me.groupBox2.Controls.Add(Me.dtFecha_Produccion)
        Me.groupBox2.Controls.Add(Me.label14)
        Me.groupBox2.Controls.Add(Me.txtPrecio_Venta)
        Me.groupBox2.Controls.Add(Me.label13)
        Me.groupBox2.Controls.Add(Me.txtPrecio_Compra)
        Me.groupBox2.Controls.Add(Me.label12)
        Me.groupBox2.Controls.Add(Me.txtStock)
        Me.groupBox2.Controls.Add(Me.label5)
        Me.groupBox2.Controls.Add(Me.btnBuscarArticulo)
        Me.groupBox2.Controls.Add(Me.label4)
        Me.groupBox2.Controls.Add(Me.txtArticulo)
        Me.groupBox2.Location = New System.Drawing.Point(11, 93)
        Me.groupBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.groupBox2.Name = "groupBox2"
        Me.groupBox2.Padding = New System.Windows.Forms.Padding(2)
        Me.groupBox2.Size = New System.Drawing.Size(641, 76)
        Me.groupBox2.TabIndex = 28
        Me.groupBox2.TabStop = False
        '
        'txtStock
        '
        Me.txtStock.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtStock.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtStock.Location = New System.Drawing.Point(74, 44)
        Me.txtStock.Margin = New System.Windows.Forms.Padding(2)
        Me.txtStock.Name = "txtStock"
        Me.txtStock.Size = New System.Drawing.Size(94, 20)
        Me.txtStock.TabIndex = 33
        '
        'label5
        '
        Me.label5.AutoSize = True
        Me.label5.Location = New System.Drawing.Point(9, 44)
        Me.label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(68, 13)
        Me.label5.TabIndex = 32
        Me.label5.Text = "Stock Inicial:"
        '
        'btnBuscarArticulo
        '
        Me.btnBuscarArticulo.Location = New System.Drawing.Point(172, 10)
        Me.btnBuscarArticulo.Margin = New System.Windows.Forms.Padding(2)
        Me.btnBuscarArticulo.Name = "btnBuscarArticulo"
        Me.btnBuscarArticulo.Size = New System.Drawing.Size(32, 25)
        Me.btnBuscarArticulo.TabIndex = 32
        Me.btnBuscarArticulo.UseVisualStyleBackColor = True
        '
        'label4
        '
        Me.label4.AutoSize = True
        Me.label4.Location = New System.Drawing.Point(7, 15)
        Me.label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.label4.Name = "label4"
        Me.label4.Size = New System.Drawing.Size(47, 13)
        Me.label4.TabIndex = 29
        Me.label4.Text = "Artículo:"
        '
        'txtArticulo
        '
        Me.txtArticulo.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.txtArticulo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtArticulo.Location = New System.Drawing.Point(74, 13)
        Me.txtArticulo.Margin = New System.Windows.Forms.Padding(2)
        Me.txtArticulo.Name = "txtArticulo"
        Me.txtArticulo.Size = New System.Drawing.Size(94, 20)
        Me.txtArticulo.TabIndex = 30
        '
        'errorIcono
        '
        Me.errorIcono.ContainerControl = Me
        '
        'FrmIngreso
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(840, 510)
        Me.Controls.Add(Me.label1)
        Me.Controls.Add(Me.pictureBox1)
        Me.Controls.Add(Me.tabControl1)
        Me.Name = "FrmIngreso"
        Me.Text = "FrmIngreso"
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabControl1.ResumeLayout(False)
        Me.tabPage1.ResumeLayout(False)
        Me.tabPage1.PerformLayout()
        CType(Me.dataListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabPage2.ResumeLayout(False)
        Me.groupBox1.ResumeLayout(False)
        Me.groupBox1.PerformLayout()
        CType(Me.dataListadoDetalle, System.ComponentModel.ISupportInitialize).EndInit()
        Me.groupBox2.ResumeLayout(False)
        Me.groupBox2.PerformLayout()
        CType(Me.errorIcono, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents txtIgv As TextBox
    Private WithEvents label11 As Label
    Private WithEvents btnQuitar As Button
    Private WithEvents btnAgregar As Button
    Private WithEvents dtFecha_Vencimiento As DateTimePicker
    Private WithEvents label15 As Label
    Private WithEvents dtFecha_Produccion As DateTimePicker
    Private WithEvents label14 As Label
    Private WithEvents txtPrecio_Venta As TextBox
    Private WithEvents label13 As Label
    Private WithEvents txtPrecio_Compra As TextBox
    Private WithEvents txtIdarticulo As TextBox
    Private WithEvents txtCorrelativo As TextBox
    Private WithEvents label8 As Label
    Private WithEvents cbTipo_Comprobante As ComboBox
    Private WithEvents dtFecha As DateTimePicker
    Private WithEvents btnBuscarProveedor As Button
    Private WithEvents txtIdproveedor As TextBox
    Private WithEvents txtProveedor As TextBox
    Private WithEvents label7 As Label
    Private WithEvents txtSerie As TextBox
    Private WithEvents label6 As Label
    Private WithEvents btnCancelar As Button
    Private WithEvents btnGuardar As Button
    Private WithEvents btnNuevo As Button
    Private WithEvents txtIdingreso As TextBox
    Private WithEvents label3 As Label
    Private WithEvents ttMensaje As ToolTip
    Private WithEvents label1 As Label
    Private WithEvents label10 As Label
    Private WithEvents pictureBox1 As PictureBox
    Private WithEvents label12 As Label
    Private WithEvents tabControl1 As TabControl
    Private WithEvents tabPage1 As TabPage
    Private WithEvents label9 As Label
    Private WithEvents dtFecha2 As DateTimePicker
    Private WithEvents dtFecha1 As DateTimePicker
    Private WithEvents dataListado As DataGridView
    Private WithEvents Eliminar As DataGridViewCheckBoxColumn
    Private WithEvents lblTotal As Label
    Private WithEvents chkEliminar As CheckBox
    Private WithEvents btnImprimir As Button
    Private WithEvents btnEliminar As Button
    Private WithEvents btnBuscar As Button
    Private WithEvents label2 As Label
    Private WithEvents tabPage2 As TabPage
    Private WithEvents groupBox1 As GroupBox
    Private WithEvents lblTotal_Pagado As Label
    Private WithEvents label16 As Label
    Private WithEvents dataListadoDetalle As DataGridView
    Private WithEvents groupBox2 As GroupBox
    Private WithEvents txtStock As TextBox
    Private WithEvents label5 As Label
    Private WithEvents btnBuscarArticulo As Button
    Private WithEvents label4 As Label
    Private WithEvents txtArticulo As TextBox
    Private WithEvents errorIcono As ErrorProvider
End Class
