﻿Imports System.Configuration
Imports System.Data.SqlClient
Public Class frmvistalistaprecios
    Dim conexion As New SqlConnection(ConfigurationManager.ConnectionStrings("BRAVOSPORT.My.MySettings.marketConnectionString").ConnectionString)
    Private dt As New DataTable
    Dim consulta3 As New SqlCommand
    Dim cadena As String
    Dim datos As New DataSet
    Dim variable As SqlDataReader
    Private Sub frmvistalistaprecios_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        mlistapreciospm()
    End Sub
    Private Sub ocultar_columnas()
        datalistado.Columns(0).Visible = False
        datalistado.Columns(4).Visible = False

    End Sub
    Private Sub mlistapreciospm()
        Try
            Dim func As New flistaprecios
            dt = func.mlistapreciosp()
            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt
                datalistado.ColumnHeadersVisible = True

            Else
                datalistado.DataSource = Nothing
                datalistado.ColumnHeadersVisible = False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try


    End Sub

    Private Sub datalistado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellDoubleClick
        If txtflag.Text = "1" Then


            frmdetalle_venta.cbosundm.Text = datalistado.SelectedCells.Item(0).Value
            frmdetalle_venta.cboundm.Text = datalistado.SelectedCells.Item(1).Value
            frmdetalle_venta.txtprecio_unitariom.Text = datalistado.SelectedCells.Item(2).Value
            frmdetalle_venta.txtcundm.Text = datalistado.SelectedCells.Item(3).Value
            frmdetalle_venta.txtpcompra.Text = datalistado.SelectedCells.Item(5).Value

            frmdetalle_venta.txtcantidad.Text = 1

            Me.Close()
        End If
    End Sub
End Class