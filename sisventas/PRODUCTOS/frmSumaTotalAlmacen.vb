﻿Imports System.Configuration
Imports System.Data.SqlClient
Public Class frmSumaTotalAlmacen
    Dim conexion As New SqlConnection(ConfigurationManager.ConnectionStrings("BRAVOSPORT.My.MySettings.marketConnectionString").ConnectionString)
    Private dt As New DataTable
    Dim consulta3 As New SqlCommand
    Dim cadena As String
    Dim datos As New DataSet
    Dim variable As SqlDataReader
    Private Sub frmSumaTotalAlmacen_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        mostrar_sumatotal_almacen()

    End Sub

    Private Sub mostrar_sumatotal_almacen()
        Try
            Dim func As New flistaprecios
            dt = func.mostrar_sumatotal_almacen
            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt
                datalistado.ColumnHeadersVisible = True

            Else
                datalistado.DataSource = Nothing
                datalistado.ColumnHeadersVisible = False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try


    End Sub

End Class