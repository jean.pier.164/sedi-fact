﻿Imports System.Net
Imports System.Web.Script.Serialization

Public Class enviar_facturador
    Public Sub enviar_sunat()
        Dim url As String = "http://demo.sediperu.com/api/documents"
        Using client As New WebClient()
            client.Headers("Content-Type") = "application/json"
            client.Headers("Authorization") = "Bearer BueYobdWiXGVM9ZYBNgpfSrDYCEhcb7HfU2bz7d6vGYfcFHCVr"


            Dim enviar As Enviar = New Enviar
            Dim cliente As DatosDelClienteOReceptor = New DatosDelClienteOReceptor
            Dim totales As Totales = New Totales
            ' Dim item As Item
            'Dim item = New Item(1) {}


            'Dim item() As Item = New Item(0) {}
            Dim item(0) As Item



            cliente.codigo_tipo_documento_identidad = "6"
            cliente.numero_documento = "10414711225"
            cliente.apellidos_y_nombres_o_razon_social = "EMPRESA XYZ S.A."
            cliente.codigo_pais = "PE"
            cliente.ubigeo = "150101"
            cliente.direccion = "Av. 2 de Mayo"
            cliente.correo_electronico = "demo@gmail.com"
            cliente.telefono = "921565256"

            totales.total_exportacion = 0
            totales.total_operaciones_gravadas = 100
            totales.total_operaciones_exoneradas = 0
            totales.total_operaciones_gratuitas = 0
            totales.total_igv = 18
            totales.total_impuestos = 18
            totales.total_valor = 100
            totales.total_venta = 118



            item(0) = New Item("P0121", "Inca Kola 250 ml", "51121703", "NIU", 2, 50, "01", 59, "10", 100, 18, 18, 18, 100, 118)
            Console.Write("hola" & item(0).descripcion & item.Length)

            enviar.serie_documento = "F001"
            enviar.numero_documento = "#"
            enviar.fecha_de_emision = "2018-10-09"
            enviar.hora_de_emision = "10:11:11"
            enviar.codigo_tipo_operacion = "0101"
            enviar.codigo_tipo_documento = "01"
            enviar.codigo_tipo_moneda = "PEN"
            enviar.fecha_de_vencimiento = "2018-08-30"
            enviar.numero_orden_de_compra = "0045467898"
            enviar.datos_del_cliente_o_receptor = cliente
            enviar.totales = totales
            enviar.items = item
            enviar.informacion_adicional = "Forma de pago:Efectivo|Caja: 1"






            '  Dim Str As String = JsonConvert.SerializeObject(Obj)
            Dim orderString As String = "" + (New JavaScriptSerializer()).Serialize(enviar) + ""
            ' url = String.Format("{0}/api/push_order", server)
            'client.UploadData(url, "POST", orderInBytes)
            ''This returns "'The remote server returned an error: (400) Bad Request.'"
            client.UploadString(url, "POST", orderString)

        End Using


    End Sub


    Public Class DatosDelClienteOReceptor
        Public Property codigo_tipo_documento_identidad As String
        Public Property numero_documento As String
        Public Property apellidos_y_nombres_o_razon_social As String
        Public Property codigo_pais As String
        Public Property ubigeo As String
        Public Property direccion As String
        Public Property correo_electronico As String
        Public Property telefono As String
    End Class

    Public Class Totales
        Public Property total_exportacion As Double
        Public Property total_operaciones_gravadas As Double
        Public Property total_operaciones_inafectas As Double
        Public Property total_operaciones_exoneradas As Double
        Public Property total_operaciones_gratuitas As Double
        Public Property total_igv As Double
        Public Property total_impuestos As Double
        Public Property total_valor As Integer
        Public Property total_venta As Integer
    End Class

    Public Class Item
        Public Property codigo_interno As String
        Public Property descripcion As String
        Public Property codigo_producto_sunat As String
        Public Property unidad_de_medida As String
        Public Property cantidad As Integer
        Public Property valor_unitario As Integer
        Public Property codigo_tipo_precio As String
        Public Property precio_unitario As Integer
        Public Property codigo_tipo_afectacion_igv As String
        Public Property total_base_igv As Double
        Public Property porcentaje_igv As Integer
        Public Property total_igv As Integer
        Public Property total_impuestos As Integer
        Public Property total_valor_item As Integer
        Public Property total_item As Integer

        Public Sub New(ByVal codigo_interno As String, ByVal descripcion As String, ByVal codigo_producto_sunat As String,
                ByVal unidad_de_medida As String, ByVal cantidad As Integer, ByVal valor_unitario As Integer,
                ByVal codigo_tipo_precio As String, ByVal precio_unitario As Integer, ByVal codigo_tipo_afectacion_igv As String,
                ByVal total_base_igv As Double, ByVal porcentaje_igv As Integer, ByVal total_igv As Integer,
                ByVal total_impuestos As Integer, ByVal total_valor_item As Integer, ByVal total_item As Integer
            )

            Me.codigo_interno = codigo_interno
            Me.descripcion = descripcion
            Me.codigo_producto_sunat = codigo_producto_sunat
            Me.unidad_de_medida = unidad_de_medida
            Me.cantidad = cantidad
            Me.valor_unitario = valor_unitario
            Me.codigo_tipo_precio = codigo_tipo_precio
            Me.precio_unitario = precio_unitario
            Me.codigo_tipo_afectacion_igv = codigo_tipo_afectacion_igv
            Me.total_base_igv = total_base_igv
            Me.porcentaje_igv = porcentaje_igv
            Me.total_igv = total_igv
            Me.total_impuestos = total_impuestos
            Me.total_valor_item = total_valor_item
            Me.total_item = total_item
        End Sub


    End Class

    Public Class Enviar
        Public Property serie_documento As String
        Public Property numero_documento As String
        Public Property fecha_de_emision As String
        Public Property hora_de_emision As String
        Public Property codigo_tipo_operacion As String
        Public Property codigo_tipo_documento As String
        Public Property codigo_tipo_moneda As String
        Public Property fecha_de_vencimiento As String
        Public Property numero_orden_de_compra As String
        Public Property datos_del_cliente_o_receptor As DatosDelClienteOReceptor
        Public Property totales As Totales
        Public Property items As Item()
        Public Property informacion_adicional As String
    End Class
End Class
