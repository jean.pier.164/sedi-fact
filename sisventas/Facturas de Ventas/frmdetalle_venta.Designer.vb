﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmdetalle_venta
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmdetalle_venta))
        Me.generar_comprobantemBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.marketdscomprobante = New BRAVOSPORT.marketdscomprobante()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtdescrip_comprobante = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtcorrelativo = New System.Windows.Forms.TextBox()
        Me.txtidusuario = New System.Windows.Forms.TextBox()
        Me.txtcolaborador = New System.Windows.Forms.TextBox()
        Me.txtusuario = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtcodsunat = New System.Windows.Forms.TextBox()
        Me.txtnrocaja = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtCpais = New System.Windows.Forms.TextBox()
        Me.Tipo_document = New System.Windows.Forms.Label()
        Me.txttipo_documento = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtdireccion = New System.Windows.Forms.TextBox()
        Me.txtubigueo = New System.Windows.Forms.TextBox()
        Me.txt_ruc = New System.Windows.Forms.TextBox()
        Me.btneditarVenta = New System.Windows.Forms.Button()
        Me.txtfecha = New System.Windows.Forms.TextBox()
        Me.txtserie_documento = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtnombre_cliente = New System.Windows.Forms.TextBox()
        Me.serie_docu = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtidcliente = New System.Windows.Forms.TextBox()
        Me.txtidventa = New System.Windows.Forms.TextBox()
        Me.cbtipo_documento = New System.Windows.Forms.ComboBox()
        Me.btnbuscar_cliente = New System.Windows.Forms.Button()
        Me.txtstock = New System.Windows.Forms.NumericUpDown()
        Me.txtcantidad = New System.Windows.Forms.NumericUpDown()
        Me.txtprecio_unitario = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtnombre_producto = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtidproducto = New System.Windows.Forms.TextBox()
        Me.btnguardar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnna5 = New System.Windows.Forms.Button()
        Me.lblletras = New System.Windows.Forms.Label()
        Me.btnA5 = New System.Windows.Forms.Button()
        Me.btnsalirr = New System.Windows.Forms.Button()
        Me.btnenviar_fact = New System.Windows.Forms.Button()
        Me.chkservi = New System.Windows.Forms.CheckBox()
        Me.btnEnviarLuegoSunat = New System.Windows.Forms.Button()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblsubtotal = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lbligv = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblvuelto = New System.Windows.Forms.Label()
        Me.lbltotal = New System.Windows.Forms.Label()
        Me.txtdinero = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnNv5 = New System.Windows.Forms.Button()
        Me.btnFactura = New System.Windows.Forms.Button()
        Me.btnBoleta = New System.Windows.Forms.Button()
        Me.btnFacturaI = New System.Windows.Forms.Button()
        Me.btnboletainterna = New System.Windows.Forms.Button()
        Me.btnEnviarSunat = New System.Windows.Forms.Button()
        Me.btnmodificar = New System.Windows.Forms.Button()
        Me.cbosundm = New System.Windows.Forms.ComboBox()
        Me.btnpedido = New System.Windows.Forms.Button()
        Me.chactivar = New System.Windows.Forms.CheckBox()
        Me.txtprecio_unitariom = New System.Windows.Forms.TextBox()
        Me.txtcanttotal = New System.Windows.Forms.TextBox()
        Me.txtcundm = New System.Windows.Forms.TextBox()
        Me.btnbuscarlistaprecio = New System.Windows.Forms.Button()
        Me.cboundm = New System.Windows.Forms.ComboBox()
        Me.inexistente = New System.Windows.Forms.Label()
        Me.datalistado = New System.Windows.Forms.DataGridView()
        Me.txtbuscar = New System.Windows.Forms.TextBox()
        Me.cbocampo = New System.Windows.Forms.ComboBox()
        Me.cboproduct = New System.Windows.Forms.ComboBox()
        Me.ProductoBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Mmdsnombre = New BRAVOSPORT.mmdsnombre()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtpcompra = New System.Windows.Forms.TextBox()
        Me.txtflag = New System.Windows.Forms.TextBox()
        Me.A = New System.Windows.Forms.Button()
        Me.ProductoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MarketmayoristaDataSet = New BRAVOSPORT.marketmayoristaDataSet()
        Me.MarketDataSetdosfechas = New BRAVOSPORT.marketDataSetdosfechas()
        Me.erroricono = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.generar_comprobantemTableAdapter = New BRAVOSPORT.marketdscomprobanteTableAdapters.generar_comprobantemTableAdapter()
        Me.recibos = New BRAVOSPORT.recibos()
        Me.MarketDataSetticket1 = New BRAVOSPORT.marketDataSetticket()
        Me.BindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Generar_comprobanteTableAdapter1 = New BRAVOSPORT.marketDataSetticketTableAdapters.generar_comprobanteTableAdapter()
        Me.generar_comprobanteTableAdapter = New BRAVOSPORT.marketDataSetticketTableAdapters.generar_comprobanteTableAdapter()
        Me.generar_comprobanteBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.marketDataSetticket = New BRAVOSPORT.marketDataSetticket()
        Me.ProductoTableAdapter = New BRAVOSPORT.marketmayoristaDataSetTableAdapters.productoTableAdapter()
        Me.Generar_comprobantemTableAdapter1 = New BRAVOSPORT.recibosTableAdapters.generar_comprobantemTableAdapter()
        Me.ProductoTableAdapter1 = New BRAVOSPORT.mmdsnombreTableAdapters.productoTableAdapter()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.Button2 = New System.Windows.Forms.Button()
        CType(Me.generar_comprobantemBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.marketdscomprobante, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.txtstock, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.txtcantidad, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductoBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Mmdsnombre, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProductoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MarketmayoristaDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MarketDataSetdosfechas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.erroricono, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.recibos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MarketDataSetticket1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.generar_comprobanteBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.marketDataSetticket, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'generar_comprobantemBindingSource
        '
        Me.generar_comprobantemBindingSource.DataMember = "generar_comprobantem"
        Me.generar_comprobantemBindingSource.DataSource = Me.marketdscomprobante
        '
        'marketdscomprobante
        '
        Me.marketdscomprobante.DataSetName = "marketdscomprobante"
        Me.marketdscomprobante.EnforceConstraints = False
        Me.marketdscomprobante.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ReportViewer1)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtdescrip_comprobante)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.txtcorrelativo)
        Me.GroupBox1.Controls.Add(Me.txtidusuario)
        Me.GroupBox1.Controls.Add(Me.txtcolaborador)
        Me.GroupBox1.Controls.Add(Me.txtusuario)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.txtcodsunat)
        Me.GroupBox1.Controls.Add(Me.txtnrocaja)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtCpais)
        Me.GroupBox1.Controls.Add(Me.Tipo_document)
        Me.GroupBox1.Controls.Add(Me.txttipo_documento)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtdireccion)
        Me.GroupBox1.Controls.Add(Me.txtubigueo)
        Me.GroupBox1.Controls.Add(Me.txt_ruc)
        Me.GroupBox1.Controls.Add(Me.btneditarVenta)
        Me.GroupBox1.Controls.Add(Me.txtfecha)
        Me.GroupBox1.Controls.Add(Me.txtserie_documento)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.txtnombre_cliente)
        Me.GroupBox1.Controls.Add(Me.serie_docu)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtidcliente)
        Me.GroupBox1.Controls.Add(Me.txtidventa)
        Me.GroupBox1.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox1.Location = New System.Drawing.Point(12, 63)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(363, 536)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        '
        'ReportViewer1
        '
        Me.ReportViewer1.DocumentMapWidth = 8
        Me.ReportViewer1.LocalReport.EnableExternalImages = True
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "BRAVOSPORT.ticket.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 16)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(328, 520)
        Me.ReportViewer1.TabIndex = 24
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label4.Location = New System.Drawing.Point(17, 361)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(158, 16)
        Me.Label4.TabIndex = 103
        Me.Label4.Text = "Descripcion Documento"
        '
        'txtdescrip_comprobante
        '
        Me.txtdescrip_comprobante.Location = New System.Drawing.Point(3, 380)
        Me.txtdescrip_comprobante.Name = "txtdescrip_comprobante"
        Me.txtdescrip_comprobante.Size = New System.Drawing.Size(217, 25)
        Me.txtdescrip_comprobante.TabIndex = 102
        Me.txtdescrip_comprobante.Text = "NOTA DE VENTA"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label16.Location = New System.Drawing.Point(179, 313)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(59, 16)
        Me.Label16.TabIndex = 101
        Me.Label16.Text = "NroCaja"
        '
        'txtcorrelativo
        '
        Me.txtcorrelativo.Location = New System.Drawing.Point(255, 16)
        Me.txtcorrelativo.Name = "txtcorrelativo"
        Me.txtcorrelativo.Size = New System.Drawing.Size(73, 25)
        Me.txtcorrelativo.TabIndex = 91
        '
        'txtidusuario
        '
        Me.txtidusuario.Enabled = False
        Me.txtidusuario.Location = New System.Drawing.Point(77, 19)
        Me.txtidusuario.Name = "txtidusuario"
        Me.txtidusuario.Size = New System.Drawing.Size(15, 25)
        Me.txtidusuario.TabIndex = 6
        Me.txtidusuario.Visible = False
        '
        'txtcolaborador
        '
        Me.txtcolaborador.Location = New System.Drawing.Point(0, 19)
        Me.txtcolaborador.Name = "txtcolaborador"
        Me.txtcolaborador.Size = New System.Drawing.Size(15, 25)
        Me.txtcolaborador.TabIndex = 40
        '
        'txtusuario
        '
        Me.txtusuario.Enabled = False
        Me.txtusuario.Location = New System.Drawing.Point(62, 16)
        Me.txtusuario.Name = "txtusuario"
        Me.txtusuario.Size = New System.Drawing.Size(10, 25)
        Me.txtusuario.TabIndex = 7
        Me.txtusuario.Visible = False
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label13.Location = New System.Drawing.Point(22, 328)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(70, 16)
        Me.Label13.TabIndex = 100
        Me.Label13.Text = "CodSunat"
        '
        'txtcodsunat
        '
        Me.txtcodsunat.Enabled = False
        Me.txtcodsunat.Location = New System.Drawing.Point(3, 300)
        Me.txtcodsunat.Name = "txtcodsunat"
        Me.txtcodsunat.Size = New System.Drawing.Size(61, 25)
        Me.txtcodsunat.TabIndex = 99
        Me.txtcodsunat.Text = "3"
        '
        'txtnrocaja
        '
        Me.txtnrocaja.Enabled = False
        Me.txtnrocaja.Location = New System.Drawing.Point(134, 300)
        Me.txtnrocaja.Name = "txtnrocaja"
        Me.txtnrocaja.Size = New System.Drawing.Size(83, 25)
        Me.txtnrocaja.TabIndex = 98
        Me.txtnrocaja.Text = "01"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label11.Location = New System.Drawing.Point(17, 283)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(42, 16)
        Me.Label11.TabIndex = 97
        Me.Label11.Text = "PAIS:"
        '
        'txtCpais
        '
        Me.txtCpais.Enabled = False
        Me.txtCpais.Location = New System.Drawing.Point(0, 270)
        Me.txtCpais.Name = "txtCpais"
        Me.txtCpais.Size = New System.Drawing.Size(124, 25)
        Me.txtCpais.TabIndex = 96
        '
        'Tipo_document
        '
        Me.Tipo_document.AutoSize = True
        Me.Tipo_document.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Tipo_document.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Tipo_document.Location = New System.Drawing.Point(19, 158)
        Me.Tipo_document.Name = "Tipo_document"
        Me.Tipo_document.Size = New System.Drawing.Size(68, 16)
        Me.Tipo_document.TabIndex = 95
        Me.Tipo_document.Text = "Tipo Doc:"
        '
        'txttipo_documento
        '
        Me.txttipo_documento.Location = New System.Drawing.Point(0, 145)
        Me.txttipo_documento.Name = "txttipo_documento"
        Me.txttipo_documento.Size = New System.Drawing.Size(124, 25)
        Me.txttipo_documento.TabIndex = 94
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label5.Location = New System.Drawing.Point(18, 225)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(71, 16)
        Me.Label5.TabIndex = 93
        Me.Label5.Text = "UBIGUEO:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label1.Location = New System.Drawing.Point(17, 252)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 16)
        Me.Label1.TabIndex = 92
        Me.Label1.Text = "DIRECCION :"
        '
        'txtdireccion
        '
        Me.txtdireccion.Enabled = False
        Me.txtdireccion.Location = New System.Drawing.Point(0, 234)
        Me.txtdireccion.Name = "txtdireccion"
        Me.txtdireccion.Size = New System.Drawing.Size(124, 25)
        Me.txtdireccion.TabIndex = 91
        '
        'txtubigueo
        '
        Me.txtubigueo.Enabled = False
        Me.txtubigueo.Location = New System.Drawing.Point(0, 207)
        Me.txtubigueo.Name = "txtubigueo"
        Me.txtubigueo.Size = New System.Drawing.Size(124, 25)
        Me.txtubigueo.TabIndex = 90
        '
        'txt_ruc
        '
        Me.txt_ruc.Enabled = False
        Me.txt_ruc.Location = New System.Drawing.Point(0, 176)
        Me.txt_ruc.Name = "txt_ruc"
        Me.txt_ruc.Size = New System.Drawing.Size(124, 25)
        Me.txt_ruc.TabIndex = 89
        '
        'btneditarVenta
        '
        Me.btneditarVenta.BackColor = System.Drawing.SystemColors.Highlight
        Me.btneditarVenta.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btneditarVenta.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btneditarVenta.Location = New System.Drawing.Point(6, 480)
        Me.btneditarVenta.Name = "btneditarVenta"
        Me.btneditarVenta.Size = New System.Drawing.Size(104, 37)
        Me.btneditarVenta.TabIndex = 84
        Me.btneditarVenta.Text = "Editar Venta"
        Me.btneditarVenta.UseVisualStyleBackColor = False
        '
        'txtfecha
        '
        Me.txtfecha.Location = New System.Drawing.Point(0, 78)
        Me.txtfecha.Name = "txtfecha"
        Me.txtfecha.Size = New System.Drawing.Size(124, 25)
        Me.txtfecha.TabIndex = 88
        '
        'txtserie_documento
        '
        Me.txtserie_documento.Location = New System.Drawing.Point(0, 118)
        Me.txtserie_documento.Name = "txtserie_documento"
        Me.txtserie_documento.Size = New System.Drawing.Size(124, 25)
        Me.txtserie_documento.TabIndex = 48
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label10.Location = New System.Drawing.Point(18, 200)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(69, 16)
        Me.Label10.TabIndex = 20
        Me.Label10.Text = "RUC/DNI :"
        '
        'txtnombre_cliente
        '
        Me.txtnombre_cliente.Enabled = False
        Me.txtnombre_cliente.Location = New System.Drawing.Point(0, 47)
        Me.txtnombre_cliente.Name = "txtnombre_cliente"
        Me.txtnombre_cliente.Size = New System.Drawing.Size(124, 25)
        Me.txtnombre_cliente.TabIndex = 16
        '
        'serie_docu
        '
        Me.serie_docu.AutoSize = True
        Me.serie_docu.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.serie_docu.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.serie_docu.Location = New System.Drawing.Point(18, 127)
        Me.serie_docu.Name = "serie_docu"
        Me.serie_docu.Size = New System.Drawing.Size(74, 16)
        Me.serie_docu.TabIndex = 7
        Me.serie_docu.Text = "Serie Doc:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label3.Location = New System.Drawing.Point(27, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 16)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Fecha :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label2.Location = New System.Drawing.Point(21, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 16)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Cliente :"
        '
        'txtidcliente
        '
        Me.txtidcliente.Enabled = False
        Me.txtidcliente.Location = New System.Drawing.Point(130, 78)
        Me.txtidcliente.Name = "txtidcliente"
        Me.txtidcliente.Size = New System.Drawing.Size(29, 25)
        Me.txtidcliente.TabIndex = 2
        '
        'txtidventa
        '
        Me.txtidventa.Enabled = False
        Me.txtidventa.Location = New System.Drawing.Point(111, 16)
        Me.txtidventa.Name = "txtidventa"
        Me.txtidventa.Size = New System.Drawing.Size(124, 25)
        Me.txtidventa.TabIndex = 0
        '
        'cbtipo_documento
        '
        Me.cbtipo_documento.FormattingEnabled = True
        Me.cbtipo_documento.Items.AddRange(New Object() {"CONTADO", "CREDITO", "TARJETA"})
        Me.cbtipo_documento.Location = New System.Drawing.Point(112, 87)
        Me.cbtipo_documento.Name = "cbtipo_documento"
        Me.cbtipo_documento.Size = New System.Drawing.Size(95, 26)
        Me.cbtipo_documento.TabIndex = 19
        Me.cbtipo_documento.Text = "CONTADO"
        '
        'btnbuscar_cliente
        '
        Me.btnbuscar_cliente.BackColor = System.Drawing.SystemColors.MenuHighlight
        Me.btnbuscar_cliente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnbuscar_cliente.ForeColor = System.Drawing.Color.White
        Me.btnbuscar_cliente.Location = New System.Drawing.Point(267, 5)
        Me.btnbuscar_cliente.Name = "btnbuscar_cliente"
        Me.btnbuscar_cliente.Size = New System.Drawing.Size(35, 27)
        Me.btnbuscar_cliente.TabIndex = 23
        Me.btnbuscar_cliente.Text = "..."
        Me.btnbuscar_cliente.UseVisualStyleBackColor = False
        '
        'txtstock
        '
        Me.txtstock.Enabled = False
        Me.txtstock.Font = New System.Drawing.Font("Arial Black", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtstock.Location = New System.Drawing.Point(814, 42)
        Me.txtstock.Maximum = New Decimal(New Integer() {20000, 0, 0, 0})
        Me.txtstock.Name = "txtstock"
        Me.txtstock.Size = New System.Drawing.Size(80, 34)
        Me.txtstock.TabIndex = 29
        Me.txtstock.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'txtcantidad
        '
        Me.txtcantidad.Font = New System.Drawing.Font("Arial Black", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcantidad.Location = New System.Drawing.Point(743, 43)
        Me.txtcantidad.Maximum = New Decimal(New Integer() {2000, 0, 0, 0})
        Me.txtcantidad.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.txtcantidad.Name = "txtcantidad"
        Me.txtcantidad.Size = New System.Drawing.Size(61, 34)
        Me.txtcantidad.TabIndex = 28
        Me.txtcantidad.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'txtprecio_unitario
        '
        Me.txtprecio_unitario.Font = New System.Drawing.Font("Arial Black", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtprecio_unitario.ForeColor = System.Drawing.Color.Magenta
        Me.txtprecio_unitario.Location = New System.Drawing.Point(673, 44)
        Me.txtprecio_unitario.Name = "txtprecio_unitario"
        Me.txtprecio_unitario.Size = New System.Drawing.Size(62, 34)
        Me.txtprecio_unitario.TabIndex = 27
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label9.Location = New System.Drawing.Point(671, 17)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(75, 19)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "P.Venta :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label7.Location = New System.Drawing.Point(746, 17)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 19)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Cant:"
        '
        'txtnombre_producto
        '
        Me.txtnombre_producto.Location = New System.Drawing.Point(213, 52)
        Me.txtnombre_producto.Name = "txtnombre_producto"
        Me.txtnombre_producto.Size = New System.Drawing.Size(456, 25)
        Me.txtnombre_producto.TabIndex = 22
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label6.Location = New System.Drawing.Point(409, 17)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(94, 19)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Producto  :"
        '
        'txtidproducto
        '
        Me.txtidproducto.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtidproducto.Location = New System.Drawing.Point(950, 42)
        Me.txtidproducto.Name = "txtidproducto"
        Me.txtidproducto.Size = New System.Drawing.Size(37, 20)
        Me.txtidproducto.TabIndex = 20
        '
        'btnguardar
        '
        Me.btnguardar.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnguardar.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnguardar.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnguardar.Location = New System.Drawing.Point(1, 82)
        Me.btnguardar.Name = "btnguardar"
        Me.btnguardar.Size = New System.Drawing.Size(106, 37)
        Me.btnguardar.TabIndex = 15
        Me.btnguardar.Text = "Agregar"
        Me.btnguardar.UseVisualStyleBackColor = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnna5)
        Me.GroupBox2.Controls.Add(Me.lblletras)
        Me.GroupBox2.Controls.Add(Me.btnA5)
        Me.GroupBox2.Controls.Add(Me.btnsalirr)
        Me.GroupBox2.Controls.Add(Me.btnenviar_fact)
        Me.GroupBox2.Controls.Add(Me.chkservi)
        Me.GroupBox2.Controls.Add(Me.btnguardar)
        Me.GroupBox2.Controls.Add(Me.btnEnviarLuegoSunat)
        Me.GroupBox2.Controls.Add(Me.txtnombre_producto)
        Me.GroupBox2.Controls.Add(Me.cbtipo_documento)
        Me.GroupBox2.Controls.Add(Me.GroupBox4)
        Me.GroupBox2.Controls.Add(Me.GroupBox3)
        Me.GroupBox2.Controls.Add(Me.cbosundm)
        Me.GroupBox2.Controls.Add(Me.btnpedido)
        Me.GroupBox2.Controls.Add(Me.chactivar)
        Me.GroupBox2.Controls.Add(Me.txtprecio_unitariom)
        Me.GroupBox2.Controls.Add(Me.txtcanttotal)
        Me.GroupBox2.Controls.Add(Me.txtcundm)
        Me.GroupBox2.Controls.Add(Me.btnbuscarlistaprecio)
        Me.GroupBox2.Controls.Add(Me.cboundm)
        Me.GroupBox2.Controls.Add(Me.inexistente)
        Me.GroupBox2.Controls.Add(Me.datalistado)
        Me.GroupBox2.Controls.Add(Me.txtbuscar)
        Me.GroupBox2.Controls.Add(Me.cbocampo)
        Me.GroupBox2.Controls.Add(Me.cboproduct)
        Me.GroupBox2.Controls.Add(Me.txtprecio_unitario)
        Me.GroupBox2.Controls.Add(Me.txtstock)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.txtcantidad)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.txtpcompra)
        Me.GroupBox2.Controls.Add(Me.txtflag)
        Me.GroupBox2.Controls.Add(Me.txtidproducto)
        Me.GroupBox2.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox2.Location = New System.Drawing.Point(381, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(957, 598)
        Me.GroupBox2.TabIndex = 5
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "VENTAS"
        '
        'btnna5
        '
        Me.btnna5.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnna5.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnna5.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnna5.Location = New System.Drawing.Point(814, 541)
        Me.btnna5.Name = "btnna5"
        Me.btnna5.Size = New System.Drawing.Size(78, 34)
        Me.btnna5.TabIndex = 94
        Me.btnna5.Text = "N. A5"
        Me.btnna5.UseVisualStyleBackColor = False
        '
        'lblletras
        '
        Me.lblletras.AutoSize = True
        Me.lblletras.Font = New System.Drawing.Font("Arial", 12.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblletras.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lblletras.Location = New System.Drawing.Point(432, 545)
        Me.lblletras.Name = "lblletras"
        Me.lblletras.Size = New System.Drawing.Size(12, 18)
        Me.lblletras.TabIndex = 92
        Me.lblletras.Text = "."
        '
        'btnA5
        '
        Me.btnA5.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnA5.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnA5.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnA5.Location = New System.Drawing.Point(814, 501)
        Me.btnA5.Name = "btnA5"
        Me.btnA5.Size = New System.Drawing.Size(78, 34)
        Me.btnA5.TabIndex = 93
        Me.btnA5.Text = "A5"
        Me.btnA5.UseVisualStyleBackColor = False
        '
        'btnsalirr
        '
        Me.btnsalirr.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnsalirr.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsalirr.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnsalirr.Location = New System.Drawing.Point(812, 431)
        Me.btnsalirr.Name = "btnsalirr"
        Me.btnsalirr.Size = New System.Drawing.Size(80, 70)
        Me.btnsalirr.TabIndex = 20
        Me.btnsalirr.Text = "Nueva Venta"
        Me.btnsalirr.UseVisualStyleBackColor = False
        '
        'btnenviar_fact
        '
        Me.btnenviar_fact.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnenviar_fact.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnenviar_fact.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnenviar_fact.Location = New System.Drawing.Point(835, 438)
        Me.btnenviar_fact.Name = "btnenviar_fact"
        Me.btnenviar_fact.Size = New System.Drawing.Size(59, 31)
        Me.btnenviar_fact.TabIndex = 88
        Me.btnenviar_fact.Text = "Tes Fact"
        Me.btnenviar_fact.UseVisualStyleBackColor = False
        '
        'chkservi
        '
        Me.chkservi.AutoSize = True
        Me.chkservi.Location = New System.Drawing.Point(9, 21)
        Me.chkservi.Name = "chkservi"
        Me.chkservi.Size = New System.Drawing.Size(86, 22)
        Me.chkservi.TabIndex = 87
        Me.chkservi.Text = "Servicio"
        Me.chkservi.UseVisualStyleBackColor = True
        '
        'btnEnviarLuegoSunat
        '
        Me.btnEnviarLuegoSunat.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnEnviarLuegoSunat.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnviarLuegoSunat.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnEnviarLuegoSunat.Location = New System.Drawing.Point(835, 438)
        Me.btnEnviarLuegoSunat.Name = "btnEnviarLuegoSunat"
        Me.btnEnviarLuegoSunat.Size = New System.Drawing.Size(57, 50)
        Me.btnEnviarLuegoSunat.TabIndex = 84
        Me.btnEnviarLuegoSunat.Text = "Enviar luego a Sunat"
        Me.btnEnviarLuegoSunat.UseVisualStyleBackColor = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label17)
        Me.GroupBox4.Controls.Add(Me.Label12)
        Me.GroupBox4.Controls.Add(Me.lblsubtotal)
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Controls.Add(Me.lbligv)
        Me.GroupBox4.Controls.Add(Me.Label15)
        Me.GroupBox4.Controls.Add(Me.lblvuelto)
        Me.GroupBox4.Controls.Add(Me.lbltotal)
        Me.GroupBox4.Controls.Add(Me.txtdinero)
        Me.GroupBox4.Location = New System.Drawing.Point(425, 440)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(381, 102)
        Me.GroupBox4.TabIndex = 85
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "GroupBox4"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Fuchsia
        Me.Label17.Location = New System.Drawing.Point(250, 67)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(57, 24)
        Me.Label17.TabIndex = 91
        Me.Label17.Text = "IGV :"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label12.Location = New System.Drawing.Point(6, 3)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(96, 24)
        Me.Label12.TabIndex = 38
        Me.Label12.Text = "PAGAR :"
        '
        'lblsubtotal
        '
        Me.lblsubtotal.AutoSize = True
        Me.lblsubtotal.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblsubtotal.ForeColor = System.Drawing.Color.Fuchsia
        Me.lblsubtotal.Location = New System.Drawing.Point(313, 19)
        Me.lblsubtotal.Name = "lblsubtotal"
        Me.lblsubtotal.Size = New System.Drawing.Size(26, 29)
        Me.lblsubtotal.TabIndex = 90
        Me.lblsubtotal.Text = "0"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Red
        Me.Label14.Location = New System.Drawing.Point(7, 36)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(95, 22)
        Me.Label14.TabIndex = 60
        Me.Label14.Text = "DINERO :"
        '
        'lbligv
        '
        Me.lbligv.AutoSize = True
        Me.lbligv.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbligv.ForeColor = System.Drawing.Color.Fuchsia
        Me.lbligv.Location = New System.Drawing.Point(313, 66)
        Me.lbligv.Name = "lbligv"
        Me.lbligv.Size = New System.Drawing.Size(26, 29)
        Me.lbligv.TabIndex = 89
        Me.lbligv.Text = "0"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Arial", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Fuchsia
        Me.Label15.Location = New System.Drawing.Point(7, 71)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(104, 24)
        Me.Label15.TabIndex = 61
        Me.Label15.Text = "VUELTO :"
        '
        'lblvuelto
        '
        Me.lblvuelto.AutoSize = True
        Me.lblvuelto.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblvuelto.ForeColor = System.Drawing.Color.Fuchsia
        Me.lblvuelto.Location = New System.Drawing.Point(127, 67)
        Me.lblvuelto.Name = "lblvuelto"
        Me.lblvuelto.Size = New System.Drawing.Size(26, 29)
        Me.lblvuelto.TabIndex = 63
        Me.lblvuelto.Text = "0"
        '
        'lbltotal
        '
        Me.lbltotal.AutoSize = True
        Me.lbltotal.Font = New System.Drawing.Font("Arial", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltotal.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.lbltotal.Location = New System.Drawing.Point(109, 5)
        Me.lbltotal.Name = "lbltotal"
        Me.lbltotal.Size = New System.Drawing.Size(26, 29)
        Me.lbltotal.TabIndex = 31
        Me.lbltotal.Text = "0"
        '
        'txtdinero
        '
        Me.txtdinero.ForeColor = System.Drawing.Color.Red
        Me.txtdinero.Location = New System.Drawing.Point(114, 37)
        Me.txtdinero.Name = "txtdinero"
        Me.txtdinero.Size = New System.Drawing.Size(75, 25)
        Me.txtdinero.TabIndex = 64
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Button1)
        Me.GroupBox3.Controls.Add(Me.btnNv5)
        Me.GroupBox3.Controls.Add(Me.btnFactura)
        Me.GroupBox3.Controls.Add(Me.btnBoleta)
        Me.GroupBox3.Controls.Add(Me.btnFacturaI)
        Me.GroupBox3.Controls.Add(Me.btnboletainterna)
        Me.GroupBox3.Controls.Add(Me.btnEnviarSunat)
        Me.GroupBox3.Controls.Add(Me.btnmodificar)
        Me.GroupBox3.Location = New System.Drawing.Point(14, 427)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(405, 134)
        Me.GroupBox3.TabIndex = 84
        Me.GroupBox3.TabStop = False
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.Highlight
        Me.Button1.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Button1.Location = New System.Drawing.Point(-8, 44)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(127, 69)
        Me.Button1.TabIndex = 30
        Me.Button1.Text = "Quitar Articulos"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'btnNv5
        '
        Me.btnNv5.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnNv5.CausesValidation = False
        Me.btnNv5.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNv5.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnNv5.Location = New System.Drawing.Point(14, 64)
        Me.btnNv5.Name = "btnNv5"
        Me.btnNv5.Size = New System.Drawing.Size(91, 39)
        Me.btnNv5.TabIndex = 93
        Me.btnNv5.Text = "NOTA DE VENTA"
        Me.btnNv5.UseVisualStyleBackColor = False
        '
        'btnFactura
        '
        Me.btnFactura.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnFactura.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFactura.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnFactura.Location = New System.Drawing.Point(275, 45)
        Me.btnFactura.Name = "btnFactura"
        Me.btnFactura.Size = New System.Drawing.Size(130, 68)
        Me.btnFactura.TabIndex = 85
        Me.btnFactura.Text = "FACTURA"
        Me.btnFactura.UseVisualStyleBackColor = False
        '
        'btnBoleta
        '
        Me.btnBoleta.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnBoleta.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnBoleta.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnBoleta.Location = New System.Drawing.Point(139, 45)
        Me.btnBoleta.Name = "btnBoleta"
        Me.btnBoleta.Size = New System.Drawing.Size(130, 68)
        Me.btnBoleta.TabIndex = 84
        Me.btnBoleta.Text = "BOLETA"
        Me.btnBoleta.UseVisualStyleBackColor = False
        '
        'btnFacturaI
        '
        Me.btnFacturaI.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnFacturaI.CausesValidation = False
        Me.btnFacturaI.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnFacturaI.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnFacturaI.Location = New System.Drawing.Point(290, 55)
        Me.btnFacturaI.Name = "btnFacturaI"
        Me.btnFacturaI.Size = New System.Drawing.Size(86, 39)
        Me.btnFacturaI.TabIndex = 92
        Me.btnFacturaI.Text = "FACTURA I"
        Me.btnFacturaI.UseVisualStyleBackColor = False
        '
        'btnboletainterna
        '
        Me.btnboletainterna.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnboletainterna.CausesValidation = False
        Me.btnboletainterna.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnboletainterna.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnboletainterna.Location = New System.Drawing.Point(153, 47)
        Me.btnboletainterna.Name = "btnboletainterna"
        Me.btnboletainterna.Size = New System.Drawing.Size(91, 39)
        Me.btnboletainterna.TabIndex = 86
        Me.btnboletainterna.Text = "BOLETA I"
        Me.btnboletainterna.UseVisualStyleBackColor = False
        '
        'btnEnviarSunat
        '
        Me.btnEnviarSunat.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnEnviarSunat.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEnviarSunat.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnEnviarSunat.Location = New System.Drawing.Point(11, 52)
        Me.btnEnviarSunat.Name = "btnEnviarSunat"
        Me.btnEnviarSunat.Size = New System.Drawing.Size(66, 45)
        Me.btnEnviarSunat.TabIndex = 83
        Me.btnEnviarSunat.Text = "GENERAR COMPROBANTE"
        Me.btnEnviarSunat.UseVisualStyleBackColor = False
        '
        'btnmodificar
        '
        Me.btnmodificar.BackColor = System.Drawing.SystemColors.Highlight
        Me.btnmodificar.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnmodificar.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btnmodificar.Location = New System.Drawing.Point(14, 56)
        Me.btnmodificar.Name = "btnmodificar"
        Me.btnmodificar.Size = New System.Drawing.Size(73, 40)
        Me.btnmodificar.TabIndex = 57
        Me.btnmodificar.Text = "Modificar"
        Me.btnmodificar.UseVisualStyleBackColor = False
        '
        'cbosundm
        '
        Me.cbosundm.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cbosundm.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cbosundm.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbosundm.FormattingEnabled = True
        Me.cbosundm.Items.AddRange(New Object() {"1/2", "1/4", "1", "2", "3", "4", "5", "6"})
        Me.cbosundm.Location = New System.Drawing.Point(58, 88)
        Me.cbosundm.Name = "cbosundm"
        Me.cbosundm.Size = New System.Drawing.Size(33, 24)
        Me.cbosundm.TabIndex = 81
        '
        'btnpedido
        '
        Me.btnpedido.Location = New System.Drawing.Point(926, 562)
        Me.btnpedido.Name = "btnpedido"
        Me.btnpedido.Size = New System.Drawing.Size(10, 22)
        Me.btnpedido.TabIndex = 66
        Me.btnpedido.Text = "ImPedido"
        Me.btnpedido.UseVisualStyleBackColor = True
        '
        'chactivar
        '
        Me.chactivar.AutoSize = True
        Me.chactivar.Checked = True
        Me.chactivar.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chactivar.Location = New System.Drawing.Point(750, 91)
        Me.chactivar.Name = "chactivar"
        Me.chactivar.Size = New System.Drawing.Size(79, 22)
        Me.chactivar.TabIndex = 73
        Me.chactivar.Text = "Pmayor"
        Me.chactivar.UseVisualStyleBackColor = True
        '
        'txtprecio_unitariom
        '
        Me.txtprecio_unitariom.Font = New System.Drawing.Font("Arial Black", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtprecio_unitariom.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.txtprecio_unitariom.Location = New System.Drawing.Point(673, 84)
        Me.txtprecio_unitariom.Name = "txtprecio_unitariom"
        Me.txtprecio_unitariom.Size = New System.Drawing.Size(62, 34)
        Me.txtprecio_unitariom.TabIndex = 72
        '
        'txtcanttotal
        '
        Me.txtcanttotal.Location = New System.Drawing.Point(53, 93)
        Me.txtcanttotal.Name = "txtcanttotal"
        Me.txtcanttotal.Size = New System.Drawing.Size(13, 25)
        Me.txtcanttotal.TabIndex = 59
        '
        'txtcundm
        '
        Me.txtcundm.Location = New System.Drawing.Point(14, 91)
        Me.txtcundm.Name = "txtcundm"
        Me.txtcundm.Size = New System.Drawing.Size(10, 25)
        Me.txtcundm.TabIndex = 58
        '
        'btnbuscarlistaprecio
        '
        Me.btnbuscarlistaprecio.Location = New System.Drawing.Point(835, 82)
        Me.btnbuscarlistaprecio.Name = "btnbuscarlistaprecio"
        Me.btnbuscarlistaprecio.Size = New System.Drawing.Size(57, 36)
        Me.btnbuscarlistaprecio.TabIndex = 54
        Me.btnbuscarlistaprecio.Text = "..."
        Me.btnbuscarlistaprecio.UseVisualStyleBackColor = True
        '
        'cboundm
        '
        Me.cboundm.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboundm.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboundm.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboundm.FormattingEnabled = True
        Me.cboundm.Items.AddRange(New Object() {"UND", "DOC", "/2doc", "/4doc", "TR", "/2Tr", "PQ", "/2Pq", "CAJ", "/2Caj", "DSP", "BLS", "KG", "ZC", "/2Zc", "FRS", "TRO"})
        Me.cboundm.Location = New System.Drawing.Point(30, 93)
        Me.cboundm.Name = "cboundm"
        Me.cboundm.Size = New System.Drawing.Size(17, 24)
        Me.cboundm.TabIndex = 56
        '
        'inexistente
        '
        Me.inexistente.AutoSize = True
        Me.inexistente.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.inexistente.ForeColor = System.Drawing.Color.Red
        Me.inexistente.Location = New System.Drawing.Point(346, 233)
        Me.inexistente.Name = "inexistente"
        Me.inexistente.Size = New System.Drawing.Size(170, 22)
        Me.inexistente.TabIndex = 52
        Me.inexistente.Text = "Datos Inexistente"
        '
        'datalistado
        '
        Me.datalistado.AllowUserToAddRows = False
        Me.datalistado.AllowUserToDeleteRows = False
        Me.datalistado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.datalistado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.datalistado.Location = New System.Drawing.Point(6, 123)
        Me.datalistado.MultiSelect = False
        Me.datalistado.Name = "datalistado"
        Me.datalistado.ReadOnly = True
        Me.datalistado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.datalistado.Size = New System.Drawing.Size(888, 298)
        Me.datalistado.TabIndex = 51
        '
        'txtbuscar
        '
        Me.txtbuscar.Location = New System.Drawing.Point(97, 49)
        Me.txtbuscar.Name = "txtbuscar"
        Me.txtbuscar.Size = New System.Drawing.Size(110, 25)
        Me.txtbuscar.TabIndex = 41
        '
        'cbocampo
        '
        Me.cbocampo.FormattingEnabled = True
        Me.cbocampo.Items.AddRange(New Object() {"codigo", "codigom"})
        Me.cbocampo.Location = New System.Drawing.Point(9, 48)
        Me.cbocampo.Name = "cbocampo"
        Me.cbocampo.Size = New System.Drawing.Size(82, 26)
        Me.cbocampo.TabIndex = 40
        Me.cbocampo.Text = "codigo"
        '
        'cboproduct
        '
        Me.cboproduct.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboproduct.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboproduct.DataSource = Me.ProductoBindingSource1
        Me.cboproduct.DisplayMember = "nombre"
        Me.cboproduct.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboproduct.FormattingEnabled = True
        Me.cboproduct.Location = New System.Drawing.Point(213, 88)
        Me.cboproduct.Name = "cboproduct"
        Me.cboproduct.Size = New System.Drawing.Size(456, 26)
        Me.cboproduct.TabIndex = 0
        '
        'ProductoBindingSource1
        '
        Me.ProductoBindingSource1.DataMember = "producto"
        Me.ProductoBindingSource1.DataSource = Me.Mmdsnombre
        '
        'Mmdsnombre
        '
        Me.Mmdsnombre.DataSetName = "mmdsnombre"
        Me.Mmdsnombre.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label8.Location = New System.Drawing.Point(810, 17)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(63, 19)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "Stock :"
        '
        'txtpcompra
        '
        Me.txtpcompra.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtpcompra.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.txtpcompra.Location = New System.Drawing.Point(950, 95)
        Me.txtpcompra.Name = "txtpcompra"
        Me.txtpcompra.Size = New System.Drawing.Size(36, 21)
        Me.txtpcompra.TabIndex = 82
        Me.txtpcompra.Text = "1"
        Me.txtpcompra.Visible = False
        '
        'txtflag
        '
        Me.txtflag.Location = New System.Drawing.Point(950, 67)
        Me.txtflag.Name = "txtflag"
        Me.txtflag.Size = New System.Drawing.Size(37, 25)
        Me.txtflag.TabIndex = 55
        '
        'A
        '
        Me.A.BackgroundImage = CType(resources.GetObject("A.BackgroundImage"), System.Drawing.Image)
        Me.A.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.A.Location = New System.Drawing.Point(308, 5)
        Me.A.Name = "A"
        Me.A.Size = New System.Drawing.Size(39, 56)
        Me.A.TabIndex = 80
        Me.A.UseVisualStyleBackColor = True
        '
        'ProductoBindingSource
        '
        Me.ProductoBindingSource.DataMember = "producto"
        Me.ProductoBindingSource.DataSource = Me.MarketmayoristaDataSet
        '
        'MarketmayoristaDataSet
        '
        Me.MarketmayoristaDataSet.DataSetName = "marketmayoristaDataSet"
        Me.MarketmayoristaDataSet.EnforceConstraints = False
        Me.MarketmayoristaDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MarketDataSetdosfechas
        '
        Me.MarketDataSetdosfechas.DataSetName = "marketDataSetdosfechas"
        Me.MarketDataSetdosfechas.EnforceConstraints = False
        Me.MarketDataSetdosfechas.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'erroricono
        '
        Me.erroricono.ContainerControl = Me
        '
        'generar_comprobantemTableAdapter
        '
        Me.generar_comprobantemTableAdapter.ClearBeforeFill = True
        '
        'recibos
        '
        Me.recibos.DataSetName = "recibos"
        Me.recibos.EnforceConstraints = False
        Me.recibos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MarketDataSetticket1
        '
        Me.MarketDataSetticket1.DataSetName = "marketDataSetticket"
        Me.MarketDataSetticket1.EnforceConstraints = False
        Me.MarketDataSetticket1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BindingSource1
        '
        Me.BindingSource1.DataMember = "generar_comprobante"
        Me.BindingSource1.DataSource = Me.MarketDataSetticket1
        '
        'Generar_comprobanteTableAdapter1
        '
        Me.Generar_comprobanteTableAdapter1.ClearBeforeFill = True
        '
        'generar_comprobanteTableAdapter
        '
        Me.generar_comprobanteTableAdapter.ClearBeforeFill = True
        '
        'generar_comprobanteBindingSource
        '
        Me.generar_comprobanteBindingSource.DataMember = "generar_comprobantem"
        Me.generar_comprobanteBindingSource.DataSource = Me.recibos
        '
        'marketDataSetticket
        '
        Me.marketDataSetticket.DataSetName = "marketDataSetticket"
        Me.marketDataSetticket.EnforceConstraints = False
        Me.marketDataSetticket.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ProductoTableAdapter
        '
        Me.ProductoTableAdapter.ClearBeforeFill = True
        '
        'Generar_comprobantemTableAdapter1
        '
        Me.Generar_comprobantemTableAdapter1.ClearBeforeFill = True
        '
        'ProductoTableAdapter1
        '
        Me.ProductoTableAdapter1.ClearBeforeFill = True
        '
        'ComboBox1
        '
        Me.ComboBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Items.AddRange(New Object() {"EFECTIVO", "TARJETA"})
        Me.ComboBox1.Location = New System.Drawing.Point(18, 5)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(243, 24)
        Me.ComboBox1.TabIndex = 89
        Me.ComboBox1.Text = "REPARTIDORn"
        '
        'ComboBox2
        '
        Me.ComboBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Items.AddRange(New Object() {"Victor Aguirre", "Carlos Human"})
        Me.ComboBox2.Location = New System.Drawing.Point(18, 33)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(243, 24)
        Me.ComboBox2.TabIndex = 90
        Me.ComboBox2.Text = "TIENDAS"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.MenuHighlight
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(267, 26)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(35, 27)
        Me.Button2.TabIndex = 91
        Me.Button2.Text = "..."
        Me.Button2.UseVisualStyleBackColor = False
        '
        'frmdetalle_venta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(1311, 611)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.A)
        Me.Controls.Add(Me.btnbuscar_cliente)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.ForeColor = System.Drawing.Color.Cornsilk
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Name = "frmdetalle_venta"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "VENTAS-REALIZAR VENTAS"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.generar_comprobantemBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.marketdscomprobante, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.txtstock, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.txtcantidad, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductoBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Mmdsnombre, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProductoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MarketmayoristaDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MarketDataSetdosfechas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.erroricono, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.recibos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MarketDataSetticket1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.generar_comprobanteBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.marketDataSetticket, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cbtipo_documento As System.Windows.Forms.ComboBox
    Friend WithEvents txtnombre_cliente As System.Windows.Forms.TextBox
    Friend WithEvents btnguardar As System.Windows.Forms.Button
    Friend WithEvents serie_docu As System.Windows.Forms.Label
    Friend WithEvents txtcolaborador As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtidcliente As System.Windows.Forms.TextBox
    Friend WithEvents txtidventa As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents erroricono As System.Windows.Forms.ErrorProvider
    Friend WithEvents txtstock As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtcantidad As System.Windows.Forms.NumericUpDown
    Friend WithEvents txtprecio_unitario As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtnombre_producto As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtidproducto As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnsalirr As System.Windows.Forms.Button
    Friend WithEvents lbltotal As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboproduct As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtserie_documento As System.Windows.Forms.TextBox
    Friend WithEvents txtusuario As System.Windows.Forms.TextBox
    Friend WithEvents txtidusuario As System.Windows.Forms.TextBox
    Friend WithEvents cbocampo As System.Windows.Forms.ComboBox
    Friend WithEvents txtbuscar As System.Windows.Forms.TextBox
    Friend WithEvents btnbuscar_cliente As System.Windows.Forms.Button
    Friend WithEvents inexistente As System.Windows.Forms.Label
    Friend WithEvents datalistado As System.Windows.Forms.DataGridView
    Friend WithEvents btnbuscarlistaprecio As Button
    Friend WithEvents txtflag As TextBox
    Friend WithEvents cboundm As ComboBox
    Friend WithEvents btnmodificar As Button
    Friend WithEvents txtcundm As TextBox
    Friend WithEvents txtcanttotal As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents txtdinero As TextBox
    Friend WithEvents lblvuelto As Label
    Friend WithEvents btnpedido As Button
    Friend WithEvents txtprecio_unitariom As TextBox
    Friend WithEvents chactivar As CheckBox
    Friend WithEvents Label8 As Label
    Friend WithEvents generar_comprobantemTableAdapter As marketdscomprobanteTableAdapters.generar_comprobantemTableAdapter
    Friend WithEvents A As Button
    Friend WithEvents cbosundm As ComboBox
    Friend WithEvents MarketDataSetdosfechas As marketDataSetdosfechas
    Friend WithEvents recibos As recibos
    Friend WithEvents txtpcompra As TextBox
    Friend WithEvents MarketDataSetticket1 As marketDataSetticket
    Friend WithEvents BindingSource1 As BindingSource
    Friend WithEvents Generar_comprobanteTableAdapter1 As marketDataSetticketTableAdapters.generar_comprobanteTableAdapter
    Friend WithEvents marketdscomprobante As marketdscomprobante
    Friend WithEvents generar_comprobantemBindingSource As BindingSource
    Friend WithEvents generar_comprobanteTableAdapter As marketDataSetticketTableAdapters.generar_comprobanteTableAdapter
    Friend WithEvents generar_comprobanteBindingSource As BindingSource
    Friend WithEvents marketDataSetticket As marketDataSetticket
    Friend WithEvents MarketmayoristaDataSet As marketmayoristaDataSet
    Friend WithEvents ProductoBindingSource As BindingSource
    Friend WithEvents ProductoTableAdapter As marketmayoristaDataSetTableAdapters.productoTableAdapter
    Private WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents Generar_comprobantemTableAdapter1 As recibosTableAdapters.generar_comprobantemTableAdapter
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Mmdsnombre As mmdsnombre
    Friend WithEvents ProductoBindingSource1 As BindingSource
    Friend WithEvents ProductoTableAdapter1 As mmdsnombreTableAdapters.productoTableAdapter
    Friend WithEvents btnEnviarLuegoSunat As Button
    Friend WithEvents txtfecha As TextBox
    Friend WithEvents btneditarVenta As Button
    Friend WithEvents chkservi As CheckBox
    Friend WithEvents Button2 As Button
    Friend WithEvents ComboBox2 As ComboBox
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents btnenviar_fact As Button
    Friend WithEvents btnFactura As Button
    Friend WithEvents btnBoleta As Button
    Friend WithEvents txtdireccion As TextBox
    Friend WithEvents txtubigueo As TextBox
    Friend WithEvents txt_ruc As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Tipo_document As Label
    Friend WithEvents txttipo_documento As TextBox
    Friend WithEvents lbligv As Label
    Friend WithEvents lblsubtotal As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents txtCpais As TextBox
    Friend WithEvents txtcorrelativo As TextBox
    Friend WithEvents txtcodsunat As TextBox
    Friend WithEvents txtnrocaja As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents btnEnviarSunat As Button
    Friend WithEvents btnFacturaI As Button
    Friend WithEvents btnboletainterna As Button
    Friend WithEvents btnA5 As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents txtdescrip_comprobante As TextBox
    Friend WithEvents btnNv5 As Button
    Friend WithEvents Label17 As Label
    Friend WithEvents lblletras As Label
    Friend WithEvents btnna5 As Button
End Class
