﻿Imports System.Configuration
Imports System.Data.SqlClient
Public Class frmcCantidadProductosporUsuario

    Dim conexion As New SqlConnection(ConfigurationManager.ConnectionStrings("BRAVOSPORT.My.MySettings.marketConnectionString").ConnectionString)
    Dim cadena As String
    Dim datos As New DataSet
    Dim base As New SqlDataAdapter("select * from usuario", conexion)
    Dim variable As SqlDataReader
    Dim consulta3 As New SqlCommand
    Private dt As New DataTable

    Private Sub frmconsultasventas_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'mostrar()

        Dim mycount As Integer

        mycount = datalistado.Rows.Count()
        txtcount.Text = mycount

        cbologin.Text = "TODOS"
        consulta3.CommandType = CommandType.Text
        consulta3.CommandText = ("select login from usuario")
        consulta3.Connection = (conexion)
        conexion.Open()
        variable = consulta3.ExecuteReader

        While variable.Read = True
            cbologin.Items.Add(variable.Item(0))
        End While

        conexion.Close()




        'Try
        '    Dim query As IEnumerable(Of Object) =
        '        From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
        '        Where (
        '            (row.Cells("Cantidades").Value IsNot Nothing) AndAlso
        '            (row.Cells("Cantidades").Value IsNot DBNull.Value)) Select row.Cells("Cantidades").Value()

        '    Dim resultado As Decimal =
        '        query.Sum(Function(row) Convert.ToDecimal(row))

        '    txtscantidad.Text = resultado


        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)

        'End Try




        'Try
        '    Dim query As IEnumerable(Of Object) =
        '        From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
        '        Where (
        '            (row.Cells("SubTotalCompra").Value IsNot Nothing) AndAlso
        '            (row.Cells("SubTotalCompra").Value IsNot DBNull.Value)) Select row.Cells("SubTotalCompra").Value()

        '    Dim resultado As Decimal =
        '        query.Sum(Function(row) Convert.ToDecimal(row))
        '    txtstcompras.Text = String.Format(resultado)


        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)

        'End Try



        'Try
        '    Dim query As IEnumerable(Of Object) =
        '        From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
        '        Where (
        '            (row.Cells("SubTotalVenta").Value IsNot Nothing) AndAlso
        '            (row.Cells("SubTotalVenta").Value IsNot DBNull.Value)) Select row.Cells("SubTotalVenta").Value()

        '    Dim resultado As Decimal =
        '        query.Sum(Function(row) Convert.ToDouble(row))
        '    txtstventas.Text = String.Format(resultado)


        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)

        'End Try



        'txtutilidadpusuario.Text = Convert.ToDouble(txtstventas.Text) - Convert.ToDouble(txtstcompras.Text)



    End Sub

    Private Sub mostrar()
        Try
            Dim func As New fcventas
            dt = func.mostrar

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

    End Sub

    Private Sub mostrar_cantprodvendporusuario()
        Try
            Dim func As New fcantprodvendporusuario
            dt = func.mostrar_cantprodvendporusuario()



            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try



    End Sub


    Private Sub mostrar_ventasporfechas()
        Try
            Dim func As New fmostrarventasporfechas
            dt = func.mostrar_ventasporfechas()



            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try



    End Sub

    Private Sub mostrar_detalle_de_servicios_brindados()
        Try
            Dim func As New fmostrarventasporfechas
            dt = func.mostrar_detalle_de_servicios_brindados



            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try



    End Sub


    Private Sub btnconsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnconsultar.Click
        If cbologin.Text = "TODOS" Then
            MessageBox.Show("TODOS")
            mostrar_ventasporfechas()

            Dim mycount As Integer

            mycount = datalistado.Rows.Count()
            txtcount.Text = mycount


            Try
                Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("SubTotalCompra").Value IsNot Nothing) AndAlso
                    (row.Cells("SubTotalCompra").Value IsNot DBNull.Value)) Select row.Cells("SubTotalCompra").Value()

                Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDecimal(row))
                txtstcompras.Text = String.Format(resultado)


            Catch ex As Exception
                MessageBox.Show(ex.Message)

            End Try



            Try
                Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("SubTotalVenta").Value IsNot Nothing) AndAlso
                    (row.Cells("SubTotalVenta").Value IsNot DBNull.Value)) Select row.Cells("SubTotalVenta").Value()

                Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDouble(row))
                txtstventas.Text = String.Format(resultado)


            Catch ex As Exception
                MessageBox.Show(ex.Message)

            End Try



            txtutilidadpusuario.Text = Convert.ToDouble(txtstventas.Text) - Convert.ToDouble(txtstcompras.Text)


        Else
            mostrar_cantprodvendporusuario()


            Dim mycount As Integer

            mycount = datalistado.Rows.Count()
            txtcount.Text = mycount





            Try
                Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("SubTotalCompra").Value IsNot Nothing) AndAlso
                    (row.Cells("SubTotalCompra").Value IsNot DBNull.Value)) Select row.Cells("SubTotalCompra").Value()

                Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDecimal(row))
                txtstcompras.Text = String.Format(resultado)


            Catch ex As Exception
                MessageBox.Show(ex.Message)

            End Try



            Try
                Dim query As IEnumerable(Of Object) =
                From row As DataGridViewRow In datalistado.Rows.Cast(Of DataGridViewRow)()
                Where (
                    (row.Cells("SubTotalVenta").Value IsNot Nothing) AndAlso
                    (row.Cells("SubTotalVenta").Value IsNot DBNull.Value)) Select row.Cells("SubTotalVenta").Value()

                Dim resultado As Decimal =
                query.Sum(Function(row) Convert.ToDouble(row))
                txtstventas.Text = String.Format(resultado)


            Catch ex As Exception
                MessageBox.Show(ex.Message)

            End Try



            txtutilidadpusuario.Text = Convert.ToDouble(txtstventas.Text) - Convert.ToDouble(txtstcompras.Text)

        End If

        If cbologin.Text = "SERVICIOS" Then
            mostrar_detalle_de_servicios_brindados()


        End If


    End Sub


    Private Sub btnimprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnimprimir.Click
        Reporte_Detalle_Productos_Vendidos_TipoPago.txtfechai.Text = Me.txtfechai.Text
        Reporte_Detalle_Productos_Vendidos_TipoPago.txtfechafi.Text = Me.txtfechafi.Text
        Reporte_Detalle_Productos_Vendidos_TipoPago.ShowDialog()
        'Reporte_Detalle_de_los_Productos_vendidos.txtfechai.Text = Me.txtfechai.Text
        'Reporte_Detalle_de_los_Productos_vendidos.txtfechafi.Text = Me.txtfechafi.Text
        'Reporte_Detalle_de_los_Productos_vendidos.ShowDialog()

        'ReporteVDosFechas.txtfechai.Text = Me.txtfechai.Text
        'ReporteVDosFechas.txtfechafi.Text = Me.txtfechafi.Text


        'ReporteVDosFechas.ShowDialog()
    End Sub

    Private Sub txtcount_TextChanged(sender As Object, e As EventArgs) Handles txtcount.TextChanged

    End Sub

    Private Sub cbologin_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbologin.SelectedIndexChanged

    End Sub

    Private Sub btnReporteServicios_Click(sender As Object, e As EventArgs) Handles btnReporteServicios.Click



        Reporte_Detalle_ServiciosDados_TipoPago.txtfechai.Text = Me.txtfechai.Text
        Reporte_Detalle_ServiciosDados_TipoPago.txtfechafi.Text = Me.txtfechafi.Text
        Reporte_Detalle_ServiciosDados_TipoPago.ShowDialog()
        'Reporte_Detalle_de_los_Servicios.txtfechai.Text = Me.txtfechai.Text
        'Reporte_Detalle_de_los_Servicios.txtfechafi.Text = Me.txtfechafi.Text
        'Reporte_Detalle_de_los_Servicios.ShowDialog()




    End Sub
End Class