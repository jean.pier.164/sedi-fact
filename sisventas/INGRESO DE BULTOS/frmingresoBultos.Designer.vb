﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmingresoBultos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dtfecha_traslado = New System.Windows.Forms.DateTimePicker()
        Me.dtfecha_ingreso = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.cbeliminar = New System.Windows.Forms.CheckBox()
        Me.btneliminar = New System.Windows.Forms.Button()
        Me.inexistente = New System.Windows.Forms.Label()
        Me.txtbuscar = New System.Windows.Forms.TextBox()
        Me.cbocampo = New System.Windows.Forms.ComboBox()
        Me.datalistado = New System.Windows.Forms.DataGridView()
        Me.Eliminar = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.btnIngresa = New System.Windows.Forms.Button()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtbultos_recibidos = New System.Windows.Forms.TextBox()
        Me.TextBox7 = New System.Windows.Forms.TextBox()
        Me.txtguia_trasnportista = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtcorrelativo = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtserie = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtprecio_venta = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtfecha_traslado = New System.Windows.Forms.TextBox()
        Me.txtfecha_ingreso = New System.Windows.Forms.TextBox()
        Me._SORDONEZ_FV02DataSet1 = New BRAVOSPORT._SORDONEZ_FV02DataSet()
        Me.DSCIERRECAJA = New BRAVOSPORT.DSCIERRECAJA()
        Me.mostrar_apertura_usuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.mostrar_apertura_usuarioTableAdapter = New BRAVOSPORT.DSCIERRECAJATableAdapters.mostrar_apertura_usuarioTableAdapter()
        Me.mostrar_egresos_usuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.mostrar_egresos_usuarioTableAdapter = New BRAVOSPORT.DSCIERRECAJATableAdapters.mostrar_egresos_usuarioTableAdapter()
        Me.mostrar_ingreso_usuarioBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.mostrar_ingreso_usuarioTableAdapter = New BRAVOSPORT.DSCIERRECAJATableAdapters.mostrar_ingreso_usuarioTableAdapter()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtgrupo_proveedor = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._SORDONEZ_FV02DataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DSCIERRECAJA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mostrar_apertura_usuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mostrar_egresos_usuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mostrar_ingreso_usuarioBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtgrupo_proveedor)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.dtfecha_traslado)
        Me.GroupBox1.Controls.Add(Me.dtfecha_ingreso)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.btnIngresa)
        Me.GroupBox1.Controls.Add(Me.Label18)
        Me.GroupBox1.Controls.Add(Me.Label17)
        Me.GroupBox1.Controls.Add(Me.txtbultos_recibidos)
        Me.GroupBox1.Controls.Add(Me.TextBox7)
        Me.GroupBox1.Controls.Add(Me.txtguia_trasnportista)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.txtcorrelativo)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.txtserie)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtprecio_venta)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtfecha_traslado)
        Me.GroupBox1.Controls.Add(Me.txtfecha_ingreso)
        Me.GroupBox1.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.GroupBox1.Location = New System.Drawing.Point(12, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1115, 606)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "INGRESO DE BULTOS"
        '
        'dtfecha_traslado
        '
        Me.dtfecha_traslado.Location = New System.Drawing.Point(314, 35)
        Me.dtfecha_traslado.Name = "dtfecha_traslado"
        Me.dtfecha_traslado.Size = New System.Drawing.Size(313, 25)
        Me.dtfecha_traslado.TabIndex = 62
        '
        'dtfecha_ingreso
        '
        Me.dtfecha_ingreso.Location = New System.Drawing.Point(737, 24)
        Me.dtfecha_ingreso.Name = "dtfecha_ingreso"
        Me.dtfecha_ingreso.Size = New System.Drawing.Size(352, 25)
        Me.dtfecha_ingreso.TabIndex = 61
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Button2)
        Me.GroupBox2.Controls.Add(Me.cbeliminar)
        Me.GroupBox2.Controls.Add(Me.btneliminar)
        Me.GroupBox2.Controls.Add(Me.inexistente)
        Me.GroupBox2.Controls.Add(Me.txtbuscar)
        Me.GroupBox2.Controls.Add(Me.cbocampo)
        Me.GroupBox2.Controls.Add(Me.datalistado)
        Me.GroupBox2.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.Highlight
        Me.GroupBox2.Location = New System.Drawing.Point(0, 210)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1115, 346)
        Me.GroupBox2.TabIndex = 60
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "BULTOS REGISTRADO"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.MenuHighlight
        Me.Button2.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Button2.Location = New System.Drawing.Point(770, 564)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(135, 51)
        Me.Button2.TabIndex = 36
        Me.Button2.Text = "Ver Suma Total Almacen"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'cbeliminar
        '
        Me.cbeliminar.AutoSize = True
        Me.cbeliminar.Location = New System.Drawing.Point(15, 51)
        Me.cbeliminar.Name = "cbeliminar"
        Me.cbeliminar.Size = New System.Drawing.Size(80, 20)
        Me.cbeliminar.TabIndex = 16
        Me.cbeliminar.Text = "Eliminar"
        Me.cbeliminar.UseVisualStyleBackColor = True
        '
        'btneliminar
        '
        Me.btneliminar.BackColor = System.Drawing.SystemColors.MenuHighlight
        Me.btneliminar.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btneliminar.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.btneliminar.Location = New System.Drawing.Point(16, 564)
        Me.btneliminar.Name = "btneliminar"
        Me.btneliminar.Size = New System.Drawing.Size(135, 51)
        Me.btneliminar.TabIndex = 15
        Me.btneliminar.Text = "Eliminar"
        Me.btneliminar.UseVisualStyleBackColor = False
        '
        'inexistente
        '
        Me.inexistente.AutoSize = True
        Me.inexistente.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.inexistente.ForeColor = System.Drawing.Color.Red
        Me.inexistente.Location = New System.Drawing.Point(453, 203)
        Me.inexistente.Name = "inexistente"
        Me.inexistente.Size = New System.Drawing.Size(141, 19)
        Me.inexistente.TabIndex = 3
        Me.inexistente.Text = "Datos Inexistente"
        '
        'txtbuscar
        '
        Me.txtbuscar.Location = New System.Drawing.Point(242, 47)
        Me.txtbuscar.Name = "txtbuscar"
        Me.txtbuscar.Size = New System.Drawing.Size(265, 22)
        Me.txtbuscar.TabIndex = 2
        '
        'cbocampo
        '
        Me.cbocampo.FormattingEnabled = True
        Me.cbocampo.Items.AddRange(New Object() {"codigo", "nombre"})
        Me.cbocampo.Location = New System.Drawing.Point(115, 47)
        Me.cbocampo.Name = "cbocampo"
        Me.cbocampo.Size = New System.Drawing.Size(121, 24)
        Me.cbocampo.TabIndex = 1
        '
        'datalistado
        '
        Me.datalistado.AllowUserToAddRows = False
        Me.datalistado.AllowUserToDeleteRows = False
        Me.datalistado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.datalistado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.datalistado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Eliminar})
        Me.datalistado.Location = New System.Drawing.Point(16, 86)
        Me.datalistado.Name = "datalistado"
        Me.datalistado.ReadOnly = True
        Me.datalistado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.datalistado.Size = New System.Drawing.Size(1055, 472)
        Me.datalistado.TabIndex = 0
        '
        'Eliminar
        '
        Me.Eliminar.HeaderText = "Eliminar"
        Me.Eliminar.Name = "Eliminar"
        Me.Eliminar.ReadOnly = True
        Me.Eliminar.Width = 67
        '
        'btnIngresa
        '
        Me.btnIngresa.BackColor = System.Drawing.SystemColors.MenuHighlight
        Me.btnIngresa.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnIngresa.ForeColor = System.Drawing.SystemColors.HighlightText
        Me.btnIngresa.Location = New System.Drawing.Point(737, 161)
        Me.btnIngresa.Name = "btnIngresa"
        Me.btnIngresa.Size = New System.Drawing.Size(110, 43)
        Me.btnIngresa.TabIndex = 59
        Me.btnIngresa.Text = "INGRESAR"
        Me.btnIngresa.UseVisualStyleBackColor = False
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label18.Location = New System.Drawing.Point(238, 120)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(113, 23)
        Me.Label18.TabIndex = 58
        Me.Label18.Text = "BULTOS REC :"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label17.Location = New System.Drawing.Point(453, 120)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(127, 23)
        Me.Label17.TabIndex = 57
        Me.Label17.Text = "BULTOS PEND :"
        '
        'txtbultos_recibidos
        '
        Me.txtbultos_recibidos.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbultos_recibidos.Location = New System.Drawing.Point(357, 117)
        Me.txtbultos_recibidos.MaxLength = 8
        Me.txtbultos_recibidos.Name = "txtbultos_recibidos"
        Me.txtbultos_recibidos.Size = New System.Drawing.Size(76, 26)
        Me.txtbultos_recibidos.TabIndex = 55
        '
        'TextBox7
        '
        Me.TextBox7.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox7.Location = New System.Drawing.Point(586, 117)
        Me.TextBox7.MaxLength = 8
        Me.TextBox7.Name = "TextBox7"
        Me.TextBox7.Size = New System.Drawing.Size(79, 26)
        Me.TextBox7.TabIndex = 54
        '
        'txtguia_trasnportista
        '
        Me.txtguia_trasnportista.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtguia_trasnportista.Location = New System.Drawing.Point(124, 79)
        Me.txtguia_trasnportista.Name = "txtguia_trasnportista"
        Me.txtguia_trasnportista.Size = New System.Drawing.Size(274, 26)
        Me.txtguia_trasnportista.TabIndex = 52
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label16.Location = New System.Drawing.Point(733, 120)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(43, 23)
        Me.Label16.TabIndex = 51
        Me.Label16.Text = "DIB:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label15.Location = New System.Drawing.Point(829, 99)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(56, 23)
        Me.Label15.TabIndex = 50
        Me.Label15.Text = "Serie:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label14.Location = New System.Drawing.Point(955, 99)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(107, 23)
        Me.Label14.TabIndex = 49
        Me.Label14.Text = "Preimpreso:"
        '
        'txtcorrelativo
        '
        Me.txtcorrelativo.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtcorrelativo.Location = New System.Drawing.Point(959, 131)
        Me.txtcorrelativo.Name = "txtcorrelativo"
        Me.txtcorrelativo.Size = New System.Drawing.Size(112, 26)
        Me.txtcorrelativo.TabIndex = 48
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label11.Location = New System.Drawing.Point(715, 67)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(95, 23)
        Me.Label11.TabIndex = 47
        Me.Label11.Text = "F. Ingreso :"
        '
        'txtserie
        '
        Me.txtserie.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtserie.Location = New System.Drawing.Point(813, 131)
        Me.txtserie.Name = "txtserie"
        Me.txtserie.Size = New System.Drawing.Size(112, 26)
        Me.txtserie.TabIndex = 37
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label7.Location = New System.Drawing.Point(8, 76)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(87, 23)
        Me.Label7.TabIndex = 36
        Me.Label7.Text = "G. Trsnsp:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label5.Location = New System.Drawing.Point(6, 120)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(108, 23)
        Me.Label5.TabIndex = 19
        Me.Label5.Text = "BULTOS SGT:"
        '
        'txtprecio_venta
        '
        Me.txtprecio_venta.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtprecio_venta.Location = New System.Drawing.Point(128, 120)
        Me.txtprecio_venta.MaxLength = 8
        Me.txtprecio_venta.Name = "txtprecio_venta"
        Me.txtprecio_venta.Size = New System.Drawing.Size(85, 26)
        Me.txtprecio_venta.TabIndex = 16
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label2.Location = New System.Drawing.Point(6, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(112, 23)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "F. Translado :"
        '
        'txtfecha_traslado
        '
        Me.txtfecha_traslado.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfecha_traslado.Location = New System.Drawing.Point(124, 35)
        Me.txtfecha_traslado.Name = "txtfecha_traslado"
        Me.txtfecha_traslado.Size = New System.Drawing.Size(192, 26)
        Me.txtfecha_traslado.TabIndex = 2
        '
        'txtfecha_ingreso
        '
        Me.txtfecha_ingreso.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfecha_ingreso.Location = New System.Drawing.Point(844, 64)
        Me.txtfecha_ingreso.Name = "txtfecha_ingreso"
        Me.txtfecha_ingreso.Size = New System.Drawing.Size(255, 26)
        Me.txtfecha_ingreso.TabIndex = 63
        '
        '_SORDONEZ_FV02DataSet1
        '
        Me._SORDONEZ_FV02DataSet1.DataSetName = "_SORDONEZ_FV02DataSet"
        Me._SORDONEZ_FV02DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DSCIERRECAJA
        '
        Me.DSCIERRECAJA.DataSetName = "DSCIERRECAJA"
        Me.DSCIERRECAJA.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'mostrar_apertura_usuarioBindingSource
        '
        Me.mostrar_apertura_usuarioBindingSource.DataMember = "mostrar_apertura_usuario"
        Me.mostrar_apertura_usuarioBindingSource.DataSource = Me.DSCIERRECAJA
        '
        'mostrar_apertura_usuarioTableAdapter
        '
        Me.mostrar_apertura_usuarioTableAdapter.ClearBeforeFill = True
        '
        'mostrar_egresos_usuarioBindingSource
        '
        Me.mostrar_egresos_usuarioBindingSource.DataMember = "mostrar_egresos_usuario"
        Me.mostrar_egresos_usuarioBindingSource.DataSource = Me.DSCIERRECAJA
        '
        'mostrar_egresos_usuarioTableAdapter
        '
        Me.mostrar_egresos_usuarioTableAdapter.ClearBeforeFill = True
        '
        'mostrar_ingreso_usuarioBindingSource
        '
        Me.mostrar_ingreso_usuarioBindingSource.DataMember = "mostrar_ingreso_usuario"
        Me.mostrar_ingreso_usuarioBindingSource.DataSource = Me.DSCIERRECAJA
        '
        'mostrar_ingreso_usuarioTableAdapter
        '
        Me.mostrar_ingreso_usuarioTableAdapter.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.Label1.Location = New System.Drawing.Point(12, 181)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(151, 23)
        Me.Label1.TabIndex = 64
        Me.Label1.Text = "Grupo Proveedor:"
        Me.Label1.UseWaitCursor = True
        '
        'txtgrupo_proveedor
        '
        Me.txtgrupo_proveedor.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtgrupo_proveedor.Location = New System.Drawing.Point(169, 181)
        Me.txtgrupo_proveedor.MaxLength = 8
        Me.txtgrupo_proveedor.Name = "txtgrupo_proveedor"
        Me.txtgrupo_proveedor.Size = New System.Drawing.Size(147, 26)
        Me.txtgrupo_proveedor.TabIndex = 65
        '
        'FrmingresoBultos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1164, 603)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "FrmingresoBultos"
        Me.Text = "IngresoBultos"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.datalistado, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._SORDONEZ_FV02DataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DSCIERRECAJA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mostrar_apertura_usuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mostrar_egresos_usuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mostrar_ingreso_usuarioBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label16 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents txtcorrelativo As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtserie As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtprecio_venta As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtfecha_traslado As TextBox
    Friend WithEvents btnIngresa As Button
    Friend WithEvents Label18 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents txtbultos_recibidos As TextBox
    Friend WithEvents TextBox7 As TextBox
    Friend WithEvents txtguia_trasnportista As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Button2 As Button
    Friend WithEvents cbeliminar As CheckBox
    Friend WithEvents btneliminar As Button
    Friend WithEvents inexistente As Label
    Friend WithEvents txtbuscar As TextBox
    Friend WithEvents cbocampo As ComboBox
    Friend WithEvents datalistado As DataGridView
    Friend WithEvents Eliminar As DataGridViewCheckBoxColumn
    Friend WithEvents dtfecha_ingreso As DateTimePicker
    Friend WithEvents dtfecha_traslado As DateTimePicker
    Friend WithEvents _SORDONEZ_FV02DataSet1 As _SORDONEZ_FV02DataSet
    Friend WithEvents txtfecha_ingreso As TextBox
    Friend WithEvents mostrar_apertura_usuarioBindingSource As BindingSource
    Friend WithEvents DSCIERRECAJA As DSCIERRECAJA
    Friend WithEvents mostrar_egresos_usuarioBindingSource As BindingSource
    Friend WithEvents mostrar_ingreso_usuarioBindingSource As BindingSource
    Friend WithEvents mostrar_apertura_usuarioTableAdapter As DSCIERRECAJATableAdapters.mostrar_apertura_usuarioTableAdapter
    Friend WithEvents mostrar_egresos_usuarioTableAdapter As DSCIERRECAJATableAdapters.mostrar_egresos_usuarioTableAdapter
    Friend WithEvents mostrar_ingreso_usuarioTableAdapter As DSCIERRECAJATableAdapters.mostrar_ingreso_usuarioTableAdapter
    Friend WithEvents txtgrupo_proveedor As TextBox
    Friend WithEvents Label1 As Label
End Class
