﻿Imports System.Data.SqlClient
Public Class Fingresobultos
    Inherits conexion

    Dim cmd As New SqlCommand

    Public Function mostrar() As DataTable
        Try
            conectado()
            cmd = New SqlCommand("mostrar_bultos")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            If cmd.ExecuteNonQuery Then
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Else

                Return Nothing
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Return Nothing
        Finally
            desconectado()

            End Try

        End Function




        Public Function insertar(ByVal dts As Vingresobultos) As Boolean
            Try
                conectado()
                cmd = New SqlCommand("insertar_bultos")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = cnn

                cmd.Parameters.AddWithValue("@fecha_traslado", dts.gfecha_traslado)
                cmd.Parameters.AddWithValue("@fecha_ingreso", dts.gfecha_ingreso)
                cmd.Parameters.AddWithValue("@guia_transportista", dts.gguia_transportista)
                cmd.Parameters.AddWithValue("@bultos_recibidos", dts.gbultos_recibidos)
            cmd.Parameters.AddWithValue("@grupo_proveedor", dts.ggrupo_proveedor)
            cmd.Parameters.AddWithValue("@serie", dts.gserie)
                cmd.Parameters.AddWithValue("@correlativo", dts.gcorrelativo)




                If cmd.ExecuteNonQuery Then
                    Return True

                Else
                    Return False

                End If

            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            Finally
                desconectado()

            End Try

        End Function


        Public Function editar(ByVal dts As Vingresobultos) As Boolean
            Try
                conectado()
                cmd = New SqlCommand("editar_bultos")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = cnn

                cmd.Parameters.AddWithValue("@idbulto", dts.gidbulto)
                cmd.Parameters.AddWithValue("@fecha_traslado", dts.gfecha_traslado)
                cmd.Parameters.AddWithValue("@fecha_ingreso", dts.gfecha_ingreso)
                cmd.Parameters.AddWithValue("@guia_transportista", dts.gguia_transportista)
                cmd.Parameters.AddWithValue("@bultos_recibidos", dts.gbultos_recibidos)
            cmd.Parameters.AddWithValue("@grupo_proveedor", dts.ggrupo_proveedor)
            cmd.Parameters.AddWithValue("@serie", dts.gserie)
                cmd.Parameters.AddWithValue("@correlativo", dts.gcorrelativo)


                If cmd.ExecuteNonQuery Then
                    Return True

                Else
                    Return False

                End If

            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            Finally
                desconectado()

            End Try

        End Function

        Public Function eliminar(ByVal dts As Vingresobultos) As Boolean
            Try
                conectado()
                cmd = New SqlCommand("eliminar_bultos")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = cnn

                cmd.Parameters.Add("@idbulto", SqlDbType.NVarChar, 50).Value = dts.gidbulto
                cmd.Parameters.Add("@fecha_traslado", SqlDbType.NVarChar, 50).Value = dts.gfecha_traslado
                cmd.Parameters.Add("@fecha_ingreso", SqlDbType.NVarChar, 50).Value = dts.gfecha_ingreso
                cmd.Parameters.Add("@bultos_recibidos", SqlDbType.NVarChar, 50).Value = dts.gbultos_recibidos
                cmd.Parameters.Add("@grupo_proveedor", SqlDbType.NVarChar, 50).Value = dts.ggrupo_proveedor
                cmd.Parameters.Add("@serie", SqlDbType.NVarChar, 50).Value = dts.gserie
                cmd.Parameters.Add("@coorelativo", SqlDbType.NVarChar, 50).Value = dts.gcorrelativo


                If cmd.ExecuteNonQuery Then
                    Return True
                Else
                    Return False

                End If

            Catch ex As Exception
                MsgBox(ex.Message)
                Return False

            End Try
        End Function
    End Class


