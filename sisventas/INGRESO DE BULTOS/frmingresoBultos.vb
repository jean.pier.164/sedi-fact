﻿Public Class FrmingresoBultos
    Private dt As New DataTable
    Private Sub mostrar()
        Try
            Dim func As New Fingresobultos

            dt = func.mostrar

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt
                txtbuscar.Enabled = True

                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing
                txtbuscar.Enabled = False

                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try

        buscar()

    End Sub

    Private Sub buscar()
        Try

            Dim ds As New DataSet

            ds.Tables.Add(dt.Copy)

            Dim dv As New DataView(ds.Tables(0))


            dv.RowFilter = cbocampo.Text & " like'" & txtbuscar.Text & "%'"

            If dv.Count <> 0 Then
                inexistente.Visible = False
                datalistado.DataSource = dv
                ocultar_columnas()

            Else
                inexistente.Visible = True
                datalistado.DataSource = Nothing


            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub ocultar_columnas()
        datalistado.Columns(1).Visible = False

    End Sub


    Private Sub frmFamilia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        mostrar()



        txtfecha_ingreso.Text = dtfecha_ingreso.Value

        txtfecha_traslado.Text = dtfecha_traslado.Value


    End Sub

    Private Sub datalistado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellDoubleClick






        'If txtflag.Text = "1" Then





        '    frmproducto.txtidfamilia.Text = datalistado.SelectedCells.Item(1).Value
        '    frmproducto.txtFamilia.Text = datalistado.SelectedCells.Item(2).Value

        '    Me.Close()







        'End If

    End Sub

    Private Sub datalistado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellContentClick

    End Sub

    Private Sub btnIngresa_Click(sender As Object, e As EventArgs) Handles btnIngresa.Click
        If Me.ValidateChildren = True And txtguia_trasnportista.Text <> "" Then
            Try
                Dim dts As New vingresobultos
                Dim func As New Fingresobultos

                dts.gfecha_traslado = txtfecha_traslado.Text
                dts.gfecha_ingreso = txtfecha_ingreso.Text
                dts.gguia_transportista = txtguia_trasnportista.Text
                dts.gbultos_recibidos = txtbultos_recibidos.Text
                dts.ggrupo_proveedor = txtgrupo_proveedor.Text
                dts.gserie = txtserie.Text
                dts.gcorrelativo = txtcorrelativo.Text




                If func.insertar(dts) Then
                    MessageBox.Show("Categoria Registrada Correctamente", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar()

                Else
                    MessageBox.Show("Categoria no fue registrada intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar()


                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub dtfecha_traslado_ValueChanged(sender As Object, e As EventArgs) Handles dtfecha_traslado.ValueChanged


        txtfecha_traslado.Text = dtfecha_traslado.Value
    End Sub

    Private Sub dtfecha_ingreso_ValueChanged(sender As Object, e As EventArgs) Handles dtfecha_ingreso.ValueChanged
        txtfecha_ingreso.Text = dtfecha_ingreso.Value
    End Sub
End Class
