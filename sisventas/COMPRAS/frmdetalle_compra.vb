﻿Public Class frmdetalle_compra

    Private dt As New DataTable


    Public Sub limpiar()
        'btnguardar.Visible = True
        txtidproducto.Text = ""
        txtnombre_producto.Text = ""
        txtstock_almacen.Text = 0
        txtstock_factura.Text = 0
    End Sub
    Private Sub mostrar_detalle()
        Try
            Dim func As New fdetalle_compra
            dt = func.mostrar_detalle_compra





            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt


                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
                ocultar_columnas()
            Else
                datalistado.DataSource = Nothing


                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try



        buscar()


    End Sub

    Private Sub buscar()
        'Try

        '    Dim ds As New DataSet

        '    ds.Tables.Add(dt.Copy)

        '    Dim dv As New DataView(ds.Tables(0))


        '    dv.RowFilter = "idcompras=  '" & txtidcompra.Text & "'"

        '    If dv.Count <> 0 Then
        '        inexistente.Visible = False
        '        datalistado.DataSource = dv
        '        ocultar_columnas()

        '    Else
        '        inexistente.Visible = True
        '        datalistado.DataSource = Nothing


        '    End If

        'Catch ex As Exception
        '    MsgBox(ex.Message)

        'End Try
    End Sub

    Private Sub ocultar_columnas()

        datalistado.Columns(0).Visible = False
        datalistado.Columns(1).Visible = False
        datalistado.Columns(2).Visible = False
    End Sub

    Private Sub frmdetalle_compra_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        btnguardar.Enabled = False
        mostrar_detalle()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)
        Try


            Dim vdb As New vdetalle_compra
            Dim func As New fdetalle_compra

            vdb.giddetalle_ingreso = txtiddetalle_ingreso.Text


            If func.eliminar(vdb) Then
                'If func.aumentar_stock(vdb) Then

                'End If
                Call mostrar_detalle()
                MessageBox.Show("Quitado de la venta?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Articulo no fue quitado de la venta?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If



            Call mostrar_detalle()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnguardar_Click(sender As Object, e As EventArgs)
        If Me.ValidateChildren = True And txtidproducto.Text <> "" And txtstock_factura.Text <> "" And txtstock_total.Text <> "" Then
            Try
                Dim dts As New vdetalle_compra
                Dim func As New fdetalle_compra

                dts.gidingreso = txtidingreso.Text
                dts.gidproducto = txtidproducto.Text
                dts.gprecio_compra = txtprecio_compra.Text
                dts.gprecio_venta = txtprecio_venta.Text

                dts.gstock_inicial = txtstock_factura.Text
                dts.gstock_actual = txtstock_total.Text
                dts.gfecha_compra = txtfecha.Value
                dts.gundm = txtundm.Text
                dts.gcundm = txtcundm.Text








                If func.insertar(dts) Then




                    MessageBox.Show("Articulo fue añadido Correctamente vamos añadir productos", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar_detalle()



                Else
                    MessageBox.Show("Articulo  no fue añadido Correctamente, intente de nuevos", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar_detalle()



                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub btnbuscar_producto_Click(sender As Object, e As EventArgs)
        frmproducto.txtflag.Text = "1"
        frmproducto.ShowDialog()
    End Sub

    Private Sub datalistado_CellClick(sender As Object, e As DataGridViewCellEventArgs)
        txtiddetalle_ingreso.Text = datalistado.SelectedCells.Item(0).Value
        txtidproducto.Text = datalistado.SelectedCells.Item(2).Value
        txtundm.Text = datalistado.SelectedCells.Item(3).Value
        txtnombre_producto.Text = datalistado.SelectedCells.Item(4).Value
        txtstock_almacen.Text = datalistado.SelectedCells.Item(5).Value
        txtprecio_compra.Text = datalistado.SelectedCells.Item(6).Value
        txtprecio_venta.Text = datalistado.SelectedCells.Item(7).Value
    End Sub

    Private Sub btnactualizar_stock_Click(sender As Object, e As EventArgs) Handles btnactualizar_stock.Click
        Dim result As DialogResult

        result = MessageBox.Show("Ingresando Stock  ?", "Actualizando Stock", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)

        If result = DialogResult.OK Then

        End If

        If Me.ValidateChildren = True And txtidproducto.Text <> "" Then
            Try


                Dim dts As New vproducto_fact
                Dim func As New fproducto_fact
                dts.gidproducto = txtidproducto.Text

                dts.gstock = txtstock_total.Text




                If func.aumentar_stock_fact(dts) Then
                    MessageBox.Show("Actualizado Correctamente", "Modificando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    btnguardar.Enabled = True

                Else
                    MessageBox.Show("No fue Actualizado intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)



                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If

    End Sub

    Private Sub btnbuscar_producto_Click_1(sender As Object, e As EventArgs) Handles btnbuscar_producto.Click
        frmproducto.txtflag.Text = "1"
        frmproducto.ShowDialog()
    End Sub

    Private Sub btnguardar_Click_1(sender As Object, e As EventArgs) Handles btnguardar.Click
        If Me.ValidateChildren = True And txtidproducto.Text <> "" And txtstock_factura.Text <> "" And txtprecio_compra.Text <> "" Then
            Try
                Dim dts As New vdetalle_compra
                Dim func As New fdetalle_compra

                dts.gidingreso = txtidingreso.Text
                dts.gidproducto = txtidproducto.Text
                dts.gprecio_compra = txtprecio_compra.Text
                dts.gprecio_venta = txtprecio_venta.Text

                dts.gstock_inicial = txtstock_factura.Text
                dts.gstock_actual = txtstock_total.Text
                dts.gfecha_compra = txtfecha.Value
                dts.gundm = txtundm.Text
                dts.gcundm = txtcundm.Text


                If func.insertar(dts) Then




                    MessageBox.Show("Articulo fue añadido Correctamente vamos añadir productos", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar_detalle()
                    btnguardar.Enabled = False


                Else
                    MessageBox.Show("Articulo  no fue añadido Correctamente, intente de nuevos", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar_detalle()



                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs) Handles Button1.Click
        Try


            Dim vdb As New vdetalle_compra
            Dim func As New fdetalle_compra

            vdb.giddetalle_ingreso = txtiddetalle_ingreso.Text


            If func.eliminar(vdb) Then
                'If func.aumentar_stock(vdb) Then

                'End If
                Call mostrar_detalle()
                MessageBox.Show("Quitado de la venta?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Articulo no fue quitado de la venta?", "Eliminando registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If



            Call mostrar_detalle()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

    End Sub

    Private Sub btnimprimir_Click(sender As Object, e As EventArgs) Handles btnimprimir.Click

    End Sub

    Private Sub txtstock_factura_TextChanged(sender As Object, e As EventArgs) Handles txtstock_factura.TextChanged
        Me.txtstock_total.Text = CStr(Val(txtstock_almacen.Text) + Val(txtstock_factura.Text))
    End Sub

    Private Sub inexistente_Click(sender As Object, e As EventArgs) Handles inexistente.Click

    End Sub
End Class