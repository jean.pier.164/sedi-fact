﻿

Imports System.Data.SqlClient
    Public Class fdetalle_compra


        Inherits conexion
        Dim cmd As New SqlCommand

        'Public Function mostrar_compra() As DataTable
        '    Try
        '        conectado()
        '        cmd = New SqlCommand("mostrar_detalle_compra")
        '        cmd.CommandType = CommandType.StoredProcedure
        '        cmd.Connection = cnn

        '        If cmd.ExecuteNonQuery Then
        '            Dim dt As New DataTable
        '            Dim da As New SqlDataAdapter(cmd)
        '            da.Fill(dt)
        '            Return dt
        '        Else

        '            Return Nothing
        '        End If
        '    Catch ex As Exception
        '        MsgBox(ex.Message)
        '        Return Nothing
        '    Finally
        '        desconectado()

        '    End Try

        'End Function


        Public Function insertar(ByVal dts As vdetalle_compra) As Boolean
            Try
                conectado()
                cmd = New SqlCommand("insertar_detalle_compra")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = cnn

                cmd.Parameters.AddWithValue("@idingreso", dts.gidingreso)
                cmd.Parameters.AddWithValue("@idproducto", dts.gidproducto)
                cmd.Parameters.AddWithValue("@precio_compra", dts.gprecio_compra)
                cmd.Parameters.AddWithValue("@precio_venta", dts.gprecio_venta)
                cmd.Parameters.AddWithValue("@stock_inicial", dts.gstock_inicial)
                cmd.Parameters.AddWithValue("@stock_actual", dts.gstock_actual)
                cmd.Parameters.AddWithValue("@undm", dts.gundm)
                cmd.Parameters.AddWithValue("@cundm", dts.gcundm)





                If cmd.ExecuteNonQuery Then
                    Return True

                Else
                    Return False

                End If

            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            Finally
                desconectado()

            End Try

        End Function
        Public Function editar(ByVal dts As vdetalle_compra) As Boolean
            Try
                conectado()
                cmd = New SqlCommand("editar_detalle_compra")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = cnn
                cmd.Parameters.AddWithValue("@iddetalle_ingreso", dts.giddetalle_ingreso)
                cmd.Parameters.AddWithValue("@idingreso", dts.gidingreso)
                cmd.Parameters.AddWithValue("@idproducto", dts.gidproducto)
                cmd.Parameters.AddWithValue("@precio_compra", dts.gprecio_compra)
                cmd.Parameters.AddWithValue("@precio_venta", dts.gprecio_venta)
                cmd.Parameters.AddWithValue("@stock_inicial", dts.gstock_inicial)
                cmd.Parameters.AddWithValue("@stock_actual", dts.gstock_actual)
                cmd.Parameters.AddWithValue("@undm", dts.gundm)
                cmd.Parameters.AddWithValue("@cundm", dts.gcundm)




                If cmd.ExecuteNonQuery Then
                    Return True

                Else
                    Return False

                End If

            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            Finally
                desconectado()

            End Try

        End Function

        Public Function eliminar(ByVal dts As vdetalle_compra) As Boolean
            Try
                conectado()
                cmd = New SqlCommand("eliminar_detalle_compra")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = cnn

                cmd.Parameters.Add("@iddetalle_ingreso", SqlDbType.NVarChar, 50).Value = dts.giddetalle_ingreso

                If cmd.ExecuteNonQuery Then
                    Return True
                Else
                    Return False

                End If

            Catch ex As Exception
                MsgBox(ex.Message)
                Return False

            End Try
        End Function

        Public Function mostrar_detalle_compra() As DataTable
            Try
                conectado()
                cmd = New SqlCommand("mostrar_detalle_compra")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = cnn
                cmd.Parameters.AddWithValue("@idingreso", frmdetalle_compra.txtidingreso.Text)

                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(cmd)
                da.Fill(dt)
                Return dt
            Catch ex As Exception
                MsgBox(ex.Message)
                Return Nothing
            Finally
                desconectado()

            End Try
        End Function
    End Class

