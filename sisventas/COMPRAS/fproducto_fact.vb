﻿Imports System.Data.SqlClient
Public Class fproducto_fact


    Inherits conexion

    Dim cmd As New SqlCommand
    Public Function aumentar_stock_fact(ByVal dts As vproducto_fact) As Boolean
        Try
            conectado()
            cmd = New SqlCommand("aumentar_stock_factura")
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Connection = cnn

            cmd.Parameters.AddWithValue("@idproducto", dts.gidproducto)
            cmd.Parameters.AddWithValue("@stock", dts.gstock)


            If cmd.ExecuteNonQuery Then
                Return True

            Else
                Return False

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Return False
        Finally
            desconectado()

        End Try

    End Function

End Class
