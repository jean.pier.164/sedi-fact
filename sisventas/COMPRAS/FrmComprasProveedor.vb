﻿Public Class FrmComprasProveedor
    Private dt As New DataTable

    Private Sub cargar_detalle()


        frmdetalle_compra.txtidingreso.Text = datalistado.SelectedCells.Item(1).Value
        frmdetalle_compra.txtidusuario.Text = datalistado.SelectedCells.Item(2).Value

        frmdetalle_compra.txtidproveedor.Text = datalistado.SelectedCells.Item(3).Value
        frmdetalle_compra.txtnombre_proveedor.Text = datalistado.SelectedCells.Item(4).Value

        frmdetalle_compra.txtfecha.Text = datalistado.SelectedCells.Item(5).Value




        frmdetalle_compra.cbtipo_documento.Text = datalistado.SelectedCells.Item(6).Value
        frmdetalle_compra.txtserie.Text = datalistado.SelectedCells.Item(7).Value

        frmdetalle_compra.txtcorrelativo.Text = datalistado.SelectedCells.Item(8).Value
        frmdetalle_compra.txtigv.Text = datalistado.SelectedCells.Item(9).Value
        frmdetalle_compra.txtestado.Text = datalistado.SelectedCells.Item(10).Value
        frmdetalle_compra.txtusuario.Text = datalistado.SelectedCells.Item(11).Value



        frmdetalle_compra.ShowDialog()

    End Sub

    Private Sub mostrarusuario()

        Dim func As New fusuario
        dt = func.mostrarusu

    End Sub
    Public Sub limpiar()
        btnguardar.Visible = True
        btneditar.Visible = False
        txtidproveedor.Text = ""
        txtnombre_proveedor.Text = ""
        txtcorrelativo.Text = ""

        txtidingreso.Text = ""

    End Sub


    Private Sub mostrar()
        Try
            Dim func As New fcompras
            dt = func.mostrar

            datalistado.Columns.Item("Eliminar").Visible = False

            If dt.Rows.Count <> 0 Then
                datalistado.DataSource = dt
                txtbuscar.Enabled = True

                datalistado.ColumnHeadersVisible = True
                inexistente.Visible = False
            Else
                datalistado.DataSource = Nothing
                txtbuscar.Enabled = False

                datalistado.ColumnHeadersVisible = False
                inexistente.Visible = True
            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
        btnnuevo.Visible = True
        btneditar.Visible = False

        buscar()

    End Sub

    Private Sub buscar()
        Try

            Dim ds As New DataSet

            ds.Tables.Add(dt.Copy)

            Dim dv As New DataView(ds.Tables(0))


            dv.RowFilter = cbocampo.Text & " like'" & txtbuscar.Text & "%'"

            If dv.Count <> 0 Then
                inexistente.Visible = False
                datalistado.DataSource = dv
                ocultar_columnas()

            Else
                inexistente.Visible = True
                datalistado.DataSource = Nothing


            End If

        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub ocultar_columnas()


    End Sub
    Private Sub FrmComprasProveedor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        mostrar()
        mostrarusuario()
    End Sub

    Private Sub btnguardar_Click(sender As Object, e As EventArgs) Handles btnguardar.Click
        If Me.ValidateChildren = True And txtserie.Text <> "" Then
            Try
                Dim dts As New vcompras
                Dim func As New fcompras

                dts.gidusuario = txtidusuario.Text
                dts.gidproveedor = txtidproveedor.Text
                dts.gfecha = txtfecha.Value
                dts.gtipo_comprobante = cbtipo_documento.Text
                dts.gserie = txtserie.Text
                dts.gcorrelativo = txtcorrelativo.Text
                dts.gigv = txtigv.Text
                dts.gestado = cboestado.Text






                If func.insertar(dts) Then


                    MessageBox.Show("Venta Registrada Correctamente vamos añadir productos", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar()
                    limpiar()
                    cargar_detalle()


                Else
                    MessageBox.Show("Venta no fue registrada intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar()


                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub btnbuscar_proveedor_Click(sender As Object, e As EventArgs) Handles btnbuscar_proveedor.Click
        frmproveedor.txtflag.Text = "1"
        frmproveedor.ShowDialog()
    End Sub

    Private Sub datalistado_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellDoubleClick
        cargar_detalle()
    End Sub

    Private Sub datalistado_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellClick
        txtidingreso.Text = datalistado.SelectedCells.Item(1).Value
        txtidusuario.Text = datalistado.SelectedCells.Item(2).Value

        txtidproveedor.Text = datalistado.SelectedCells.Item(3).Value



        txtnombre_proveedor.Text = datalistado.SelectedCells.Item(4).Value

        txtfecha.Text = datalistado.SelectedCells.Item(5).Value


        cbtipo_documento.Text = datalistado.SelectedCells.Item(6).Value
        txtserie.Text = datalistado.SelectedCells.Item(7).Value

        txtcorrelativo.Text = datalistado.SelectedCells.Item(8).Value
        txtigv.Text = datalistado.SelectedCells.Item(9).Value
        cboestado.Text = datalistado.SelectedCells.Item(10).Value
        txtusuario.Text = datalistado.SelectedCells.Item(11).Value

        btnnuevo.Visible = True
        btneditar.Visible = True
        btnguardar.Visible = False


    End Sub

    Private Sub datalistado_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles datalistado.CellContentClick
        If e.ColumnIndex = Me.datalistado.Columns.Item("Eliminar").Index Then
            Dim chkcell As DataGridViewCheckBoxCell = Me.datalistado.Rows(e.RowIndex).Cells("Eliminar")
            chkcell.Value = Not chkcell.Value

        End If
    End Sub

    Private Sub btneditar_Click(sender As Object, e As EventArgs) Handles btneditar.Click
        Dim result As DialogResult

        result = MessageBox.Show("Realmente desea editar los datos de la Venta ?", "Modificando Registros", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)

        If result = DialogResult.OK Then

        End If

        If Me.ValidateChildren = True And txtcorrelativo.Text <> "" And txtidingreso.Text <> "" Then
            Try
                Dim dts As New vcompras
                Dim func As New fcompras
                dts.gidingreso = txtidingreso.Text

                dts.gidusuario = txtidusuario.Text
                dts.gidproveedor = txtidproveedor.Text
                dts.gfecha = txtfecha.Value
                dts.gtipo_comprobante = cbtipo_documento.Text
                dts.gserie = txtserie.Text
                dts.gcorrelativo = txtcorrelativo.Text
                dts.gigv = txtigv.Text
                dts.gestado = cboestado.Text







                If func.editar(dts) Then
                    MessageBox.Show("Venta Modificada Correctamente", "Modificando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    mostrar()
                    limpiar()
                Else
                    MessageBox.Show("Venta no fue Modificada intente de nuevo", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    mostrar()
                    limpiar()

                End If



            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            MessageBox.Show("falta ingresar algun dato", "Guardando Registros", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub
End Class