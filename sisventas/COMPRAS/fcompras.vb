﻿Imports System.Data.SqlClient
Public Class fcompras
        Inherits conexion
        Dim cmd As New SqlCommand

        Public Function mostrar() As DataTable

            Try
                conectado()
                cmd = New SqlCommand("mostrar_compra")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = cnn

                If cmd.ExecuteNonQuery Then
                    Dim dt As New DataTable
                    Dim da As New SqlDataAdapter(cmd)
                    da.Fill(dt)
                    Return dt
                Else
                    Return Nothing
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                Return Nothing
            Finally
                desconectado()
            End Try



        End Function


        Public Function insertar(ByVal dts As vcompras) As Boolean
            Try
                conectado()
                cmd = New SqlCommand("insertar_compras")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = cnn

                cmd.Parameters.AddWithValue("@idusuario", dts.gidusuario)
                cmd.Parameters.AddWithValue("@idproveedor", dts.gidproveedor)
                cmd.Parameters.AddWithValue("@fecha", dts.gfecha)
                cmd.Parameters.AddWithValue("@tipo_comprobante", dts.gtipo_comprobante)
                cmd.Parameters.AddWithValue("@serie", dts.gserie)
                cmd.Parameters.AddWithValue("@correlativo", dts.gcorrelativo)
                cmd.Parameters.AddWithValue("@igv", dts.gigv)
                cmd.Parameters.AddWithValue("@estado", dts.gestado)


                If cmd.ExecuteNonQuery Then
                    Return True

                Else
                    Return False

                End If

            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            Finally
                desconectado()

            End Try
        End Function


        Public Function editar(ByVal dts As vcompras) As Boolean


            Try
                conectado()
                cmd = New SqlCommand("editar_compras")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = cnn
                cmd.Parameters.AddWithValue("@idingreso", dts.gidingreso)
                cmd.Parameters.AddWithValue("@idusuario", dts.gidusuario)
                cmd.Parameters.AddWithValue("@idproveedor", dts.gidproveedor)
                cmd.Parameters.AddWithValue("@fecha", dts.gfecha)
                cmd.Parameters.AddWithValue("@tipo_comprobante", dts.gtipo_comprobante)
                cmd.Parameters.AddWithValue("@serie", dts.gserie)
                cmd.Parameters.AddWithValue("@correlativo", dts.gcorrelativo)
                cmd.Parameters.AddWithValue("@igv", dts.gigv)
                cmd.Parameters.AddWithValue("@estado", dts.gestado)


                If cmd.ExecuteNonQuery Then
                    Return True
                Else
                    Return False
                End If

            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            Finally
                desconectado()

            End Try




        End Function

        Public Function eliminar(ByVal dts As vcompras) As Boolean

            Try
                conectado()
                cmd = New SqlCommand("eliminar_compras")
                cmd.CommandType = CommandType.StoredProcedure
                cmd.Connection = cnn
                cmd.Parameters.Add("@idingreso", SqlDbType.NVarChar, 50).Value = dts.gidingreso

                If cmd.ExecuteNonQuery Then
                    Return True
                Else
                    Return False
                End If

            Catch ex As Exception
                MsgBox(ex.Message)
                Return False
            End Try
        End Function

    End Class


