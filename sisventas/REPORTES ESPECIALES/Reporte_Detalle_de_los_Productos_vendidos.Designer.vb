﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Reporte_Detalle_de_los_Productos_vendidos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.ndsReporte_detalle_de_los_Productos_Vendidos = New BRAVOSPORT.ndsReporte_detalle_de_los_Productos_Vendidos()
        Me.dosfechas_detalle_productos_vendidosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.dosfechas_detalle_productos_vendidosTableAdapter = New BRAVOSPORT.ndsReporte_detalle_de_los_Productos_VendidosTableAdapters.dosfechas_detalle_productos_vendidosTableAdapter()
        Me.txtfechafi = New System.Windows.Forms.TextBox()
        Me.txtfechai = New System.Windows.Forms.TextBox()
        CType(Me.ndsReporte_detalle_de_los_Productos_Vendidos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dosfechas_detalle_productos_vendidosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "Reporte_detalle_de_los_Productos_Vendidos"
        ReportDataSource1.Value = Me.dosfechas_detalle_productos_vendidosBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "BRAVOSPORT.Reporte_detalle_de_los_Productos_Vendidos.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(903, 450)
        Me.ReportViewer1.TabIndex = 16
        '
        'ndsReporte_detalle_de_los_Productos_Vendidos
        '
        Me.ndsReporte_detalle_de_los_Productos_Vendidos.DataSetName = "ndsReporte_detalle_de_los_Productos_Vendidos"
        Me.ndsReporte_detalle_de_los_Productos_Vendidos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'dosfechas_detalle_productos_vendidosBindingSource
        '
        Me.dosfechas_detalle_productos_vendidosBindingSource.DataMember = "dosfechas_detalle_productos_vendidos"
        Me.dosfechas_detalle_productos_vendidosBindingSource.DataSource = Me.ndsReporte_detalle_de_los_Productos_Vendidos
        '
        'dosfechas_detalle_productos_vendidosTableAdapter
        '
        Me.dosfechas_detalle_productos_vendidosTableAdapter.ClearBeforeFill = True
        '
        'txtfechafi
        '
        Me.txtfechafi.Location = New System.Drawing.Point(234, 215)
        Me.txtfechafi.Name = "txtfechafi"
        Me.txtfechafi.Size = New System.Drawing.Size(100, 20)
        Me.txtfechafi.TabIndex = 18
        Me.txtfechafi.Visible = False
        '
        'txtfechai
        '
        Me.txtfechai.Location = New System.Drawing.Point(90, 215)
        Me.txtfechai.Name = "txtfechai"
        Me.txtfechai.Size = New System.Drawing.Size(100, 20)
        Me.txtfechai.TabIndex = 17
        Me.txtfechai.Visible = False
        '
        'Reporte_Detalle_de_los_Productos_vendidos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(903, 450)
        Me.Controls.Add(Me.txtfechafi)
        Me.Controls.Add(Me.txtfechai)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "Reporte_Detalle_de_los_Productos_vendidos"
        Me.Text = "Reporte_Detalle_de_los_Productos_vendidos"
        CType(Me.ndsReporte_detalle_de_los_Productos_Vendidos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dosfechas_detalle_productos_vendidosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents dosfechas_detalle_productos_vendidosBindingSource As BindingSource
    Friend WithEvents ndsReporte_detalle_de_los_Productos_Vendidos As ndsReporte_detalle_de_los_Productos_Vendidos
    Friend WithEvents dosfechas_detalle_productos_vendidosTableAdapter As ndsReporte_detalle_de_los_Productos_VendidosTableAdapters.dosfechas_detalle_productos_vendidosTableAdapter
    Friend WithEvents txtfechafi As TextBox
    Friend WithEvents txtfechai As TextBox
End Class
