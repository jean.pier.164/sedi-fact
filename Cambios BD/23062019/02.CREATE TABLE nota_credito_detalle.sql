/****** Object:  Table [dbo].[nota_credito_detalle]    Script Date: 23/06/2019 17:33:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[nota_credito_detalle](
	[iddetalle_nota_credito] [int] IDENTITY(1,1) NOT NULL,
	[idnotacredito] [int] NOT NULL,
	[idproducto] [int] NOT NULL,
	[cantidad] [int] NOT NULL,
	[precio_unitario] [decimal](18, 2) NOT NULL,
	[undm] [varchar](50) NULL,
	[iddetalle_ingreso] [int] NULL,
	[cundm] [int] NULL,
	[sundm] [varchar](50) NULL,
	[precio_compra] [decimal](18, 2) NULL,
 CONSTRAINT [PK_nota_credito_detalle] PRIMARY KEY CLUSTERED 
(
	[iddetalle_nota_credito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


