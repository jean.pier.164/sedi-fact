﻿using System.Threading.Tasks;
//using WinApp.Comun.Dto.Intercambio;

namespace FinalXML.Interfaces
{
    public interface ICertificador
    {
        Task<FirmadoResponse> FirmarXml(FirmadoRequest request);
    }
}
