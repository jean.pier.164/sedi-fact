﻿namespace FinalXML
{
    public class EnviarResumenResponse : RespuestaComunConArchivo//RespuestaComun
    {
        public string CodigoRespuesta { get; set; }
        public string MensajeRespuesta { get; set; }
        public string TramaZipCdr { get; set; }

        public string NroTicket { get; set; }
    }
}
