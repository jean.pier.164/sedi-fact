﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using FinalXML.Interfaces;

namespace FinalXML
{
    [Serializable]
    public class Invoice : IXmlSerializable, IEstructuraXml
    {
        public DateTime IssueDate { get; set; }
        public UBLExtensions UblExtensions { get; set; }
        public SignatureCac Signature { get; set; }

        public AccountingSupplierParty AccountingSupplierParty { get; set; }

        public string InvoiceTypeCode { get; set; }

        public string Id { get; set; }

        public AccountingSupplierParty AccountingCustomerParty { get; set; }
        public List<InvoiceLine> InvoiceLines { get; set; }
        public List<InvoiceDocumentReference> DespatchDocumentReferences { get; set; }
        public List<InvoiceDocumentReference> AdditionalDocumentReferences { get; set; }
        public string DocumentCurrencyCode { get; set; }
        public List<TaxTotal> TaxTotals { get; set; }
        public LegalMonetaryTotal LegalMonetaryTotal { get; set; }
        public BillingPayment PrepaidPayment { get; set; }
        public string UblVersionId { get; set; }
        public string CustomizationId { get; set; }
        public InvoiceTypeCode InvoiceTypeCode2  { get; set;}
        public DocumentCurrencyCode DocumentCurrencyCode2 { get; set; }
        public PartyIdentification2 PartyIdentification2 { get; set; }
        public IFormatProvider Formato { get; set; }
        public string LineCountNumeric { get; set; }

        public Invoice()
        {
            AccountingSupplierParty = new AccountingSupplierParty();
            AccountingCustomerParty = new AccountingSupplierParty();
            DespatchDocumentReferences = new List<InvoiceDocumentReference>();
            UblExtensions = new UBLExtensions();
            Signature = new SignatureCac();
            InvoiceLines = new List<InvoiceLine>();
            TaxTotals = new List<TaxTotal>();
            LegalMonetaryTotal = new LegalMonetaryTotal();
            UblVersionId = "2.0";
            CustomizationId = "1.0";
            Formato = new System.Globalization.CultureInfo("es-PE");
            InvoiceTypeCode2 = new InvoiceTypeCode();
            DocumentCurrencyCode2 = new DocumentCurrencyCode();
            PartyIdentification2 = new PartyIdentification2();
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(XmlWriter writer)
        {
            writer.WriteAttributeString("xmlns", EspacioNombres.xmlnsInvoice);
            writer.WriteAttributeString("xmlns:cac", EspacioNombres.cac);
            writer.WriteAttributeString("xmlns:cbc", EspacioNombres.cbc);
            writer.WriteAttributeString("xmlns:ccts", EspacioNombres.ccts);
            writer.WriteAttributeString("xmlns:ds", EspacioNombres.ds);
            writer.WriteAttributeString("xmlns:ext", EspacioNombres.ext);
            writer.WriteAttributeString("xmlns:qdt", EspacioNombres.qdt);
            writer.WriteAttributeString("xmlns:sac", EspacioNombres.sac);
            writer.WriteAttributeString("xmlns:udt", EspacioNombres.udt);
            writer.WriteAttributeString("xmlns:xsi", EspacioNombres.xsi);
            #region UBLExtensions
            writer.WriteStartElement("ext:UBLExtensions");

            #region UBLExtension
            var ext2 = UblExtensions.Extension2.ExtensionContent.AdditionalInformation;
            /*writer.WriteStartElement("ext:UBLExtension");

            #region ExtensionContent
            writer.WriteStartElement("ext:ExtensionContent");

            #region AdditionalInformation
            writer.WriteStartElement("sac:AdditionalInformation");
            {

                #region AdditionalMonetaryTotal

                {

                    foreach (var additionalMonetaryTotal in ext2.AdditionalMonetaryTotals)
                    {
                        writer.WriteStartElement("sac:AdditionalMonetaryTotal");
                        writer.WriteElementString("cbc:ID", additionalMonetaryTotal.ID);

                        #region PayableAmount

                        {
                            writer.WriteStartElement("cbc:PayableAmount");
                            {
                                writer.WriteAttributeString("currencyID", additionalMonetaryTotal.PayableAmount.currencyID);
                                writer.WriteValue(additionalMonetaryTotal.PayableAmount.value.ToString(Constantes.FormatoNumerico, Formato));

                            }
                            writer.WriteEndElement();
                        }
                        if (additionalMonetaryTotal.Percent > 0)
                        {
                            writer.WriteElementString("cbc:Percent",
                                additionalMonetaryTotal.Percent.ToString(Constantes.FormatoNumerico));
                        }
                        #endregion

                        writer.WriteEndElement();
                    }
                }

                #endregion

                #region AdditionalProperty

                {
                    foreach (var additionalProperty in ext2.AdditionalProperties)
                    {
                        writer.WriteStartElement("sac:AdditionalProperty");
                        writer.WriteElementString("cbc:ID", additionalProperty.ID);

                        #region Value

                        writer.WriteElementString("cbc:Value", additionalProperty.Value);

                        #endregion

                        writer.WriteEndElement();
                    }
                }

                #endregion

                #region SUNATEmbededDespatchAdvice
                // Para el caso de Factura-Guia.
                if (!string.IsNullOrEmpty(ext2.SunatEmbededDespatchAdvice.DeliveryAddress.ID))
                {
                    writer.WriteStartElement("sac:SUNATEmbededDespatchAdvice");
                    {
                        #region DeliveryAddress
                        writer.WriteStartElement("cac:DeliveryAddress");
                        {
                            writer.WriteElementString("cbc:ID", ext2.SunatEmbededDespatchAdvice.DeliveryAddress.ID);
                            writer.WriteElementString("cbc:StreetName", ext2.SunatEmbededDespatchAdvice.DeliveryAddress.StreetName);
                            if (!string.IsNullOrEmpty(ext2.SunatEmbededDespatchAdvice.DeliveryAddress.CitySubdivisionName))
                                writer.WriteElementString("cbc:CitySubdivisionName", ext2.SunatEmbededDespatchAdvice.DeliveryAddress.CitySubdivisionName);
                            writer.WriteElementString("cbc:CityName", ext2.SunatEmbededDespatchAdvice.DeliveryAddress.CityName);
                            writer.WriteElementString("cbc:CountrySubentity", ext2.SunatEmbededDespatchAdvice.DeliveryAddress.CountrySubentity);
                            writer.WriteElementString("cbc:District", ext2.SunatEmbededDespatchAdvice.DeliveryAddress.District);
                            writer.WriteStartElement("cac:Country");
                            {
                                writer.WriteElementString("cbc:IdentificationCode", ext2.SunatEmbededDespatchAdvice.DeliveryAddress.Country.IdentificationCode);
                            }
                            writer.WriteEndElement();
                        }
                        writer.WriteEndElement();
                        #endregion

                        #region OriginAddress
                        writer.WriteStartElement("cac:OriginAddress");
                        {
                            writer.WriteElementString("cbc:ID", ext2.SunatEmbededDespatchAdvice.OriginAddress.ID);
                            writer.WriteElementString("cbc:StreetName", ext2.SunatEmbededDespatchAdvice.OriginAddress.StreetName);
                            if (!string.IsNullOrEmpty(ext2.SunatEmbededDespatchAdvice.OriginAddress.CitySubdivisionName))
                                writer.WriteElementString("cbc:CitySubdivisionName", ext2.SunatEmbededDespatchAdvice.OriginAddress.CitySubdivisionName);
                            writer.WriteElementString("cbc:CityName", ext2.SunatEmbededDespatchAdvice.OriginAddress.CityName);
                            writer.WriteElementString("cbc:CountrySubentity", ext2.SunatEmbededDespatchAdvice.OriginAddress.CountrySubentity);
                            writer.WriteElementString("cbc:District", ext2.SunatEmbededDespatchAdvice.OriginAddress.District);
                            writer.WriteStartElement("cac:Country");
                            {
                                writer.WriteElementString("cbc:IdentificationCode", ext2.SunatEmbededDespatchAdvice.OriginAddress.Country.IdentificationCode);
                            }
                            writer.WriteEndElement();
                        }
                        writer.WriteEndElement();
                        #endregion

                        #region SUNATCarrierParty
                        writer.WriteStartElement("sac:SUNATCarrierParty");
                        {
                            writer.WriteElementString("cbc:CustomerAssignedAccountID", ext2.SunatEmbededDespatchAdvice.SunatCarrierParty.CustomerAssignedAccountID);
                            writer.WriteElementString("cbc:AdditionalAccountID", ext2.SunatEmbededDespatchAdvice.SunatCarrierParty.AdditionalAccountID);
                            writer.WriteStartElement("cac:Party");
                            {
                                writer.WriteStartElement("cac:PartyLegalEntity");
                                {
                                    writer.WriteElementString("cbc:RegistrationName", ext2.SunatEmbededDespatchAdvice.SunatCarrierParty.Party.PartyLegalEntity.RegistrationName);
                                }
                                writer.WriteEndElement();
                            }
                            writer.WriteEndElement();
                        }
                        writer.WriteEndElement();
                        #endregion

                        #region DriverParty
                        writer.WriteStartElement("sac:DriverParty");
                        {
                            writer.WriteStartElement("cac:Party");
                            {
                                writer.WriteStartElement("cac:PartyIdentification");
                                {
                                    writer.WriteElementString("cbc:ID", ext2.SunatEmbededDespatchAdvice.DriverParty.PartyIdentification.ID.value);
                                }
                                writer.WriteEndElement();
                            }
                            writer.WriteEndElement();
                        }
                        writer.WriteEndElement();
                        #endregion

                        #region SUNATRoadTransport
                        writer.WriteStartElement("sac:SUNATRoadTransport");
                        {
                            writer.WriteElementString("cbc:LicensePlateID", ext2.SunatEmbededDespatchAdvice.SunatRoadTransport.LicensePlateId);
                            writer.WriteElementString("cbc:TransportAuthorizationCode", ext2.SunatEmbededDespatchAdvice.SunatRoadTransport.TransportAuthorizationCode);
                            writer.WriteElementString("cbc:BrandName", ext2.SunatEmbededDespatchAdvice.SunatRoadTransport.BrandName);
                        }
                        writer.WriteEndElement();
                        #endregion

                        writer.WriteElementString("cbc:TransportModeCode", ext2.SunatEmbededDespatchAdvice.TransportModeCode);

                        #region GrossWeightMeasure
                        writer.WriteStartElement("cbc:GrossWeightMeasure");
                        {
                            writer.WriteAttributeString("unitCode", ext2.SunatEmbededDespatchAdvice.GrossWeightMeasure.unitCode);
                            writer.WriteValue(ext2.SunatEmbededDespatchAdvice.GrossWeightMeasure.Value.ToString(Constantes.FormatoNumerico, Formato));
                        }
                        writer.WriteEndElement();
                        #endregion
                    }
                    writer.WriteEndElement();
                }
                #endregion

                #region SUNATTransaction
                if (!string.IsNullOrEmpty(ext2.SunatTransaction.Id))
                {
                    writer.WriteStartElement("sac:SUNATTransaction");
                    {
                        writer.WriteElementString("cbc:ID", ext2.SunatTransaction.Id);
                    }
                    writer.WriteEndElement();
                }
                #endregion

            }
            writer.WriteEndElement();
            #endregion

            writer.WriteEndElement();
            #endregion

            writer.WriteEndElement();*/
            #endregion

            #region UBLExtension

            writer.WriteStartElement("ext:UBLExtension");
            #region ExtensionContent
            writer.WriteStartElement("ext:ExtensionContent");

            // En esta zona va el certificado digital.

            writer.WriteEndElement();
            #endregion
            writer.WriteEndElement();
            #endregion

            writer.WriteEndElement();
            #endregion

            writer.WriteElementString("cbc:UBLVersionID", UblVersionId);
            writer.WriteElementString("cbc:CustomizationID", CustomizationId);

            writer.WriteElementString("cbc:ID", Id);
            writer.WriteElementString("cbc:IssueDate", IssueDate.ToString("yyyy-MM-dd"));
            writer.WriteElementString("cbc:IssueTime", String.Format("{0:HH:mm:ss}", DateTime.Now));
            writer.WriteElementString("cbc:DueDate", IssueDate.ToString("yyyy-MM-dd")); //Fecha de vencimiento de Documento F-B

            writer.WriteStartElement("cbc:InvoiceTypeCode");
            writer.WriteAttributeString("listID", ext2.SunatTransaction.Id); //Tipo de Venta Interna
            writer.WriteAttributeString("listAgencyName",InvoiceTypeCode2.listAgencyName);
            writer.WriteAttributeString("listName", InvoiceTypeCode2.listAgencyName);
            writer.WriteAttributeString("listURI", InvoiceTypeCode2.listURI);
            writer.WriteValue(InvoiceTypeCode); //Tipo de Documento 03 Boleta - 01 Factura
            writer.WriteEndElement();

            writer.WriteStartElement("cbc:Note");
            writer.WriteAttributeString("languageLocaleID", "1000"); //1000 Monto en letras              
            writer.WriteValue(ext2.AdditionalProperties[0].Value);
            writer.WriteEndElement();

            writer.WriteStartElement("cbc:DocumentCurrencyCode");
            writer.WriteAttributeString("listID", DocumentCurrencyCode2.listID);
            writer.WriteAttributeString("listName", DocumentCurrencyCode2.listName);
            writer.WriteAttributeString("listAgencyName", DocumentCurrencyCode2.listAgencyName);
            writer.WriteValue(DocumentCurrencyCode);
            writer.WriteEndElement();


            writer.WriteElementString("cbc:LineCountNumeric", LineCountNumeric); //Cantidad de Items de la factura

            #region DespatchDocumentReferences
            foreach (var reference in DespatchDocumentReferences)
            {
                writer.WriteStartElement("cac:DespatchDocumentReference");
                {
                    writer.WriteElementString("cbc:ID", reference.ID);
                    writer.WriteElementString("cbc:DocumentTypeCode", reference.DocumentTypeCode);
                }
                writer.WriteEndElement();
            }
            #endregion

            #region Signature
            writer.WriteStartElement("cac:Signature");
            writer.WriteElementString("cbc:ID", Signature.ID);

            #region SignatoryParty

            writer.WriteStartElement("cac:SignatoryParty");

            writer.WriteStartElement("cac:PartyIdentification");
            writer.WriteElementString("cbc:ID", Signature.SignatoryParty.PartyIdentification.ID.value);
            writer.WriteEndElement();

            #region PartyName
            writer.WriteStartElement("cac:PartyName");

            //writer.WriteStartElement("cbc:Name");
            //writer.WriteCData(Signature.SignatoryParty.PartyName.Name);
            //writer.WriteEndElement();
            writer.WriteElementString("cbc:Name", Signature.SignatoryParty.PartyName.Name);

            writer.WriteEndElement();
            #endregion

            writer.WriteEndElement();
            #endregion

            #region DigitalSignatureAttachment
            writer.WriteStartElement("cac:DigitalSignatureAttachment");

            writer.WriteStartElement("cac:ExternalReference");
            writer.WriteElementString("cbc:URI", Signature.DigitalSignatureAttachment.ExternalReference.URI.Trim());
            writer.WriteEndElement();

            writer.WriteEndElement();
            #endregion

            writer.WriteEndElement();
            #endregion

            #region AccountingSupplierParty
            writer.WriteStartElement("cac:AccountingSupplierParty");


            #region Party
            writer.WriteStartElement("cac:Party");

            #region PartyIdentification
            writer.WriteStartElement("cac:PartyIdentification");
            writer.WriteStartElement("cbc:ID");
            writer.WriteAttributeString("schemeID", AccountingSupplierParty.AdditionalAccountID); //Codigo de identificacion de documento de contribuyente
            writer.WriteAttributeString("schemeName", PartyIdentification2.schemeName);
            writer.WriteAttributeString("schemeAgencyName", InvoiceTypeCode2.listAgencyName);
            writer.WriteAttributeString("schemeURI", PartyIdentification2.schemeURI);
            writer.WriteValue(AccountingSupplierParty.CustomerAssignedAccountID);
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion PartyIdentification

            #region PartyName
            writer.WriteStartElement("cac:PartyName");

            writer.WriteStartElement("cbc:Name");
            writer.WriteCData(AccountingSupplierParty.Party.PartyName.Name);
            writer.WriteEndElement();

            writer.WriteEndElement();
            #endregion

            #region PostalAddress
            /*writer.WriteStartElement("cac:PostalAddress");
            writer.WriteElementString("cbc:ID", AccountingSupplierParty.Party.PostalAddress.ID);
            writer.WriteElementString("cbc:StreetName", AccountingSupplierParty.Party.PostalAddress.StreetName);
            if (!string.IsNullOrEmpty(AccountingSupplierParty.Party.PostalAddress.CitySubdivisionName))
                writer.WriteElementString("cbc:CitySubdivisionName", AccountingSupplierParty.Party.PostalAddress.CitySubdivisionName);
            writer.WriteElementString("cbc:CityName", AccountingSupplierParty.Party.PostalAddress.CityName);
            writer.WriteElementString("cbc:CountrySubentity", AccountingSupplierParty.Party.PostalAddress.CountrySubentity);
            writer.WriteElementString("cbc:District", AccountingSupplierParty.Party.PostalAddress.District);

            #region Country
            writer.WriteStartElement("cac:Country");
            writer.WriteElementString("cbc:IdentificationCode",
                AccountingSupplierParty.Party.PostalAddress.Country.IdentificationCode);
            writer.WriteEndElement();
            #endregion

            writer.WriteEndElement();*/
            #endregion

            #region PartyLegalEntity
            writer.WriteStartElement("cac:PartyLegalEntity");
            writer.WriteStartElement("cbc:RegistrationName");
            writer.WriteCData(AccountingSupplierParty.Party.PartyLegalEntity.RegistrationName);
            writer.WriteEndElement();

            writer.WriteStartElement("cac:RegistrationAddress");
            writer.WriteElementString("cbc:AddressTypeCode", AccountingSupplierParty.CodDomicilioFiscal); //Código del domicilio fiscal sunat
            writer.WriteEndElement();

            writer.WriteEndElement();
            #endregion

            writer.WriteEndElement();
            #endregion

            writer.WriteEndElement();
            #endregion

            #region AccountingCustomerParty
            writer.WriteStartElement("cac:AccountingCustomerParty");

            writer.WriteElementString("cbc:CustomerAssignedAccountID", AccountingCustomerParty.CustomerAssignedAccountID);
            writer.WriteElementString("cbc:AdditionalAccountID",
                AccountingCustomerParty.AdditionalAccountID);

            #region Party
            writer.WriteStartElement("cac:Party");

            #region PartyIdentification
            writer.WriteStartElement("cac:PartyIdentification");
            writer.WriteStartElement("cbc:ID");
            writer.WriteAttributeString("schemeID", AccountingCustomerParty.AdditionalAccountID); //Codigo de identificacion de documento de cliente
            writer.WriteAttributeString("schemeName", PartyIdentification2.schemeName);
            writer.WriteAttributeString("schemeAgencyName", InvoiceTypeCode2.listAgencyName);
            writer.WriteAttributeString("schemeURI", PartyIdentification2.schemeURI);
            writer.WriteValue(AccountingCustomerParty.CustomerAssignedAccountID);
            writer.WriteEndElement();
            writer.WriteEndElement();
            #endregion PartyIdentification


            #region cbc:PartyLegalEntity
            writer.WriteStartElement("cac:PartyLegalEntity");

            writer.WriteStartElement("cbc:RegistrationName");
            writer.WriteCData(AccountingCustomerParty.Party.PartyLegalEntity.RegistrationName);
            writer.WriteEndElement();

            writer.WriteEndElement();
            #endregion

            writer.WriteEndElement();
            #endregion

            writer.WriteEndElement();
            #endregion

            #region PrepaidPayment
            if (PrepaidPayment != null)
            {
                writer.WriteStartElement("cac:PrepaidPayment");
                {
                    writer.WriteStartElement("cbc:ID");
                    {
                        writer.WriteAttributeString("schemeID", PrepaidPayment.Id.schemeID);
                        writer.WriteValue(PrepaidPayment.Id.value);
                    }
                    writer.WriteEndElement();
                    writer.WriteStartElement("cbc:PaidAmount");
                    {
                        writer.WriteAttributeString("currencyID", PrepaidPayment.PaidAmount.currencyID);
                        writer.WriteValue(PrepaidPayment.PaidAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                    }
                    writer.WriteEndElement();
                    writer.WriteStartElement("cbc:InstructionID");
                    {
                        writer.WriteAttributeString("schemeID", "6");
                        writer.WriteValue(PrepaidPayment.InstructionId);
                    }
                    writer.WriteEndElement();
                }
                writer.WriteEndElement();
            }
            #endregion

            #region AllowanceCharge Descuentos
            foreach (var taxTotal in TaxTotals)
            {
                if (LegalMonetaryTotal.AllowanceTotalAmount.value > 0) //Evalua si hay descuento
                {
                    // < !--En esta sección se ingresara el monto de descuento-->
                    writer.WriteStartElement("cac:AllowanceCharge");
                    writer.WriteElementString("cbc:ChargeIndicator", "false");
                    writer.WriteElementString("cbc:AllowanceChargeReasonCode", "0.00");
                    writer.WriteElementString("cbc:MultiplierFactorNumeric", "0.00");
                    // < !--Monto del descuento-->
                    writer.WriteStartElement("cbc:Amount");
                    writer.WriteAttributeString("currencyID", DocumentCurrencyCode);
                    writer.WriteValue(LegalMonetaryTotal.AllowanceTotalAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                    writer.WriteEndElement();
                    //Monto del cual se hará el descuento
                    writer.WriteStartElement("cbc:BaseAmount");
                    writer.WriteAttributeString("currencyID", DocumentCurrencyCode);
                    writer.WriteValue(taxTotal.TaxableAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                }
            }
            #endregion Descuentos
            #region TaxTotal
            foreach (var taxTotal in TaxTotals)
            {
                writer.WriteStartElement("cac:TaxTotal");

                writer.WriteStartElement("cbc:TaxAmount");
                writer.WriteAttributeString("currencyID", taxTotal.TaxAmount.currencyID);
                writer.WriteString(taxTotal.TaxAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                writer.WriteEndElement();

                #region TaxSubtotal
                {
                    writer.WriteStartElement("cac:TaxSubtotal");

                    writer.WriteStartElement("cbc:TaxableAmount");
                    writer.WriteAttributeString("currencyID", taxTotal.TaxableAmount.currencyID);
                    writer.WriteString(taxTotal.TaxableAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                    writer.WriteEndElement();

                    writer.WriteStartElement("cbc:TaxAmount");
                    writer.WriteAttributeString("currencyID", taxTotal.TaxSubtotal.TaxAmount.currencyID);
                    writer.WriteString(taxTotal.TaxAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                    writer.WriteEndElement();

                    #region TaxCategory

                    {
                        writer.WriteStartElement("cac:TaxCategory");
                        #region ID
                        writer.WriteStartElement("cbc:ID");
                        writer.WriteAttributeString("schemeID", "UN/ECE 5305");
                        writer.WriteAttributeString("schemeName", "Tax Category Identifier");
                        writer.WriteAttributeString("schemeAgencyName", "United Nations Economic Commission for Europe");

                        writer.WriteValue(taxTotal.TaxSubtotal.TaxCategory.Identifier); //VALOR OBTENIDO DE LA TABLA 5
                        writer.WriteEndElement();
                        #endregion ID

                        #region TaxScheme
                        {
                            writer.WriteStartElement("cac:TaxScheme");

                            //writer.WriteElementString("cbc:ID", taxTotal.TaxSubtotal.TaxCategory.TaxScheme.ID);

                            writer.WriteStartElement("cbc:ID");
                            writer.WriteAttributeString("schemeID", "UN/ECE 5305");
                            writer.WriteAttributeString("schemeAgencyID", "6");
                            writer.WriteValue(taxTotal.TaxSubtotal.TaxCategory.TaxScheme.ID);
                            writer.WriteEndElement();

                            writer.WriteElementString("cbc:Name", taxTotal.TaxSubtotal.TaxCategory.TaxScheme.Name);
                            writer.WriteElementString("cbc:TaxTypeCode", taxTotal.TaxSubtotal.TaxCategory.TaxScheme.TaxTypeCode);

                            writer.WriteEndElement();
                        }
                        #endregion

                        writer.WriteEndElement();
                    }
                    #endregion

                    writer.WriteEndElement();
                }
                #endregion

                writer.WriteEndElement();
            }
            #endregion

            #region LegalMonetaryTotal
            writer.WriteStartElement("cac:LegalMonetaryTotal");
            {
                //Descuento
                if (LegalMonetaryTotal.AllowanceTotalAmount.value > 0)
                {
                    writer.WriteStartElement("cbc:AllowanceTotalAmount");
                    {
                        writer.WriteAttributeString("currencyID", LegalMonetaryTotal.AllowanceTotalAmount.currencyID);
                        writer.WriteValue(LegalMonetaryTotal.AllowanceTotalAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                    }
                    writer.WriteEndElement();
                }
                if (LegalMonetaryTotal.PrepaidAmount.value > 0)
                {
                    writer.WriteStartElement("cbc:PrepaidAmount");
                    {
                        writer.WriteAttributeString("currencyID", LegalMonetaryTotal.PrepaidAmount.currencyID);
                        writer.WriteValue(LegalMonetaryTotal.PrepaidAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                    }
                    writer.WriteEndElement();
                }
                writer.WriteStartElement("cbc:PayableAmount");
                {
                    writer.WriteAttributeString("currencyID", LegalMonetaryTotal.PayableAmount.currencyID);
                    writer.WriteValue(LegalMonetaryTotal.PayableAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                }
                writer.WriteEndElement();

            }
            writer.WriteEndElement();
            #endregion

            #region InvoiceLines
            foreach (var invoiceLine in InvoiceLines)
            {
                writer.WriteStartElement("cac:InvoiceLine");

                writer.WriteElementString("cbc:ID", invoiceLine.ID.ToString());

                #region InvoicedQuantity
                writer.WriteStartElement("cbc:InvoicedQuantity");
                writer.WriteAttributeString("unitCode", invoiceLine.InvoicedQuantity.unitCode);
                writer.WriteAttributeString("unitCodeListID", "UN/ECE rec 20");
                writer.WriteAttributeString("unitCodeListAgencyName", "United Nations Economic Commission forEurope");
                writer.WriteValue(invoiceLine.InvoicedQuantity.Value.ToString(Constantes.FormatoNumerico, Formato));
                writer.WriteEndElement();
                #endregion

                #region LineExtensionAmount
                writer.WriteStartElement("cbc:LineExtensionAmount");
                writer.WriteAttributeString("currencyID", invoiceLine.LineExtensionAmount.currencyID);
                writer.WriteValue(invoiceLine.LineExtensionAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                writer.WriteEndElement();
                #endregion

                #region PricingReference
                writer.WriteStartElement("cac:PricingReference");

                #region AlternativeConditionPrice
                foreach (var item in invoiceLine.PricingReference.AlternativeConditionPrices)
                {
                    writer.WriteStartElement("cac:AlternativeConditionPrice");

                    #region PriceAmount
                    writer.WriteStartElement("cbc:PriceAmount");
                    writer.WriteAttributeString("currencyID", item.PriceAmount.currencyID);
                    writer.WriteValue(item.PriceAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                    writer.WriteEndElement();
                    #endregion

                    // writer.WriteElementString("cbc:PriceTypeCode", item.PriceTypeCode);
                    writer.WriteStartElement("cbc:PriceTypeCode");
                    writer.WriteAttributeString("listName", "SUNAT:Indicador de Tipo de Precio");
                    writer.WriteAttributeString("listAgencyName", "PE:SUNAT");
                    writer.WriteAttributeString("listURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo16");
                    writer.WriteValue(item.PriceTypeCode);
                    writer.WriteEndElement();


                    writer.WriteEndElement();
                }
                #endregion

                writer.WriteEndElement();
                #endregion

                #region AllowanceCharge
                if (invoiceLine.AllowanceCharge.ChargeIndicator)
                {
                    writer.WriteStartElement("cac:AllowanceCharge");

                    writer.WriteElementString("cbc:ChargeIndicator", invoiceLine.AllowanceCharge.ChargeIndicator.ToString());

                    #region Amount
                    writer.WriteStartElement("cbc:Amount");
                    writer.WriteAttributeString("currencyID", invoiceLine.AllowanceCharge.Amount.currencyID);
                    writer.WriteValue(invoiceLine.AllowanceCharge.Amount.value.ToString(Constantes.FormatoNumerico, Formato));
                    writer.WriteEndElement();
                    #endregion

                    writer.WriteEndElement();
                }
                #endregion

                #region TaxTotal
                {
                    foreach (var taxTotal in invoiceLine.TaxTotals)
                    {
                        writer.WriteStartElement("cac:TaxTotal");

                        writer.WriteStartElement("cbc:TaxAmount");
                        writer.WriteAttributeString("currencyID", taxTotal.TaxAmount.currencyID);
                        writer.WriteString(taxTotal.TaxAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                        writer.WriteEndElement();

                        #region TaxSubtotal
                        writer.WriteStartElement("cac:TaxSubtotal");

                        #region TaxableAmount

                        if (!string.IsNullOrEmpty(taxTotal.TaxableAmount.currencyID))
                        {
                            writer.WriteStartElement("cbc:TaxableAmount");
                            writer.WriteAttributeString("currencyID", taxTotal.TaxableAmount.currencyID);
                            writer.WriteString(taxTotal.TaxableAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                            writer.WriteEndElement();
                        }

                        #endregion

                        writer.WriteStartElement("cbc:TaxAmount");
                        writer.WriteAttributeString("currencyID", taxTotal.TaxSubtotal.TaxAmount.currencyID);
                        writer.WriteString(taxTotal.TaxAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                        writer.WriteEndElement();
                        if (taxTotal.TaxSubtotal.Percent > 0)
                            writer.WriteElementString("cbc:Percent", taxTotal.TaxSubtotal.Percent.ToString(Constantes.FormatoNumerico, Formato));

                        #region TaxCategory
                        writer.WriteStartElement("cac:TaxCategory");
                        //writer.WriteElementString("cbc:ID", invoiceLine.TaxTotal.TaxSubtotal.TaxCategory.ID);

                        #region ID
                        writer.WriteStartElement("cbc:ID");
                        writer.WriteAttributeString("schemeID", "UN/ECE 5305");
                        writer.WriteAttributeString("schemeName", "Tax Category Identifier");
                        writer.WriteAttributeString("schemeAgencyName", "United Nations Economic Commission for Europe");

                        writer.WriteValue(taxTotal.TaxSubtotal.TaxCategory.Identifier); //VALOR OBTENIDO DE LA TABLA 5
                        writer.WriteEndElement();
                        #endregion ID

                        writer.WriteElementString("cbc:Percent", ext2.AdditionalMonetaryTotals[4].Percent.ToString(Constantes.FormatoNumerico, Formato));
                        //writer.WriteElementString("cbc:TaxExemptionReasonCode", taxTotal.TaxSubtotal.TaxCategory.TaxExemptionReasonCode);
                        writer.WriteStartElement("cbc:TaxExemptionReasonCode");
                        writer.WriteAttributeString("listAgencyName", "PE:SUNAT");
                        writer.WriteAttributeString("listName", "SUNAT:Codigo de Tipo de Afectación del IGV");
                        writer.WriteAttributeString("listURI", "urn:pe:gob:sunat:cpe:see:gem:catalogos:catalogo07");
                        writer.WriteValue(taxTotal.TaxSubtotal.TaxCategory.TaxExemptionReasonCode);
                        writer.WriteEndElement();

                        if (!string.IsNullOrEmpty(taxTotal.TaxSubtotal.TaxCategory.TierRange))
                            writer.WriteElementString("cbc:TierRange", taxTotal.TaxSubtotal.TaxCategory.TierRange);

                        #region TaxScheme
                        {
                            writer.WriteStartElement("cac:TaxScheme");

                            // writer.WriteElementString("cbc:ID", taxTotal.TaxSubtotal.TaxCategory.TaxScheme.Id);
                            #region ID
                            writer.WriteStartElement("cbc:ID");
                            writer.WriteAttributeString("schemeID", "UN/ECE 5153");
                            writer.WriteAttributeString("schemeName", "Tax Scheme Identifier");
                            writer.WriteAttributeString("schemeAgencyName", "United Nations Economic Commission for Europe");

                            writer.WriteValue(taxTotal.TaxSubtotal.TaxCategory.TaxScheme.ID); //VALOR OBTENIDO DE LA TABLA 5
                            writer.WriteEndElement();
                            #endregion ID
                            writer.WriteElementString("cbc:Name", taxTotal.TaxSubtotal.TaxCategory.TaxScheme.Name);
                            writer.WriteElementString("cbc:TaxTypeCode", taxTotal.TaxSubtotal.TaxCategory.TaxScheme.TaxTypeCode);

                            writer.WriteEndElement();
                        }
                        #endregion

                        writer.WriteEndElement();
                        #endregion

                        writer.WriteEndElement();
                        #endregion

                        writer.WriteEndElement();
                    }
                }
                #endregion

                #region Item
                writer.WriteStartElement("cac:Item");

                #region Description
                writer.WriteElementString("cbc:Description", invoiceLine.Item.Description);
                //writer.WriteStartElement("cbc:Description");
                //writer.WriteCData(invoiceLine.Item.Description);
                //writer.WriteEndElement();
                #endregion

                #region SellersItemIdentification
                writer.WriteStartElement("cac:SellersItemIdentification");
                writer.WriteElementString("cbc:ID", invoiceLine.Item.SellersItemIdentification.ID);
                writer.WriteEndElement();
                #endregion


                #region CommodityClassification
                if (invoiceLine.ItemClassificationCode != null)
                {
                    writer.WriteStartElement("cac:CommodityClassification");
                    writer.WriteStartElement("cbc:ItemClassificationCode");
                    writer.WriteAttributeString("listID", "UNSPSC");
                    writer.WriteAttributeString("listAgencyName", "GS1 US");
                    writer.WriteAttributeString("listName", "Item Classification");
                    writer.WriteValue(invoiceLine.ItemClassificationCode);//82141601-SERVICIOS FOTOGRAFICOS, MONTAJE Y ENMARCADO	82141602 - MONTAJE DE EXPOSICION DE ARTICULOS
                    writer.WriteEndElement();
                    writer.WriteEndElement();
                }
                #endregion CommodityClassification


                writer.WriteEndElement();
                #endregion

                #region Price
                writer.WriteStartElement("cac:Price");

                writer.WriteStartElement("cbc:PriceAmount");
                writer.WriteAttributeString("currencyID", invoiceLine.Price.PriceAmount.currencyID);
                writer.WriteString(invoiceLine.Price.PriceAmount.value.ToString(Constantes.FormatoNumerico, Formato));
                writer.WriteEndElement();

                writer.WriteEndElement();
                #endregion

                writer.WriteEndElement();
            }
            #endregion
        }
    }
}